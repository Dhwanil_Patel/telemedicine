// var gulp = require("gulp");
// var angularModules = require("gulp-angular-modules");
//
// gulp.task("default", function() {
//
//     var options = {
//         name: "gulp-angular-modules", // The name of the module to use in your main Angular.js
//         modules: ['ui.router'] // Any extra modules that you want to include.
//     };
//
//     return gulp.src(["app/src/**/*.js", "!app/src/templates/*"])
//         .pipe(angularModules("gulp-angular-modules.js", options)) // Name of the file generated
//         .pipe(gulp.dest("app/src/init/")) // Destination folder
// });

var gulp = require('gulp');
var serve = require('gulp-serve');

gulp.task('serve', serve('public'));
gulp.task('serve-build', serve(['public', 'build']));
gulp.task('serve-prod', serve({
    root: ['public', 'build'],
    port: 80,
    middleware: function(req, res) {
        // custom optional middleware
    }
}));
