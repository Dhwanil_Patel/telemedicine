import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { OrderMedicine } from './order-medicine.model';
import { OrderMedicineService } from './order-medicine.service';

@Injectable()
export class OrderMedicinePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private orderMedicineService: OrderMedicineService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.orderMedicineService.find(id)
                    .subscribe((orderMedicineResponse: HttpResponse<OrderMedicine>) => {
                        const orderMedicine: OrderMedicine = orderMedicineResponse.body;
                        this.ngbModalRef = this.orderMedicineModalRef(component, orderMedicine);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.orderMedicineModalRef(component, new OrderMedicine());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    orderMedicineModalRef(component: Component, orderMedicine: OrderMedicine): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.orderMedicine = orderMedicine;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
