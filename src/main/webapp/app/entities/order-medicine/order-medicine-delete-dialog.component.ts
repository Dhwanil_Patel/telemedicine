import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OrderMedicine } from './order-medicine.model';
import { OrderMedicinePopupService } from './order-medicine-popup.service';
import { OrderMedicineService } from './order-medicine.service';

@Component({
    selector: 'jhi-order-medicine-delete-dialog',
    templateUrl: './order-medicine-delete-dialog.component.html'
})
export class OrderMedicineDeleteDialogComponent {

    orderMedicine: OrderMedicine;

    constructor(
        private orderMedicineService: OrderMedicineService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.orderMedicineService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'orderMedicineListModification',
                content: 'Deleted an orderMedicine'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-order-medicine-delete-popup',
    template: ''
})
export class OrderMedicineDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private orderMedicinePopupService: OrderMedicinePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.orderMedicinePopupService
                .open(OrderMedicineDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
