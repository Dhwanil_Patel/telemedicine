import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OrderMedicine } from './order-medicine.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<OrderMedicine>;

@Injectable()
export class OrderMedicineService {

    private resourceUrl =  SERVER_API_URL + 'api/order-medicines';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/order-medicines';
    private resourcePatientUrl = SERVER_API_URL + 'api/order-medicines/patient/';
    private resourcePharmacistUrl = SERVER_API_URL + 'api/order-medicines/pharmacist/';

    constructor(private http: HttpClient) { }

    create(orderMedicine: OrderMedicine): Observable<EntityResponseType> {
        const copy = this.convert(orderMedicine);
        return this.http.post<OrderMedicine>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    // created new service
    getOrderMedicineByPatientId(req?: any): Observable<HttpResponse<OrderMedicine[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderMedicine[]>(this.resourcePatientUrl, { params: options, observe: 'response'})
            .map((res: HttpResponse<OrderMedicine[]>) => this.convertArrayResponse(res));
    }

    // created new service
    getOrderMedicineByPharmacistId(id: number): Observable<HttpResponse<OrderMedicine[]>> {
        return this.http.get<OrderMedicine[]>(`${this.resourcePharmacistUrl}/${id}`, { observe: 'response'})
            .map((res: HttpResponse<OrderMedicine[]>) => this.convertArrayResponse(res));
    }

    update(orderMedicine: OrderMedicine): Observable<EntityResponseType> {
        const copy = this.convert(orderMedicine);
        return this.http.put<OrderMedicine>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<OrderMedicine>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<OrderMedicine[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderMedicine[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<OrderMedicine[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<OrderMedicine[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderMedicine[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<OrderMedicine[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: OrderMedicine = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<OrderMedicine[]>): HttpResponse<OrderMedicine[]> {
        const jsonResponse: OrderMedicine[] = res.body;
        const body: OrderMedicine[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to OrderMedicine.
     */
    private convertItemFromServer(orderMedicine: OrderMedicine): OrderMedicine {
        const copy: OrderMedicine = Object.assign({}, orderMedicine);
        return copy;
    }

    /**
     * Convert a OrderMedicine to a JSON which can be sent to the server.
     */
    private convert(orderMedicine: OrderMedicine): OrderMedicine {
        const copy: OrderMedicine = Object.assign({}, orderMedicine);
        return copy;
    }
}
