import { BaseEntity } from './../../shared';

export class OrderMedicine implements BaseEntity {
    constructor(
        public id?: number,
        public medicinedetail?: string,
        public totalprice?: number,
        public prescriptionId?: number,
        public pharmacistId?: number,
        public pharmacistPaymentId?: number,
        public status?: string
    ) {
    }
}
