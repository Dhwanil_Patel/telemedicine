import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { OrderMedicine } from './order-medicine.model';
import { OrderMedicinePopupService } from './order-medicine-popup.service';
import { OrderMedicineService } from './order-medicine.service';
import { Prescription, PrescriptionService } from '../prescription';
import { Pharmacist, PharmacistService } from '../pharmacist';
import { PharmacistPayment, PharmacistPaymentService } from '../pharmacist-payment';
import { Principal } from '../../shared/auth/principal.service';
import { Patient } from '../patient/patient.model';
import { PatientService } from '../patient/patient.service';

@Component({
    selector: 'jhi-order-medicine-dialog',
    templateUrl: './order-medicine-dialog.component.html'
})
export class OrderMedicineDialogComponent implements OnInit {

    patient: Patient;
    pharmacist: Pharmacist;
    patientId: number;
    pharmacistId: number;
    orderMedicine: OrderMedicine;
    isSaving: boolean;
    account: Account;
    patient_prescriptions: Prescription[];
    pharmacist_prescriptions: Prescription[];
    pharmacists: Pharmacist[];
    prescriptions: Prescription[];
    pharmacistpayments: PharmacistPayment[];

    medicine = {
        name: '',
        quantity: 0,
        time: '',
        price: 0
    };
    medicines = [];
    currentOrder = [];
    totalBill = 0;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private orderMedicineService: OrderMedicineService,
        private prescriptionService: PrescriptionService,
        private pharmacistService: PharmacistService,
        private pharmacistPaymentService: PharmacistPaymentService,
        private principal: Principal,
        private eventManager: JhiEventManager,
        private patientService: PatientService,
    ) {
        // this.totalBill = 0;
    }

    ngOnInit() {
        this.isSaving = false;
        this.principal.identity().then((account) => {
            this.account = account;
            if ( account.usertype === 'Pharmacist') {
                account.status = 'confirm';
                this.orderMedicine.status = account.status;
                console.log( 'status:' , this.orderMedicine.status );
            }
                if ( account.usertype === 'Patient') {
                // get patient id
                this.patientService.getPatientByUserId().subscribe((response) => {
                    account.status = 'pending';
                    this.orderMedicine.status = account.status;
                    this.loadPrescriptionByPatientID();
                });
            }
        });
        this.prescriptionService
            .query({filter: 'ordermedicine-is-null'})
            .subscribe((res: HttpResponse<Prescription[]>) => {
                if (!this.orderMedicine.prescriptionId) {
                    this.prescriptions = res.body;
                } else {
                    this.prescriptionService
                        .find(this.orderMedicine.prescriptionId)
                        .subscribe((subRes: HttpResponse<Prescription>) => {
                            this.prescriptions = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.pharmacistService
            .query({filter: 'ordermedicine-is-null'})
            .subscribe((res: HttpResponse<Pharmacist[]>) => {
                if (!this.orderMedicine.pharmacistId) {
                    this.pharmacists = res.body;
                } else {
                    this.pharmacistService
                        .find(this.orderMedicine.pharmacistId)
                        .subscribe((subRes: HttpResponse<Pharmacist>) => {
                            this.pharmacists = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.pharmacistPaymentService.query()
            .subscribe((res: HttpResponse<PharmacistPayment[]>) => { this.pharmacistpayments = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    loadPrescriptionByPatientID() {
        this.prescriptionService.getPrescriptionByPatientId({filter: 'ordermedicine-is-null'})
            .subscribe((response) => {
            this.patient_prescriptions = response.body;
        });
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        // if( this.account.usertype )
                this.orderMedicine.totalprice = this.countPrice();
        this.orderMedicine.medicinedetail = JSON.stringify(this.currentOrder);
        this.isSaving = true;
        if (this.orderMedicine.id !== undefined) {
            this.principal.identity().then((account) => {
                this.account = account;

            });
            this.subscribeToSaveResponse(
                this.orderMedicineService.update(this.orderMedicine));
                console.log( 'ordermedicine', this.orderMedicine);
        } else {
            this.subscribeToSaveResponse(
                this.orderMedicineService.create(this.orderMedicine));
        }
    }
    private subscribeToSaveResponse(result: Observable<HttpResponse<OrderMedicine>>) {
        result.subscribe((res: HttpResponse<OrderMedicine>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: OrderMedicine) {
        this.eventManager.broadcast({ name: 'orderMedicineListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPrescriptionById(index: number, item: Prescription) {
        return item.id;
    }

    trackPharmacistById(index: number, item: Pharmacist) {
        return item.id;
    }

    trackPharmacistPaymentById(index: number, item: PharmacistPayment) {
        return item.id;
    }

    selectPrescription() {
        const self = this;
        this.medicines = null;
        const selectedPrescriptionId = this.orderMedicine.prescriptionId;
        this.prescriptions.forEach(function(prescription) {
             console.log('prescription.id = ', prescription.id);
             console.log('this.orderMedicine.prescriptionId = ', selectedPrescriptionId);
            if (prescription.id === selectedPrescriptionId) {
                self.medicines = JSON.parse(prescription.prescriptiondetail);
            }
        });
        console.log('this.medicines = ', this.medicines);

        // let self = this;
        // this.medicines = null;
        // const selectedPrescriptionId = this.orderMedicine.prescriptionId;
        // let sizePre = this.prescriptions.length;
        // for (let j = 0; j < sizePre; j++) {
        //     if (j === selectedPrescriptionId) {
        //
        //         // const sizePresDetail = JSON.parse(this.prescriptions[j].prescriptiondetail).length;
        //         // for (let k = 0; k < sizePresDetail; k++) {
        //         //     console.log('Prescription individual : ', this.prescriptions[j].prescriptiondetail[k]);
        //             this.medicines = JSON.parse(this.prescriptions[j].prescriptiondetail);
        //         // }
        //         // console.log(this.medicines);
        //     }
        //     // console.log("Prescription is : ", this.prescriptions[j].prescriptiondetail);
        // }
        // // console.log(JSON.parse(this.prescriptions[myPrescription - 1].prescriptiondetail));
        // // this.medicines = JSON.parse(this.prescriptions[myPrescription].prescriptiondetail);
        // console.log(this.medicines);
        this.currentOrder = this.medicines;
        return this.medicines;
    }

    createJSON(id, price) {
        const self = this;
        this.currentOrder.forEach(function(medicine) {
            if (medicine.id === id) {
                if (medicine.price > 0) {
                    this.totalBill -= medicine.price;
                }
                this.totalBill += price;
                this.medicine.price = price;
                // self.medicines = JSON.parse(prescription.prescriptiondetail);
            }
        });

        // console.log('this.medicines = ', this.medicines);
        // console.log('Total Bill is : ', this.totalBill);

        // console.log('Current order is : ', this.currentOrder);
        // console.log('Length is : ', this.currentOrder.length);
        // const size = this.currentOrder.length;
        // for (let i = 0; i < size; i++) {
        //     this.medicine = this.currentOrder[i];
        //     console.log('Medicine name is : ', this.medicine.name);
        //     if (this.medicine.name === name) {
        //         if (this.medicine.price >= 0) {
        //             this.totalBill -= this.medicine.price;
        //         }
        //         this.totalBill += parseInt(price, 10);
        //         this.medicine.price = parseInt(price, 10);
        //     }
        //     this.currentOrder[i] = this.medicine;
        // }
        // console.log('New Medicine Detail is : ', this. currentOrder);
        // console.log('Total Bill is : ', this.totalBill);
    }

    countPrice() {

        console.log('Count price call');
        const self = this;
        this.totalBill = 0;

        this.currentOrder.forEach(function(medicine) {
            self.totalBill += parseInt(medicine.price, 10);
        });
        console.log('Total Bill now : ', this.totalBill);

        return self.totalBill;
    }
}

@Component({
    selector: 'jhi-order-medicine-popup',
    template: ''
})
export class OrderMedicinePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private orderMedicinePopupService: OrderMedicinePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.orderMedicinePopupService
                    .open(OrderMedicineDialogComponent as Component, params['id']);
            } else {
                this.orderMedicinePopupService
                    .open(OrderMedicineDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
