import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule } from '../../shared';
import {
    OrderMedicineService,
    OrderMedicinePopupService,
    OrderMedicineComponent,
    OrderMedicineDetailComponent,
    OrderMedicineDialogComponent,
    OrderMedicinePopupComponent,
    OrderMedicineDeletePopupComponent,
    OrderMedicineDeleteDialogComponent,
    orderMedicineRoute,
    orderMedicinePopupRoute,
    OrderMedicineResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...orderMedicineRoute,
    ...orderMedicinePopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        OrderMedicineComponent,
        OrderMedicineDetailComponent,
        OrderMedicineDialogComponent,
        OrderMedicineDeleteDialogComponent,
        OrderMedicinePopupComponent,
        OrderMedicineDeletePopupComponent,
    ],
    entryComponents: [
        OrderMedicineComponent,
        OrderMedicineDialogComponent,
        OrderMedicinePopupComponent,
        OrderMedicineDeleteDialogComponent,
        OrderMedicineDeletePopupComponent,
    ],
    providers: [
        OrderMedicineService,
        OrderMedicinePopupService,
        OrderMedicineResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationOrderMedicineModule {}
