import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { OrderMedicineComponent } from './order-medicine.component';
import { OrderMedicineDetailComponent } from './order-medicine-detail.component';
import { OrderMedicinePopupComponent } from './order-medicine-dialog.component';
import { OrderMedicineDeletePopupComponent } from './order-medicine-delete-dialog.component';

@Injectable()
export class OrderMedicineResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const orderMedicineRoute: Routes = [
    {
        path: 'order-medicine',
        component: OrderMedicineComponent,
        resolve: {
            'pagingParams': OrderMedicineResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PATIENT', 'ROLE_PHARMACIST'],
            pageTitle: 'telemedicineApplicationApp.orderMedicine.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'order-medicine/:id',
        component: OrderMedicineDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PATIENT', 'ROLE_PHARMACIST'],
            pageTitle: 'telemedicineApplicationApp.orderMedicine.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderMedicinePopupRoute: Routes = [
    {
        path: 'order-medicine-new',
        component: OrderMedicinePopupComponent,
        data: {
            authorities: ['ROLE_PATIENT'],
            pageTitle: 'telemedicineApplicationApp.orderMedicine.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-medicine/:id/edit',
        component: OrderMedicinePopupComponent,
        data: {
            authorities: ['ROLE_PHARMACIST', 'ROLE_PATIENT'],
            pageTitle: 'telemedicineApplicationApp.orderMedicine.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-medicine/:id/delete',
        component: OrderMedicineDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.orderMedicine.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
