import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { OrderMedicine } from './order-medicine.model';
import { OrderMedicineService } from './order-medicine.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { Patient } from '../patient/patient.model';
import { Pharmacist } from '../pharmacist/pharmacist.model';
import { PatientService } from '../patient/patient.service';
import { PharmacistService } from '../pharmacist/pharmacist.service';

@Component({
    selector: 'jhi-order-medicine',
    templateUrl: './order-medicine.component.html'
})
export class OrderMedicineComponent implements OnInit, OnDestroy {

    currentAccount: any;
    orderMedicines: OrderMedicine[];
    patient_orderMedicines: OrderMedicine[];
    pharmacist_orderMedicines: OrderMedicine[];
    patient: Patient;
    pharmacist: Pharmacist;
    patientid: number;
    pharmacistid: number;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    myData: any;

    constructor(
        private orderMedicineService: OrderMedicineService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private patientService: PatientService,
        private pharmacistService: PharmacistService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.orderMedicineService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: HttpResponse<OrderMedicine[]>) => this.onSuccess(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        }
        this.orderMedicineService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: HttpResponse<OrderMedicine[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/order-medicine'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/order-medicine', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/order-medicine', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            if (account.usertype === 'Patient') {
                // get patient id
                this.patientService.getPatientByUserId().subscribe((response) => {
                    this.patient = response.body;
                    this.patientid = this.patient.id;
                    this.loadOrderMedicineByPatientID();
                });
            }
            if (account.usertype === 'Pharmacist') {
                // get pharmacist id
                this.pharmacistService.getPharmacistByUserId().subscribe((response) => {
                   this.pharmacist = response.body;
                   this.pharmacistid = this.pharmacist.id;
                   this.loadOrderMedicineByPharmacistID();
                });
            }
        });
        this.registerChangeInOrderMedicines();
    }
    loadOrderMedicineByPatientID() {
        this.orderMedicineService.getOrderMedicineByPatientId(this.patientid).subscribe((response) => {
            this.patient_orderMedicines = response.body;
            const prescriptiondata = response.body[0]['medicinedetail'];
            console.log(response.body[0]['medicinedetail']);
            const resources = JSON.parse(prescriptiondata);
            const resource = resources[0]['name'];
            this.myData = resources ;
        });
    }
    loadOrderMedicineByPharmacistID() {
        this.orderMedicineService.getOrderMedicineByPharmacistId(this.pharmacistid).subscribe((response) => {
           this.pharmacist_orderMedicines = response.body;
            const prescriptiondata = response.body[0]['medicinedetail'];
            console.log(response.body[0]['medicinedetail']);
            const resources = JSON.parse(prescriptiondata);
            const resource = resources[0]['name'];
            this.myData = resources ;
        });
    }
    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: OrderMedicine) {
        return item.id;
    }
    registerChangeInOrderMedicines() {
        this.eventSubscriber = this.eventManager.subscribe('orderMedicineListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.orderMedicines = data;
        const prescriptiondata = data[0]['medicinedetail'];
        console.log(data[0]['medicinedetail']);
        const resources = JSON.parse(prescriptiondata);
        const resource = resources[0]['name'];
        this.myData = resources ;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
