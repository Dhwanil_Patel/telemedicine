import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { OrderMedicine } from './order-medicine.model';
import { OrderMedicineService } from './order-medicine.service';

@Component({
    selector: 'jhi-order-medicine-detail',
    templateUrl: './order-medicine-detail.component.html'
})
export class OrderMedicineDetailComponent implements OnInit, OnDestroy {

    orderMedicine: OrderMedicine;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private orderMedicineService: OrderMedicineService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInOrderMedicines();
    }

    load(id) {
        this.orderMedicineService.find(id)
            .subscribe((orderMedicineResponse: HttpResponse<OrderMedicine>) => {
                this.orderMedicine = orderMedicineResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOrderMedicines() {
        this.eventSubscriber = this.eventManager.subscribe(
            'orderMedicineListModification',
            (response) => this.load(this.orderMedicine.id)
        );
    }
}
