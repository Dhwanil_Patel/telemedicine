import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PharmacistComponent } from './pharmacist.component';
import { PharmacistDetailComponent } from './pharmacist-detail.component';
import { PharmacistPopupComponent } from './pharmacist-dialog.component';
import { PharmacistDeletePopupComponent } from './pharmacist-delete-dialog.component';

@Injectable()
export class PharmacistResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const pharmacistRoute: Routes = [
    {
        path: 'pharmacist',
        component: PharmacistComponent,
        resolve: {
            'pagingParams': PharmacistResolvePagingParams
        },
        data: {
            authorities: ['ROLE_PHARMACIST', 'ROLE_PATIENT', 'ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.pharmacist.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'pharmacist/:id',
        component: PharmacistDetailComponent,
        data: {
            authorities: ['ROLE_PHARMACIST', 'ROLE_PATIENT', 'ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.pharmacist.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pharmacistPopupRoute: Routes = [
    {
        path: 'pharmacist-new',
        component: PharmacistPopupComponent,
        data: {
            authorities: ['ROLE_PHARMACIST'],
            pageTitle: 'telemedicineApplicationApp.pharmacist.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pharmacist/:id/edit',
        component: PharmacistPopupComponent,
        data: {
            authorities: ['ROLE_PHARMACIST'],
            pageTitle: 'telemedicineApplicationApp.pharmacist.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pharmacist/:id/delete',
        component: PharmacistDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.pharmacist.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
