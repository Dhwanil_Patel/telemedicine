import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Pharmacist } from './pharmacist.model';
import { PharmacistPopupService } from './pharmacist-popup.service';
import { PharmacistService } from './pharmacist.service';
import { OrderMedicine, OrderMedicineService } from '../order-medicine';
import {Principal} from '../../shared/auth/principal.service';

@Component({
    selector: 'jhi-pharmacist-dialog',
    templateUrl: './pharmacist-dialog.component.html'
})
export class PharmacistDialogComponent implements OnInit {

    pharmacist: Pharmacist;
    account: Account;
    isSaving: boolean;

    ordermedicines: OrderMedicine[];

    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private pharmacistService: PharmacistService,
        private orderMedicineService: OrderMedicineService,
        private eventManager: JhiEventManager,

    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.principal.identity().then((account) => {
            this.account = account;
            this.pharmacist.userId = account.id;
        });
        this.orderMedicineService.query()
            .subscribe((res: HttpResponse<OrderMedicine[]>) => { this.ordermedicines = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pharmacist.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pharmacistService.update(this.pharmacist));
        } else {
            this.subscribeToSaveResponse(
                this.pharmacistService.create(this.pharmacist));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Pharmacist>>) {
        result.subscribe((res: HttpResponse<Pharmacist>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Pharmacist) {
        this.eventManager.broadcast({ name: 'pharmacistListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOrderMedicineById(index: number, item: OrderMedicine) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-pharmacist-popup',
    template: ''
})
export class PharmacistPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pharmacistPopupService: PharmacistPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pharmacistPopupService
                    .open(PharmacistDialogComponent as Component, params['id']);
            } else {
                this.pharmacistPopupService
                    .open(PharmacistDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
