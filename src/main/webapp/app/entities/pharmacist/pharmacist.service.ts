import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Pharmacist } from './pharmacist.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Pharmacist>;

@Injectable()
export class PharmacistService {

    private resourceUrl =  SERVER_API_URL + 'api/pharmacists';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/pharmacists';
    private resourceUserUrl =  SERVER_API_URL + 'api/pharmacists/userId';

    constructor(private http: HttpClient) { }

    create(pharmacist: Pharmacist): Observable<EntityResponseType> {
        const copy = this.convert(pharmacist);
        return this.http.post<Pharmacist>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    // get pharmacist by usreid
    getPharmacistByUserId(): Observable<EntityResponseType> {
        // this.userid = parseInt(id);
        return this.http.get<Pharmacist>(`${this.resourceUserUrl}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(pharmacist: Pharmacist): Observable<EntityResponseType> {
        const copy = this.convert(pharmacist);
        return this.http.put<Pharmacist>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Pharmacist>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Pharmacist[]>> {
        const options = createRequestOption(req);
        return this.http.get<Pharmacist[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Pharmacist[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Pharmacist[]>> {
        const options = createRequestOption(req);
        return this.http.get<Pharmacist[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Pharmacist[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Pharmacist = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Pharmacist[]>): HttpResponse<Pharmacist[]> {
        const jsonResponse: Pharmacist[] = res.body;
        const body: Pharmacist[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Pharmacist.
     */
    private convertItemFromServer(pharmacist: Pharmacist): Pharmacist {
        const copy: Pharmacist = Object.assign({}, pharmacist);
        return copy;
    }

    /**
     * Convert a Pharmacist to a JSON which can be sent to the server.
     */
    private convert(pharmacist: Pharmacist): Pharmacist {
        const copy: Pharmacist = Object.assign({}, pharmacist);
        return copy;
    }
}
