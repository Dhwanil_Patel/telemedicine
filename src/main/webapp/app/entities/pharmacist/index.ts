export * from './pharmacist.model';
export * from './pharmacist-popup.service';
export * from './pharmacist.service';
export * from './pharmacist-dialog.component';
export * from './pharmacist-delete-dialog.component';
export * from './pharmacist-detail.component';
export * from './pharmacist.component';
export * from './pharmacist.route';
