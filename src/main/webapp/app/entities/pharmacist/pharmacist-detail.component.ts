import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Pharmacist } from './pharmacist.model';
import { PharmacistService } from './pharmacist.service';

@Component({
    selector: 'jhi-pharmacist-detail',
    templateUrl: './pharmacist-detail.component.html'
})
export class PharmacistDetailComponent implements OnInit, OnDestroy {

    pharmacist: Pharmacist;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private pharmacistService: PharmacistService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPharmacists();
    }

    load(id) {
        this.pharmacistService.find(id)
            .subscribe((pharmacistResponse: HttpResponse<Pharmacist>) => {
                this.pharmacist = pharmacistResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPharmacists() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pharmacistListModification',
            (response) => this.load(this.pharmacist.id)
        );
    }
}
