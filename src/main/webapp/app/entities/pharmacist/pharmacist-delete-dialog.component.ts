import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Pharmacist } from './pharmacist.model';
import { PharmacistPopupService } from './pharmacist-popup.service';
import { PharmacistService } from './pharmacist.service';

@Component({
    selector: 'jhi-pharmacist-delete-dialog',
    templateUrl: './pharmacist-delete-dialog.component.html'
})
export class PharmacistDeleteDialogComponent {

    pharmacist: Pharmacist;

    constructor(
        private pharmacistService: PharmacistService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pharmacistService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pharmacistListModification',
                content: 'Deleted an pharmacist'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pharmacist-delete-popup',
    template: ''
})
export class PharmacistDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pharmacistPopupService: PharmacistPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pharmacistPopupService
                .open(PharmacistDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
