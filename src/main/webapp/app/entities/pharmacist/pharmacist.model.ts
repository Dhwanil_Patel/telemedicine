import { BaseEntity } from './../../shared';

export const enum Process_Status {
    'ACTIVE',
    'DEACTIVE'
}

export class Pharmacist implements BaseEntity {
    constructor(
        public id?: number,
        public shopname?: string,
        public status?: Process_Status,
        public shopaddress?: string,
        public phoneno?: string,
        public licenceno?: string,
        public documentsContentType?: string,
        public documents?: any,
        public orderMedicineId?: number,
        public userId?: number
    ) {
    }
}
