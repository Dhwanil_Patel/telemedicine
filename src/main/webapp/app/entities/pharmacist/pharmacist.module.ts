import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule } from '../../shared';
import {
    PharmacistService,
    PharmacistPopupService,
    PharmacistComponent,
    PharmacistDetailComponent,
    PharmacistDialogComponent,
    PharmacistPopupComponent,
    PharmacistDeletePopupComponent,
    PharmacistDeleteDialogComponent,
    pharmacistRoute,
    pharmacistPopupRoute,
    PharmacistResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...pharmacistRoute,
    ...pharmacistPopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PharmacistComponent,
        PharmacistDetailComponent,
        PharmacistDialogComponent,
        PharmacistDeleteDialogComponent,
        PharmacistPopupComponent,
        PharmacistDeletePopupComponent,
    ],
    entryComponents: [
        PharmacistComponent,
        PharmacistDialogComponent,
        PharmacistPopupComponent,
        PharmacistDeleteDialogComponent,
        PharmacistDeletePopupComponent,
    ],
    providers: [
        PharmacistService,
        PharmacistPopupService,
        PharmacistResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationPharmacistModule {}
