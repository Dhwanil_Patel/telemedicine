import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PrescriptionComponent } from './prescription.component';
import { PrescriptionDetailComponent } from './prescription-detail.component';
import { PrescriptionPopupComponent } from './prescription-dialog.component';
import { PrescriptionDeletePopupComponent } from './prescription-delete-dialog.component';

@Injectable()
export class PrescriptionResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prescriptionRoute: Routes = [
    {
        path: 'prescription',
        component: PrescriptionComponent,
        resolve: {
            'pagingParams': PrescriptionResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR', 'ROLE_PATIENT'],
            pageTitle: 'telemedicineApplicationApp.prescription.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'prescription/:id',
        component: PrescriptionDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR', 'ROLE_PATIENT'],
            pageTitle: 'telemedicineApplicationApp.prescription.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const prescriptionPopupRoute: Routes = [
    {
        path: 'prescription-new',
        component: PrescriptionPopupComponent,
        data: {
            authorities: ['ROLE_DOCTOR'],
            pageTitle: 'telemedicineApplicationApp.prescription.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'prescription/:id/edit',
        component: PrescriptionPopupComponent,
        data: {
            authorities: ['ROLE_DOCTOR'],
            pageTitle: 'telemedicineApplicationApp.prescription.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'prescription/:id/delete',
        component: PrescriptionDeletePopupComponent,
        data: {
            authorities: ['ROLE_DOCTOR'],
            pageTitle: 'telemedicineApplicationApp.prescription.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
