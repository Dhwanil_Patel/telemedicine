import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Prescription } from './prescription.model';
import { PrescriptionService } from './prescription.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { Appointment } from '../appointment/appointment.model';
import { AppointmentService } from '../appointment/appointment.service';
import { PatientService } from '../patient/patient.service';
import { Patient } from '../patient/patient.model';
import { Doctor } from '../doctor/doctor.model';
import { DoctorService } from '../doctor/doctor.service';

@Component({
    selector: 'jhi-prescription',
    templateUrl: './prescription.component.html'
})
export class PrescriptionComponent implements OnInit, OnDestroy {

    currentAccount: any;
    prescriptions: Prescription[];
    doctor_prescriptions: Prescription[];
    patient_prescriptions: Prescription[];
    account: Account;
    doctorId: number;
    patientId: number;
    doctor: Doctor;
    appointments: Appointment[];
    medicine: String[];
    patient: Patient;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    myData: any;

    constructor(
        private prescriptionService: PrescriptionService,
        private appointmentService: AppointmentService,
        private doctorService: DoctorService,
        private patientService: PatientService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }
    loadAll() {
        if (this.currentSearch) {
            this.prescriptionService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: HttpResponse<Prescription[]>) => this.onSuccess(res.body, res.headers),
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
        }
        this.prescriptionService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: HttpResponse<Prescription[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.principal.identity().then((account) => {
            this.account = account;
            if ( account.usertype === 'Doctor') {
                // get doctor id
                this.doctorService.getDoctorByUserId().subscribe((response) => {
                    this.doctor = response.body;
                    this.doctorId = this.doctor.id;
                    this.loadPrescriptionByDoctorID();
                });
            }
            if ( account.usertype === 'Patient') {
                // get patient id
                this.loadPrescriptionByPatientID();
                /*this.patientService.getPatientByUserId().subscribe((response) => {
                    this.patient = response.body;
                    this.patientId = this.patient.id;
                    this.loadPrescriptionByPatientID();
                });*/
            }
        });
        /*this.loadPrescriptionByPatientID().subscribe((response) => {
            // console.log('responce =====> ', res1);
            this.myData = response;
            let resources = response["prescriptiondetail"];
            let resource = resources['name'];
            console.log(resource['name']);
                console.log('data1==', response);
                        });*/
    }
    // loadPrescriptionByDoctorID
    loadPrescriptionByDoctorID() {
        this.prescriptionService.getPrescriptionByDoctorId(this.doctorId).subscribe((response) => {
            this.doctor_prescriptions = response.body;
            for ( let i = 0 ; i < response.body.length ; i++ ) {
                const prescriptiondata = response.body[i]['prescriptiondetail'];
                console.log(response.body[i]['prescriptiondetail']);
                const resources = JSON.parse(prescriptiondata);
                const resource = resources[i]['name'];
                this.myData = resources ;
                console.log('resources==', resource);
                console.log('data==', response); }
        });
    }
    // loadPrescriptionByPatientID
    loadPrescriptionByPatientID() {
        this.prescriptionService.getPrescriptionByPatientId().subscribe((response) => {
            this.patient_prescriptions = response.body;
            console.log('response size:' , response.body.length);
            for ( let i = 0 ; i < response.body.length ; i++ ) {
            const prescriptiondata = response.body[i]['prescriptiondetail'];
            console.log(response.body[i]['prescriptiondetail']);
            const resources = JSON.parse(prescriptiondata);
            const resource = resources[i]['name'];
            this.myData = resources ;
            console.log('resources==', resource);
            console.log('data==', response); }
        });
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/prescription'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/prescription', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/prescription', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.appointmentService.query().subscribe((response) => {
        this.appointments = response.body;
        });
        /*this.patientServcie.getPatientByUserId().subscribe((response) => {
            this.patient = response.body;
        });*/
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPrescriptions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Prescription) {
        return item.id;
    }
    registerChangeInPrescriptions() {
        this.eventSubscriber = this.eventManager.subscribe('prescriptionListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.prescriptions = data;
        const prescriptiondata = data[0]['prescriptiondetail'];
        const resources = JSON.parse(prescriptiondata);
        const resource = resources[0]['name'];
        this.myData = resources ;

    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
