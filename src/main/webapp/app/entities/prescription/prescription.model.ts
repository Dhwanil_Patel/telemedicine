import { BaseEntity } from './../../shared';

export class Prescription implements BaseEntity {
    constructor(
        public id?: number,
        public prescriptiondetail?: string,
        public appointmentId?: number,
        public orderMedicineId?: number,
        // public mid?: number,
        // public name?: string,
        // public quantity?: number,
        // public time?: string,
        // public price?: number,
    ) {
    }
}
