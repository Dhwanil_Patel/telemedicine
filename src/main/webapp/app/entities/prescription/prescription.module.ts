import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { AppComponent } from '@angular/core'

import { TelemedicineApplicationSharedModule } from '../../shared';
import {
    PrescriptionService,
    PrescriptionPopupService,
    PrescriptionComponent,
    PrescriptionDetailComponent,
    PrescriptionDialogComponent,
    PrescriptionPopupComponent,
    PrescriptionDeletePopupComponent,
    PrescriptionDeleteDialogComponent,
    prescriptionRoute,
    prescriptionPopupRoute,
    PrescriptionResolvePagingParams,
} from './';
// import {PrescriptionTableComponent} from './prescription-table.component';

const ENTITY_STATES = [
    ...prescriptionRoute,
    ...prescriptionPopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        // AppComponent,
        // ConfirmComponent,
        PrescriptionComponent,
        PrescriptionDetailComponent,
        PrescriptionDialogComponent,
        PrescriptionDeleteDialogComponent,
        PrescriptionPopupComponent,
        PrescriptionDeletePopupComponent,
    ],
    exports: [
        PrescriptionDialogComponent
    ],
    entryComponents: [
        PrescriptionComponent,
        PrescriptionDialogComponent,
        PrescriptionPopupComponent,
        PrescriptionDeleteDialogComponent,
        PrescriptionDeletePopupComponent,
    ],
    providers: [
        PrescriptionService,
        PrescriptionPopupService,
        PrescriptionResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationPrescriptionModule {}
