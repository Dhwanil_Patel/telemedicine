import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule } from '../../shared';
import {
    DegreeService,
    DegreePopupService,
    DegreeComponent,
    DegreeDetailComponent,
    DegreeDialogComponent,
    DegreePopupComponent,
    DegreeDeletePopupComponent,
    DegreeDeleteDialogComponent,
    degreeRoute,
    degreePopupRoute,
    DegreeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...degreeRoute,
    ...degreePopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        DegreeComponent,
        DegreeDetailComponent,
        DegreeDialogComponent,
        DegreeDeleteDialogComponent,
        DegreePopupComponent,
        DegreeDeletePopupComponent,
    ],
    entryComponents: [
        DegreeComponent,
        DegreeDialogComponent,
        DegreePopupComponent,
        DegreeDeleteDialogComponent,
        DegreeDeletePopupComponent,
    ],
    providers: [
        DegreeService,
        DegreePopupService,
        DegreeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationDegreeModule {}
