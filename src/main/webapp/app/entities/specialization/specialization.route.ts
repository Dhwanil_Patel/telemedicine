import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SpecializationComponent } from './specialization.component';
import { SpecializationDetailComponent } from './specialization-detail.component';
import { SpecializationPopupComponent } from './specialization-dialog.component';
import { SpecializationDeletePopupComponent } from './specialization-delete-dialog.component';

@Injectable()
export class SpecializationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const specializationRoute: Routes = [
    {
        path: 'specialization',
        component: SpecializationComponent,
        resolve: {
            'pagingParams': SpecializationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.specialization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'specialization/:id',
        component: SpecializationDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.specialization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const specializationPopupRoute: Routes = [
    {
        path: 'specialization-new',
        component: SpecializationPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.specialization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'specialization/:id/edit',
        component: SpecializationPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.specialization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'specialization/:id/delete',
        component: SpecializationDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.specialization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
