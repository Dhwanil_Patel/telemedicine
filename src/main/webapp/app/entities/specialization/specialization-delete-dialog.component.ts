import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Specialization } from './specialization.model';
import { SpecializationPopupService } from './specialization-popup.service';
import { SpecializationService } from './specialization.service';

@Component({
    selector: 'jhi-specialization-delete-dialog',
    templateUrl: './specialization-delete-dialog.component.html'
})
export class SpecializationDeleteDialogComponent {

    specialization: Specialization;

    constructor(
        private specializationService: SpecializationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.specializationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'specializationListModification',
                content: 'Deleted an specialization'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-specialization-delete-popup',
    template: ''
})
export class SpecializationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private specializationPopupService: SpecializationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.specializationPopupService
                .open(SpecializationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
