import { BaseEntity } from './../../shared';

export class Specialization implements BaseEntity {
    constructor(
        public id?: number,
        public expertise?: string,
    ) {
    }
}
