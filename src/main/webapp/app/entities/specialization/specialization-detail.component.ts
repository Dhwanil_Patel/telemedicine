import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Specialization } from './specialization.model';
import { SpecializationService } from './specialization.service';

@Component({
    selector: 'jhi-specialization-detail',
    templateUrl: './specialization-detail.component.html'
})
export class SpecializationDetailComponent implements OnInit, OnDestroy {

    specialization: Specialization;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private specializationService: SpecializationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSpecializations();
    }

    load(id) {
        this.specializationService.find(id)
            .subscribe((specializationResponse: HttpResponse<Specialization>) => {
                this.specialization = specializationResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSpecializations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'specializationListModification',
            (response) => this.load(this.specialization.id)
        );
    }
}
