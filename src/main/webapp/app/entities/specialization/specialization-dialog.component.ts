import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Specialization } from './specialization.model';
import { SpecializationPopupService } from './specialization-popup.service';
import { SpecializationService } from './specialization.service';

@Component({
    selector: 'jhi-specialization-dialog',
    templateUrl: './specialization-dialog.component.html'
})
export class SpecializationDialogComponent implements OnInit {

    specialization: Specialization;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private specializationService: SpecializationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.specialization.id !== undefined) {
            this.subscribeToSaveResponse(
                this.specializationService.update(this.specialization));
        } else {
            this.subscribeToSaveResponse(
                this.specializationService.create(this.specialization));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Specialization>>) {
        result.subscribe((res: HttpResponse<Specialization>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Specialization) {
        this.eventManager.broadcast({ name: 'specializationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-specialization-popup',
    template: ''
})
export class SpecializationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private specializationPopupService: SpecializationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.specializationPopupService
                    .open(SpecializationDialogComponent as Component, params['id']);
            } else {
                this.specializationPopupService
                    .open(SpecializationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
