export * from './specialization.model';
export * from './specialization-popup.service';
export * from './specialization.service';
export * from './specialization-dialog.component';
export * from './specialization-delete-dialog.component';
export * from './specialization-detail.component';
export * from './specialization.component';
export * from './specialization.route';
