import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule } from '../../shared';
import {
    SpecializationService,
    SpecializationPopupService,
    SpecializationComponent,
    SpecializationDetailComponent,
    SpecializationDialogComponent,
    SpecializationPopupComponent,
    SpecializationDeletePopupComponent,
    SpecializationDeleteDialogComponent,
    specializationRoute,
    specializationPopupRoute,
    SpecializationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...specializationRoute,
    ...specializationPopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SpecializationComponent,
        SpecializationDetailComponent,
        SpecializationDialogComponent,
        SpecializationDeleteDialogComponent,
        SpecializationPopupComponent,
        SpecializationDeletePopupComponent,
    ],
    entryComponents: [
        SpecializationComponent,
        SpecializationDialogComponent,
        SpecializationPopupComponent,
        SpecializationDeleteDialogComponent,
        SpecializationDeletePopupComponent,
    ],
    providers: [
        SpecializationService,
        SpecializationPopupService,
        SpecializationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationSpecializationModule {}
