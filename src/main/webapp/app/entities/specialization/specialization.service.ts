import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Specialization } from './specialization.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Specialization>;

@Injectable()
export class SpecializationService {

    private resourceUrl =  SERVER_API_URL + 'api/specializations';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/specializations';

    constructor(private http: HttpClient) { }

    create(specialization: Specialization): Observable<EntityResponseType> {
        const copy = this.convert(specialization);
        return this.http.post<Specialization>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(specialization: Specialization): Observable<EntityResponseType> {
        const copy = this.convert(specialization);
        return this.http.put<Specialization>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Specialization>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Specialization[]>> {
        const options = createRequestOption(req);
        return this.http.get<Specialization[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Specialization[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Specialization[]>> {
        const options = createRequestOption(req);
        return this.http.get<Specialization[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Specialization[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Specialization = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Specialization[]>): HttpResponse<Specialization[]> {
        const jsonResponse: Specialization[] = res.body;
        const body: Specialization[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Specialization.
     */
    private convertItemFromServer(specialization: Specialization): Specialization {
        const copy: Specialization = Object.assign({}, specialization);
        return copy;
    }

    /**
     * Convert a Specialization to a JSON which can be sent to the server.
     */
    private convert(specialization: Specialization): Specialization {
        const copy: Specialization = Object.assign({}, specialization);
        return copy;
    }
}
