import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TelemedicineApplicationDegreeModule } from './degree/degree.module';
import { TelemedicineApplicationSpecializationModule } from './specialization/specialization.module';
import { TelemedicineApplicationDoctorModule } from './doctor/doctor.module';
import { TelemedicineApplicationPatientModule } from './patient/patient.module';
import { TelemedicineApplicationPharmacistModule } from './pharmacist/pharmacist.module';
import { TelemedicineApplicationDoctorScheduleModule } from './doctor-schedule/doctor-schedule.module';
import { TelemedicineApplicationPatientIssueModule } from './patient-issue/patient-issue.module';
import { TelemedicineApplicationAppointmentModule } from './appointment/appointment.module';
import { TelemedicineApplicationPaymentModule } from './payment/payment.module';
import { TelemedicineApplicationPrescriptionModule } from './prescription/prescription.module';
import { TelemedicineApplicationOrderMedicineModule } from './order-medicine/order-medicine.module';
import { TelemedicineApplicationPharmacistPaymentModule } from './pharmacist-payment/pharmacist-payment.module';
import { TelemedicineApplicationCheckupModule } from './checkup/checkup.module';
import { TelemedicineApplicationReportModule } from './report/report.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TelemedicineApplicationDegreeModule,
        TelemedicineApplicationSpecializationModule,
        TelemedicineApplicationDoctorModule,
        TelemedicineApplicationPatientModule,
        TelemedicineApplicationPharmacistModule,
        TelemedicineApplicationDoctorScheduleModule,
        TelemedicineApplicationPatientIssueModule,
        TelemedicineApplicationAppointmentModule,
        TelemedicineApplicationPaymentModule,
        TelemedicineApplicationPrescriptionModule,
        TelemedicineApplicationOrderMedicineModule,
        TelemedicineApplicationPharmacistPaymentModule,
        TelemedicineApplicationCheckupModule,
        TelemedicineApplicationReportModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationEntityModule {}
