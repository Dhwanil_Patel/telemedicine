import { CheckupComponent } from './checkup.component';
import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../../shared/auth/user-route-access-service';
import { PrescriptionDialogComponent } from '../prescription/prescription-dialog.component';
export const checkupRoute: Routes = [
    {
        path: 'checkup',
        component: CheckupComponent,
        data: {
            authorities: []
        }
    },
    // {
    //     path: 'prescription-new',
    //     component: PrescriptionDialogComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'telemedicineApplicationApp.prescription.home.title'
    //     },
    //     canActivate: [UserRouteAccessService]
    // }
    ];
