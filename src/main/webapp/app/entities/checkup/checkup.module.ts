import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {FormsModule } from '@angular/forms';
import { TelemedicineApplicationSharedModule } from '../../shared';
import { CheckupComponent } from './checkup.component';
import { checkupRoute } from './checkup.route';
import { NgxEditorModule } from 'ngx-editor';
const ENTITY_STATES = [
    ...checkupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        NgxEditorModule

    ],
    declarations: [
        CheckupComponent
    ],
    entryComponents: [
        CheckupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationCheckupModule {}
