import {Component, OnInit} from '@angular/core';
import {Prescription} from '../prescription/prescription.model';
import {Appointment} from '../appointment/appointment.model';
import {AppointmentService} from '../appointment/appointment.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {PrescriptionService} from '../prescription/prescription.service';
import {JhiAlertService, JhiEventManager, JhiParseLinks} from 'ng-jhipster';
import {Patient} from '../patient/patient.model';
import {Subscription} from 'rxjs/Subscription';
import {PatientService} from '../patient/patient.service';
import {Principal} from '../../shared/auth/principal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormsModule } from '@angular/forms';

declare var Skype: any;
// import {EditorModule} from '@tinymce/tinymce-angular';

@Component({
    selector: 'jhi-checkup',
    templateUrl: './checkup.component.html',
    styles: []
})
export class CheckupComponent implements OnInit {
    currentAccount: any;
    prescriptions: Prescription[];
    // prescrip: Prescription;
    appointments: Appointment[];
    medicine: String[];
    patient: Patient;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    title = 'App Works!';

    targetpeer: any;
    peer: any;

    constructor(private prescriptionService: PrescriptionService,
                private appointmentService: AppointmentService,
                private patientServcie: PatientService,
                private parseLinks: JhiParseLinks,
                private jhiAlertService: JhiAlertService,
                private principal: Principal,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private eventManager: JhiEventManager,
                // private editor: EditorModule
                ) {
    }

    loadAll() {
        this.prescriptionService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: HttpResponse<Prescription[]>) => this.onSuccess(res.body, res.headers),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.appointmentService.query().subscribe((response) => {
            this.appointments = response.body;
        });
        this.patientServcie.getPatientByUserId().subscribe((response) => {
            this.patient = response.body;
            console.log( "patient", this.patient);
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPrescriptions();
        /*
        this.peer = new SimplePeer ({
            initiator: location.hash === '#init',
            trickle: false
        });

        this.peer.on('signal', function(data) {
            console.log(JSON.stringify(data));
            this.targetpeer = data;
        });

        this.peer.on('data', function(data) {
            console.log('Recieved message:' + data);
        });
        */
        console.log('skype = ', Skype);
    }

    ngAfterViewInit(){
            Skype.ui({
                "name": "dropdown",
                "vedio": true,
                "element": "SkypeButton_Call_live:ks_1622_1",
                "participants": ["live:ks_1622"]
            });

        Skype.ui({
            "name": "dropdown",
            "vedio": true,
            "element": "SkypeButton_Call_live:mja_281_1",
            "participants": ["live:mja_281"]
        });
    }

    connect() {
        this.peer.signal(JSON.parse(this.targetpeer));
    }

    message() {
        this.peer.send('Hello world');
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.prescriptions = data;
        console.log('prescriptions:', this.prescriptions);
    }

    registerChangeInPrescriptions() {
        this.eventSubscriber = this.eventManager.subscribe('prescriptionListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
