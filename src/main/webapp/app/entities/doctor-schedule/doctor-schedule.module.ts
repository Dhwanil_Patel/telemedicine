import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule } from '../../shared';
import {
    DoctorScheduleService,
    DoctorSchedulePopupService,
    DoctorScheduleComponent,
    DoctorScheduleDetailComponent,
    DoctorScheduleDialogComponent,
    DoctorSchedulePopupComponent,
    DoctorScheduleDeletePopupComponent,
    DoctorScheduleDeleteDialogComponent,
    doctorScheduleRoute,
    doctorSchedulePopupRoute,
    DoctorScheduleResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...doctorScheduleRoute,
    ...doctorSchedulePopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        DoctorScheduleComponent,
        DoctorScheduleDetailComponent,
        DoctorScheduleDialogComponent,
        DoctorScheduleDeleteDialogComponent,
        DoctorSchedulePopupComponent,
        DoctorScheduleDeletePopupComponent,
    ],
    entryComponents: [
        DoctorScheduleComponent,
        DoctorScheduleDialogComponent,
        DoctorSchedulePopupComponent,
        DoctorScheduleDeleteDialogComponent,
        DoctorScheduleDeletePopupComponent,
    ],
    providers: [
        DoctorScheduleService,
        DoctorSchedulePopupService,
        DoctorScheduleResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationDoctorScheduleModule {}
