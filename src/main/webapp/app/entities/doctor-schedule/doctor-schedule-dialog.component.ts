import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DoctorSchedule } from './doctor-schedule.model';
import { DoctorSchedulePopupService } from './doctor-schedule-popup.service';
import { DoctorScheduleService } from './doctor-schedule.service';
import { Doctor, DoctorService } from '../doctor';

@Component({
    selector: 'jhi-doctor-schedule-dialog',
    templateUrl: './doctor-schedule-dialog.component.html'
})
export class DoctorScheduleDialogComponent implements OnInit {

    doctorSchedule: DoctorSchedule;
    isSaving: boolean;

    doctors: Doctor[];
    doctor: Doctor;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private doctorScheduleService: DoctorScheduleService,
        private doctorService: DoctorService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.doctorService.getDoctorByUserId().subscribe((response) => {
            this.doctor = response.body;
        });
        // this.doctorService.query()
        //     .subscribe((res: HttpResponse<Doctor[]>) => { this.doctors = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.doctorService.query()
            .subscribe((res: HttpResponse<Doctor[]>) => { this.doctors = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.doctorSchedule.id !== undefined) {
            this.doctorSchedule.doctorId = this.doctor.id;
            this.subscribeToSaveResponse(
                this.doctorScheduleService.update(this.doctorSchedule));
        } else {
            this.doctorSchedule.doctorId = this.doctor.id;
            this.subscribeToSaveResponse(
                this.doctorScheduleService.create(this.doctorSchedule));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<DoctorSchedule>>) {
        result.subscribe((res: HttpResponse<DoctorSchedule>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: DoctorSchedule) {
        this.eventManager.broadcast({ name: 'doctorScheduleListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackDoctorById(index: number, item: Doctor) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-doctor-schedule-popup',
    template: ''
})
export class DoctorSchedulePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private doctorSchedulePopupService: DoctorSchedulePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.doctorSchedulePopupService
                    .open(DoctorScheduleDialogComponent as Component, params['id']);
            } else {
                this.doctorSchedulePopupService
                    .open(DoctorScheduleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
