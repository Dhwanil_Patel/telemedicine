import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DoctorSchedule } from './doctor-schedule.model';
import { DoctorSchedulePopupService } from './doctor-schedule-popup.service';
import { DoctorScheduleService } from './doctor-schedule.service';

@Component({
    selector: 'jhi-doctor-schedule-delete-dialog',
    templateUrl: './doctor-schedule-delete-dialog.component.html'
})
export class DoctorScheduleDeleteDialogComponent {

    doctorSchedule: DoctorSchedule;

    constructor(
        private doctorScheduleService: DoctorScheduleService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.doctorScheduleService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'doctorScheduleListModification',
                content: 'Deleted an doctorSchedule'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-doctor-schedule-delete-popup',
    template: ''
})
export class DoctorScheduleDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private doctorSchedulePopupService: DoctorSchedulePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.doctorSchedulePopupService
                .open(DoctorScheduleDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
