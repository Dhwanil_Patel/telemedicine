import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { DoctorSchedule } from './doctor-schedule.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<DoctorSchedule>;

@Injectable()
export class DoctorScheduleService {

    private resourceUrl =  SERVER_API_URL + 'api/doctor-schedules';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/doctor-schedules';
    private resourceDoctorUrl = SERVER_API_URL + 'api/doctor-schedules/DoctorId';

    constructor(private http: HttpClient) { }
    // get doctorschedule by doctor id
    getScheduleByDoctorId(): Observable<HttpResponse<DoctorSchedule[]>> {
        return this.http.get<DoctorSchedule[]>(`${this.resourceDoctorUrl}`, { observe: 'response'})
            .map((res: HttpResponse<DoctorSchedule[]>) => this.convertArrayResponse(res));
    }
    create(doctorSchedule: DoctorSchedule): Observable<EntityResponseType> {
        const copy = this.convert(doctorSchedule);
        return this.http.post<DoctorSchedule>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(doctorSchedule: DoctorSchedule): Observable<EntityResponseType> {
        const copy = this.convert(doctorSchedule);
        return this.http.put<DoctorSchedule>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<DoctorSchedule>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<DoctorSchedule[]>> {
        const options = createRequestOption(req);
        return this.http.get<DoctorSchedule[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<DoctorSchedule[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<DoctorSchedule[]>> {
        const options = createRequestOption(req);
        return this.http.get<DoctorSchedule[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<DoctorSchedule[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: DoctorSchedule = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<DoctorSchedule[]>): HttpResponse<DoctorSchedule[]> {
        const jsonResponse: DoctorSchedule[] = res.body;
        const body: DoctorSchedule[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to DoctorSchedule.
     */
    private convertItemFromServer(doctorSchedule: DoctorSchedule): DoctorSchedule {
        const copy: DoctorSchedule = Object.assign({}, doctorSchedule);
        return copy;
    }

    /**
     * Convert a DoctorSchedule to a JSON which can be sent to the server.
     */
    private convert(doctorSchedule: DoctorSchedule): DoctorSchedule {
        const copy: DoctorSchedule = Object.assign({}, doctorSchedule);
        return copy;
    }
}
