import { BaseEntity } from './../../shared';

export const enum Days {
    'SUNDAY',
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY'
}

export const enum Schedule_Status {
    'ENABLE',
    'DISABLE'
}

export class DoctorSchedule implements BaseEntity {
    constructor(
        public id?: number,
        public workingday?: Days,
        public fromtime?: string,
        public totime?: string,
        public noofpatient?: number,
        public status?: Schedule_Status,
        public doctorId?: number,
    ) {
    }
}
