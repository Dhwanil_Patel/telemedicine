import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { DoctorSchedule } from './doctor-schedule.model';
import { DoctorScheduleService } from './doctor-schedule.service';

@Component({
    selector: 'jhi-doctor-schedule-detail',
    templateUrl: './doctor-schedule-detail.component.html'
})
export class DoctorScheduleDetailComponent implements OnInit, OnDestroy {

    doctorSchedule: DoctorSchedule;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private doctorScheduleService: DoctorScheduleService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDoctorSchedules();
    }

    load(id) {
        this.doctorScheduleService.find(id)
            .subscribe((doctorScheduleResponse: HttpResponse<DoctorSchedule>) => {
                this.doctorSchedule = doctorScheduleResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDoctorSchedules() {
        this.eventSubscriber = this.eventManager.subscribe(
            'doctorScheduleListModification',
            (response) => this.load(this.doctorSchedule.id)
        );
    }
}
