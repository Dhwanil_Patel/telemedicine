import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { DoctorScheduleComponent } from './doctor-schedule.component';
import { DoctorScheduleDetailComponent } from './doctor-schedule-detail.component';
import { DoctorSchedulePopupComponent } from './doctor-schedule-dialog.component';
import { DoctorScheduleDeletePopupComponent } from './doctor-schedule-delete-dialog.component';

@Injectable()
export class DoctorScheduleResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const doctorScheduleRoute: Routes = [
    {
        path: 'doctor-schedule',
        component: DoctorScheduleComponent,
        resolve: {
            'pagingParams': DoctorScheduleResolvePagingParams
        },
        data: {
            authorities: ['ROLE_DOCTOR', 'ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.doctorSchedule.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'doctor-schedule/:id',
        component: DoctorScheduleDetailComponent,
        data: {
            authorities: ['ROLE_DOCTOR', 'ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.doctorSchedule.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const doctorSchedulePopupRoute: Routes = [
    {
        path: 'doctor-schedule-new',
        component: DoctorSchedulePopupComponent,
        data: {
            authorities: ['ROLE_DOCTOR'],
            pageTitle: 'telemedicineApplicationApp.doctorSchedule.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'doctor-schedule/:id/edit',
        component: DoctorSchedulePopupComponent,
        data: {
            authorities: ['ROLE_DOCTOR'],
            pageTitle: 'telemedicineApplicationApp.doctorSchedule.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'doctor-schedule/:id/delete',
        component: DoctorScheduleDeletePopupComponent,
        data: {
            authorities: ['ROLE_DOCTOR'],
            pageTitle: 'telemedicineApplicationApp.doctorSchedule.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
