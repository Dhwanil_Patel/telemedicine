import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Doctor } from './doctor.model';
import { DoctorPopupService } from './doctor-popup.service';
import { DoctorService } from './doctor.service';
import { Degree, DegreeService } from '../degree';
import { Specialization, SpecializationService } from '../specialization';
import { User } from '../../shared/user/user.model';
import { UserService } from '../../shared/user/user.service';
import { Account } from '../../shared/user/account.model';
import { Principal } from '../../shared/auth/principal.service';

@Component({
    selector: 'jhi-doctor-dialog',
    templateUrl: './doctor-dialog.component.html'
})
export class DoctorDialogComponent implements OnInit {

    doctor: Doctor;
    isSaving: boolean;
    account: Account;
    degrees: Degree[];
    users: User[];
    specializations: Specialization[];

    constructor(
        public activeModal: NgbActiveModal,
        private principal: Principal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private doctorService: DoctorService,
        private userService: UserService,
        private degreeService: DegreeService,
        private specializationService: SpecializationService,
        private elementRef: ElementRef,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.principal.identity().then((account) => {
            this.account = account;
            this.doctor.userId = account.id;
            // console.log( 'usertype:',this.account.usertype);
    });
        this.registerAuthenticationSuccess();
        this.degreeService.query()
            .subscribe((res: HttpResponse<Degree[]>) => { this.degrees = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.specializationService.query()
            .subscribe((res: HttpResponse<Specialization[]>) => { this.specializations = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }
    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

   /* upload(contentType) {
        console.log('contenttype is:', contentType);
        if((contentType === 'image/jpg') || (contentType === 'image/jpeg') || (contentType === 'image/png')) {
            console.log('contenttype is:', contentType);
        } else {
            alert('filetype not supported.');
        }
    }*/
    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.doctor, this.elementRef, field, fieldContentType, idInput);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        console.log('doctor = ', this.doctor);
        if (this.doctor.id !== undefined) {
            this.subscribeToSaveResponse(
                this.doctorService.update(this.doctor));
        } else {
            this.subscribeToSaveResponse(
                this.doctorService.create(this.doctor));
        }

    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Doctor>>) {
        result.subscribe((res: HttpResponse<Doctor>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Doctor) {
        this.eventManager.broadcast({ name: 'doctorListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackDegreeById(index: number, item: Degree) {
        return item.id;
    }

    trackSpecializationById(index: number, item: Specialization) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-doctor-popup',
    template: ''
})
export class DoctorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;
    account: Account;

    constructor(
        private route: ActivatedRoute,
        private principal: Principal,
        private doctorPopupService: DoctorPopupService,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.doctorPopupService
                    .open(DoctorDialogComponent as Component, params['id']);
            } else {
                this.doctorPopupService
                    .open(DoctorDialogComponent as Component);
            }
        });
    }
    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }
    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
