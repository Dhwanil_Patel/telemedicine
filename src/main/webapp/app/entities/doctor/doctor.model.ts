import { BaseEntity } from './../../shared';

export const enum Process_Status {
    'ACTIVE',
    'DEACTIVE'
}

export const enum Gender {
    'MALE',
    'FEMALE',
    'OTHER'
}

export class Doctor implements BaseEntity {
    constructor(
        public id?: number,
        public firstname?: string,
        public lastname?: string,
        public status?: Process_Status,
        public photoContentType?: string,
        public photo?: any,
        public gender?: Gender,
        public dateofbirth?: any,
        public address?: string,
        public mobileno?: string,
        public bloodgroup?: string,
        public exprience?: number,
        public licenceno?: string,
        public documentsContentType?: string,
        public documents?: any,
        public googleplusid?: string,
        public facebookid?: string,
        public twitterid?: string,
        public consultationcharge?: number,
        public degreeId?: number,
        public specializationId?: number,
        public userId?: number,
        public skype?: string,
        public city?: string,
        public state?: string,
        public country?: string
    ) {
    }
}
