import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PharmacistPaymentComponent } from './pharmacist-payment.component';
import { PharmacistPaymentDetailComponent } from './pharmacist-payment-detail.component';
import { PharmacistPaymentPopupComponent } from './pharmacist-payment-dialog.component';
import { PharmacistPaymentDeletePopupComponent } from './pharmacist-payment-delete-dialog.component';

@Injectable()
export class PharmacistPaymentResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const pharmacistPaymentRoute: Routes = [
    {
        path: 'pharmacist-payment',
        component: PharmacistPaymentComponent,
        resolve: {
            'pagingParams': PharmacistPaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PATIENT', 'ROLE_PHARMACIST'],
            pageTitle: 'telemedicineApplicationApp.pharmacistPayment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'pharmacist-payment/:id',
        component: PharmacistPaymentDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PATIENT', 'ROLE_PHARMACIST'],
            pageTitle: 'telemedicineApplicationApp.pharmacistPayment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pharmacistPaymentPopupRoute: Routes = [
    {
        path: 'pharmacist-payment-new',
        component: PharmacistPaymentPopupComponent,
        data: {
            authorities: ['ROLE_PATIENT'],
            pageTitle: 'telemedicineApplicationApp.pharmacistPayment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pharmacist-payment/:id/edit',
        component: PharmacistPaymentPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.pharmacistPayment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pharmacist-payment/:id/delete',
        component: PharmacistPaymentDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.pharmacistPayment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
