import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { PharmacistPayment } from './pharmacist-payment.model';
import { PharmacistPaymentService } from './pharmacist-payment.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { Pharmacist } from '../pharmacist/pharmacist.model';
import { PharmacistService } from '../pharmacist/pharmacist.service';
import { PatientService } from '../patient/patient.service';
import { Patient } from '../patient/patient.model';

@Component({
    selector: 'jhi-pharmacist-payment',
    templateUrl: './pharmacist-payment.component.html'
})
export class PharmacistPaymentComponent implements OnInit, OnDestroy {

    currentAccount: any;
    account: Account;
    pharmacistPayments: PharmacistPayment[];
    pharmacist_payments: PharmacistPayment[];
    patient_payments: PharmacistPayment[];
    pharmacistId: number;
    patientId: number;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    pharmacist: Pharmacist;
    patient: Patient;

    constructor(
        private pharmacistPaymentService: PharmacistPaymentService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private pharmacistService: PharmacistService,
        private patientServcie: PatientService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.pharmacistPaymentService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: HttpResponse<PharmacistPayment[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
            return;
        }
        this.pharmacistPaymentService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: HttpResponse<PharmacistPayment[]>) => this.onSuccess(res.body, res.headers),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.principal.identity().then((account) => {
            this.account = account;
            if (account.usertype === 'Pharmacist') {
                //  get pharmacistid
                this.pharmacistService.getPharmacistByUserId().subscribe((response) => {
                    this.pharmacist = response.body;
                    this.pharmacistId = this.pharmacist.id;
                    this.loadPaymentByPharmacistID();
                });
            }
            if (account.usertype === 'Patient') {
                // get patientid
                this.patientServcie.getPatientByUserId().subscribe((response) => {
                    this.patient = response.body;
                    this.patientId = this.patient.id;
                    this.loadPaymentByPatientID();
                });
            }
        });
    }
    // loadPaymentByPharmacistID
    loadPaymentByPharmacistID() {
        this.pharmacistPaymentService.getPaymentByPharmacistId(this.pharmacistId).subscribe((response) => {
        this.pharmacist_payments = response.body;
        });
    }
    // loadPaymentByPatientID
    loadPaymentByPatientID() {
        this.pharmacistPaymentService.getPaymentByPatienttId(this.patientId).subscribe((response) => {
            this.patient_payments = response.body;
        });
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/pharmacist-payment'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/pharmacist-payment', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/pharmacist-payment', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPharmacistPayments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PharmacistPayment) {
        return item.id;
    }
    registerChangeInPharmacistPayments() {
        this.eventSubscriber = this.eventManager.subscribe('pharmacistPaymentListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.pharmacistPayments = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
