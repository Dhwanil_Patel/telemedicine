import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PharmacistPayment } from './pharmacist-payment.model';
import { PharmacistPaymentPopupService } from './pharmacist-payment-popup.service';
import { PharmacistPaymentService } from './pharmacist-payment.service';

@Component({
    selector: 'jhi-pharmacist-payment-delete-dialog',
    templateUrl: './pharmacist-payment-delete-dialog.component.html'
})
export class PharmacistPaymentDeleteDialogComponent {

    pharmacistPayment: PharmacistPayment;

    constructor(
        private pharmacistPaymentService: PharmacistPaymentService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pharmacistPaymentService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pharmacistPaymentListModification',
                content: 'Deleted an pharmacistPayment'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pharmacist-payment-delete-popup',
    template: ''
})
export class PharmacistPaymentDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pharmacistPaymentPopupService: PharmacistPaymentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pharmacistPaymentPopupService
                .open(PharmacistPaymentDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
