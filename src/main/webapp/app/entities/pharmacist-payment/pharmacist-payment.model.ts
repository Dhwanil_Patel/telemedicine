import { BaseEntity } from './../../shared';

export const enum Payment_Status {
    'PAID',
    'UNPAID'
}

export class PharmacistPayment implements BaseEntity {
    constructor(
        public id?: number,
        public paymentDate?: any,
        public totalamount?: number,
        public paymentmode?: string,
        public paymentStatus?: Payment_Status,
        public transcationid?: string,
        public orderMedicineId?: number,
    ) {
    }
}
