import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { PharmacistPayment } from './pharmacist-payment.model';
import { PharmacistPaymentService } from './pharmacist-payment.service';

@Injectable()
export class PharmacistPaymentPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private pharmacistPaymentService: PharmacistPaymentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.pharmacistPaymentService.find(id)
                    .subscribe((pharmacistPaymentResponse: HttpResponse<PharmacistPayment>) => {
                        const pharmacistPayment: PharmacistPayment = pharmacistPaymentResponse.body;
                        if (pharmacistPayment.paymentDate) {
                            pharmacistPayment.paymentDate = {
                                year: pharmacistPayment.paymentDate.getFullYear(),
                                month: pharmacistPayment.paymentDate.getMonth() + 1,
                                day: pharmacistPayment.paymentDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.pharmacistPaymentModalRef(component, pharmacistPayment);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.pharmacistPaymentModalRef(component, new PharmacistPayment());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    pharmacistPaymentModalRef(component: Component, pharmacistPayment: PharmacistPayment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.pharmacistPayment = pharmacistPayment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
