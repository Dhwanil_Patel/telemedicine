import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PharmacistPayment } from './pharmacist-payment.model';
import { PharmacistPaymentService } from './pharmacist-payment.service';

@Component({
    selector: 'jhi-pharmacist-payment-detail',
    templateUrl: './pharmacist-payment-detail.component.html'
})
export class PharmacistPaymentDetailComponent implements OnInit, OnDestroy {

    pharmacistPayment: PharmacistPayment;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private pharmacistPaymentService: PharmacistPaymentService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPharmacistPayments();
    }

    load(id) {
        this.pharmacistPaymentService.find(id)
            .subscribe((pharmacistPaymentResponse: HttpResponse<PharmacistPayment>) => {
                this.pharmacistPayment = pharmacistPaymentResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPharmacistPayments() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pharmacistPaymentListModification',
            (response) => this.load(this.pharmacistPayment.id)
        );
    }
}
