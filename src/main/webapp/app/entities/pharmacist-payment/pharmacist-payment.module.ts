import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule } from '../../shared';
import {
    PharmacistPaymentService,
    PharmacistPaymentPopupService,
    PharmacistPaymentComponent,
    PharmacistPaymentDetailComponent,
    PharmacistPaymentDialogComponent,
    PharmacistPaymentPopupComponent,
    PharmacistPaymentDeletePopupComponent,
    PharmacistPaymentDeleteDialogComponent,
    pharmacistPaymentRoute,
    pharmacistPaymentPopupRoute,
    PharmacistPaymentResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...pharmacistPaymentRoute,
    ...pharmacistPaymentPopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PharmacistPaymentComponent,
        PharmacistPaymentDetailComponent,
        PharmacistPaymentDialogComponent,
        PharmacistPaymentDeleteDialogComponent,
        PharmacistPaymentPopupComponent,
        PharmacistPaymentDeletePopupComponent,
    ],
    entryComponents: [
        PharmacistPaymentComponent,
        PharmacistPaymentDialogComponent,
        PharmacistPaymentPopupComponent,
        PharmacistPaymentDeleteDialogComponent,
        PharmacistPaymentDeletePopupComponent,
    ],
    providers: [
        PharmacistPaymentService,
        PharmacistPaymentPopupService,
        PharmacistPaymentResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationPharmacistPaymentModule {}
