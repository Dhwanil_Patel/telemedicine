import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PharmacistPayment } from './pharmacist-payment.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<PharmacistPayment>;

@Injectable()
export class PharmacistPaymentService {

    private resourceUrl =  SERVER_API_URL + 'api/pharmacist-payments';
    private resoucePharmacistUrl = SERVER_API_URL + 'api/pharmacist-payments/pharmacist';
    private resoucePatientUrl = SERVER_API_URL + 'api/pharmacist-payments/patient';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/pharmacist-payments';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(pharmacistPayment: PharmacistPayment): Observable<EntityResponseType> {
        const copy = this.convert(pharmacistPayment);
        return this.http.post<PharmacistPayment>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

// create new Service
    getPaymentByPharmacistId(id: number): Observable<HttpResponse<PharmacistPayment[]>> {
        return this.http.get<PharmacistPayment[]>(`${this.resoucePharmacistUrl}/${id}`, { observe: 'response'})
        .map((res: HttpResponse<PharmacistPayment[]>) => this.convertArrayResponse(res));
    }

// create new Service
    getPaymentByPatienttId(id: number): Observable<HttpResponse<PharmacistPayment[]>> {
        return this.http.get<PharmacistPayment[]>(`${this.resoucePatientUrl}/${id}`, { observe: 'response'})
            .map((res: HttpResponse<PharmacistPayment[]>) => this.convertArrayResponse(res));
    }

    update(pharmacistPayment: PharmacistPayment): Observable<EntityResponseType> {
        const copy = this.convert(pharmacistPayment);
        return this.http.put<PharmacistPayment>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<PharmacistPayment>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<PharmacistPayment[]>> {
        const options = createRequestOption(req);
        return this.http.get<PharmacistPayment[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PharmacistPayment[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<PharmacistPayment[]>> {
        const options = createRequestOption(req);
        return this.http.get<PharmacistPayment[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PharmacistPayment[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: PharmacistPayment = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<PharmacistPayment[]>): HttpResponse<PharmacistPayment[]> {
        const jsonResponse: PharmacistPayment[] = res.body;
        const body: PharmacistPayment[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to PharmacistPayment.
     */
    private convertItemFromServer(pharmacistPayment: PharmacistPayment): PharmacistPayment {
        const copy: PharmacistPayment = Object.assign({}, pharmacistPayment);
        copy.paymentDate = this.dateUtils
            .convertLocalDateFromServer(pharmacistPayment.paymentDate);
        return copy;
    }

    /**
     * Convert a PharmacistPayment to a JSON which can be sent to the server.
     */
    private convert(pharmacistPayment: PharmacistPayment): PharmacistPayment {
        const copy: PharmacistPayment = Object.assign({}, pharmacistPayment);
        copy.paymentDate = this.dateUtils
            .convertLocalDateToServer(pharmacistPayment.paymentDate);
        return copy;
    }
}
