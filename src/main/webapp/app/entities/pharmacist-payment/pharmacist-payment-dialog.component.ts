import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PharmacistPayment } from './pharmacist-payment.model';
import { PharmacistPaymentPopupService } from './pharmacist-payment-popup.service';
import { PharmacistPaymentService } from './pharmacist-payment.service';
import { OrderMedicine, OrderMedicineService } from '../order-medicine';

@Component({
    selector: 'jhi-pharmacist-payment-dialog',
    templateUrl: './pharmacist-payment-dialog.component.html'
})
export class PharmacistPaymentDialogComponent implements OnInit {

    pharmacistPayment: PharmacistPayment;
    isSaving: boolean;
    ordermedicines: OrderMedicine[];
    order_medicines: OrderMedicine[];
    paymentDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private pharmacistPaymentService: PharmacistPaymentService,
        private orderMedicineService: OrderMedicineService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.orderMedicineService
            .getOrderMedicineByPatientId({filter: 'pharmacistpayment-is-null'})
            .subscribe((response) => {
                this.ordermedicines = response.body;
            });
        // this.orderMedicineService
        //     .query({filter: 'pharmacistpayment-is-null'})
        //     .subscribe((res: HttpResponse<OrderMedicine[]>) => {
        //         if (!this.pharmacistPayment.orderMedicineId) {
        //             this.ordermedicines = res.body;
        //         } else {
        //             this.orderMedicineService
        //                 .find(this.pharmacistPayment.orderMedicineId)
        //                 .subscribe((subRes: HttpResponse<OrderMedicine>) => {
        //                     this.ordermedicines = [subRes.body].concat(res.body);
        //                 }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
        //         }
        //     }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pharmacistPayment.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pharmacistPaymentService.update(this.pharmacistPayment));
        } else {
            this.subscribeToSaveResponse(
                this.pharmacistPaymentService.create(this.pharmacistPayment));
        }
    }
    // get amount by ordermedicineid
        getTotalAmount() {
        this.order_medicines = this.ordermedicines.filter((ordermedicines) => {
            return ordermedicines.id === this.pharmacistPayment.orderMedicineId;

        });
        this.pharmacistPayment.totalamount = this.order_medicines[0].totalprice;
        console.log( 'order_medicine', this.order_medicines);
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PharmacistPayment>>) {
        result.subscribe((res: HttpResponse<PharmacistPayment>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: PharmacistPayment) {
        this.eventManager.broadcast({ name: 'pharmacistPaymentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOrderMedicineById(index: number, item: OrderMedicine) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-pharmacist-payment-popup',
    template: ''
})
export class PharmacistPaymentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pharmacistPaymentPopupService: PharmacistPaymentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pharmacistPaymentPopupService
                    .open(PharmacistPaymentDialogComponent as Component, params['id']);
            } else {
                this.pharmacistPaymentPopupService
                    .open(PharmacistPaymentDialogComponent as Component);
            }
        });
    }
    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
