import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PatientIssue } from './patient-issue.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<PatientIssue>;

@Injectable()
export class PatientIssueService {

    private resourceUrl =  SERVER_API_URL + 'api/patient-issue';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/patient-issue';

    constructor(private http: HttpClient) { }

    create(patientIssue: PatientIssue): Observable<EntityResponseType> {
        const copy = this.convert(patientIssue);
        return this.http.post<PatientIssue>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(patientIssue: PatientIssue): Observable<EntityResponseType> {
        const copy = this.convert(patientIssue);
        return this.http.put<PatientIssue>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<PatientIssue>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<PatientIssue[]>> {
        const options = createRequestOption(req);
        return this.http.get<PatientIssue[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PatientIssue[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<PatientIssue[]>> {
        const options = createRequestOption(req);
        return this.http.get<PatientIssue[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PatientIssue[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: PatientIssue = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<PatientIssue[]>): HttpResponse<PatientIssue[]> {
        const jsonResponse: PatientIssue[] = res.body;
        const body: PatientIssue[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to PatientIssue.
     */
    private convertItemFromServer(patientIssue: PatientIssue): PatientIssue {
        const copy: PatientIssue = Object.assign({}, patientIssue);
        return copy;
    }

    /**
     * Convert a PatientIssue to a JSON which can be sent to the server.
     */
    private convert(patientIssue: PatientIssue): PatientIssue {
        const copy: PatientIssue = Object.assign({}, patientIssue);
        return copy;
    }
}
