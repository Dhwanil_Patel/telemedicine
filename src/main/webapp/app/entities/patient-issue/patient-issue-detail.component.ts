import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PatientIssue } from './patient-issue.model';
import { PatientIssueService } from './patient-issue.service';

@Component({
    selector: 'jhi-patient-issue-detail',
    templateUrl: './patient-issue-detail.component.html'
})
export class PatientIssueDetailComponent implements OnInit, OnDestroy {

    PatientIssue: PatientIssue;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private patientIssueService: PatientIssueService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPatientIssue();
    }

    load(id) {
        this.patientIssueService.find(id)
            .subscribe((PatientIssueResponse: HttpResponse<PatientIssue>) => {
                this.PatientIssue = PatientIssueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPatientIssue() {
        this.eventSubscriber = this.eventManager.subscribe(
            'PatientIssueListModification',
            (response) => this.load(this.PatientIssue.id)
        );
    }
}
