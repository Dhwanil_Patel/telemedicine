import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PatientIssueComponent } from './patient-issue.component';
import { PatientIssueDetailComponent } from './patient-issue-detail.component';
import { PatientIssuePopupComponent } from './patient-issue-dialog.component';
import { PatientIssueDeletePopupComponent } from './patient-issue-delete-dialog.component';

@Injectable()
export class PatientIssueResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const PatientIssueRoute: Routes = [
    {
        path: 'patient-issue',
        component: PatientIssueComponent,
        resolve: {
            'pagingParams': PatientIssueResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.PatientIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'patient-issue/:id',
        component: PatientIssueDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.PatientIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const PatientIssuePopupRoute: Routes = [
    {
        path: 'patient-issue-new',
        component: PatientIssuePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.PatientIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'patient-issue/:id/edit',
        component: PatientIssuePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.PatientIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'patient-issue/:id/delete',
        component: PatientIssueDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'telemedicineApplicationApp.PatientIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
