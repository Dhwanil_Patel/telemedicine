import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PatientIssue } from './patient-issue.model';
import { PatientIssuePopupService } from './patient-issue-popup.service';
import { PatientIssueService } from './patient-issue.service';

@Component({
    selector: 'jhi-patient-issue-delete-dialog',
    templateUrl: './patient-issue-delete-dialog.component.html'
})
export class PatientIssueDeleteDialogComponent {

    patientIssue: PatientIssue;

    constructor(
        private patientIssueService: PatientIssueService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.patientIssueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'PatientIssueListModification',
                content: 'Deleted an PatientIssue'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-patient-issue-delete-popup',
    template: ''
})
export class PatientIssueDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private patientIssuePopupService: PatientIssuePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.patientIssuePopupService
                .open(PatientIssueDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
