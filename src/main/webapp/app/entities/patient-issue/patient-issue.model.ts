import { BaseEntity } from './../../shared';

export class PatientIssue implements BaseEntity {
    constructor(
        public id?: number,
        public issue?: string,
    ) {
    }
}
