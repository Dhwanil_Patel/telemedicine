import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule  } from '../../shared';
import {
    PatientIssueService,
    PatientIssuePopupService,
    PatientIssueComponent,
    PatientIssueDetailComponent,
    PatientIssueDialogComponent,
    PatientIssuePopupComponent,
    PatientIssueDeletePopupComponent,
    PatientIssueDeleteDialogComponent,
    PatientIssueRoute,
    PatientIssuePopupRoute,
    PatientIssueResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...PatientIssueRoute,
    ...PatientIssuePopupRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PatientIssueComponent,
        PatientIssueDetailComponent,
        PatientIssueDialogComponent,
        PatientIssueDeleteDialogComponent,
        PatientIssuePopupComponent,
        PatientIssueDeletePopupComponent,
    ],
    entryComponents: [
        PatientIssueComponent,
        PatientIssueDialogComponent,
        PatientIssuePopupComponent,
        PatientIssueDeleteDialogComponent,
        PatientIssueDeletePopupComponent,
    ],
    providers: [
        PatientIssueService,
        PatientIssuePopupService,
        PatientIssueResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationPatientIssueModule {}
