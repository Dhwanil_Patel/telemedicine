import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PatientIssue } from './patient-issue.model';
import { PatientIssuePopupService } from './patient-issue-popup.service';
import { PatientIssueService } from './patient-issue.service';

@Component({
    selector: 'jhi-patient-issue-dialog',
    templateUrl: './patient-issue-dialog.component.html'
})
export class PatientIssueDialogComponent implements OnInit {

    patientIssue: PatientIssue;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private patientIssueService: PatientIssueService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.patientIssue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.patientIssueService.update(this.patientIssue));
        } else {
            this.subscribeToSaveResponse(
                this.patientIssueService.create(this.patientIssue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PatientIssue>>) {
        result.subscribe((res: HttpResponse<PatientIssue>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: PatientIssue) {
        this.eventManager.broadcast({ name: 'patientIssueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-patient-issue-popup',
    template: ''
})
export class PatientIssuePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private patientIssuePopupService: PatientIssuePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.patientIssuePopupService
                    .open(PatientIssueDialogComponent as Component, params['id']);
            } else {
                this.patientIssuePopupService
                    .open(PatientIssueDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
