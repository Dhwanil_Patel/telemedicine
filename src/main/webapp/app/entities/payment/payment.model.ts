import { BaseEntity } from './../../shared';

export class Payment implements BaseEntity {
    constructor(
        public id?: number,
        public paymentdate?: any,
        public totalamount?: number,
        public paymentmethod?: string,
        public transactionid?: string,
        public appointmentId?: number,
    ) {
    }
}
