import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Payment } from './payment.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Payment>;

@Injectable()
export class PaymentService {

    private resourceUrl =  SERVER_API_URL + 'api/payments';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/payments';
    private resourceDoctorUrl = SERVER_API_URL + 'api/payments/doctor';
    private resourcePatientUrl = SERVER_API_URL + 'api/payments/patient';
    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(payment: Payment): Observable<EntityResponseType> {
        const copy = this.convert(payment);
        return this.http.post<Payment>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    // create new Service
    getPaymentByDoctorId(id: number): Observable<HttpResponse<Payment[]>> {
        return this.http.get<Payment[]>(`${this.resourceDoctorUrl}/${id}`, { observe: 'response'})
            .map((res: HttpResponse<Payment[]>) => this.convertArrayResponse(res));
    }

// create new Service
    getPaymentByPatientId(id: number): Observable<HttpResponse<Payment[]>> {
        return this.http.get<Payment[]>(`${this.resourcePatientUrl}/${id}`, { observe: 'response'})
            .map((res: HttpResponse<Payment[]>) => this.convertArrayResponse(res));
    }

    update(payment: Payment): Observable<EntityResponseType> {
        const copy = this.convert(payment);
        return this.http.put<Payment>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Payment>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Payment[]>> {
        const options = createRequestOption(req);
        return this.http.get<Payment[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Payment[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Payment[]>> {
        const options = createRequestOption(req);
        return this.http.get<Payment[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Payment[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Payment = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Payment[]>): HttpResponse<Payment[]> {
        const jsonResponse: Payment[] = res.body;
        const body: Payment[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Payment.
     */
    private convertItemFromServer(payment: Payment): Payment {
        const copy: Payment = Object.assign({}, payment);
        copy.paymentdate = this.dateUtils
            .convertLocalDateFromServer(payment.paymentdate);
        return copy;
    }

    /**
     * Convert a Payment to a JSON which can be sent to the server.
     */
    private convert(payment: Payment): Payment {
        const copy: Payment = Object.assign({}, payment);
        copy.paymentdate = this.dateUtils
            .convertLocalDateToServer(payment.paymentdate);
        return copy;
    }
}
