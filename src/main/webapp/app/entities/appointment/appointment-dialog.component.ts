import {Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Appointment } from './appointment.model';
import { AppointmentPopupService } from './appointment-popup.service';
import { AppointmentService } from './appointment.service';
import { Doctor, DoctorService } from '../doctor';
import { Patient, PatientService } from '../patient';
import { Specialization, SpecializationService } from '../specialization';
import { PatientIssue, PatientIssueService } from '../patient-issue';
import { DoctorSchedule, DoctorScheduleService } from '../doctor-schedule';
import { Payment, PaymentService } from '../payment';
import { Prescription, PrescriptionService } from '../prescription';
// import { MatSelectModule } from '@angular/material';

// For Design
import {FormControl, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'jhi-appointment-dialog',
    templateUrl: './appointment-dialog.component.html'
})

@NgModule({
    imports: [],
})

export class AppointmentDialogComponent implements OnInit {
    appointment: Appointment;
    isSaving: boolean;

    // myForm: FormGroup;
    patientControl =  new FormControl('', [Validators.required]);
    patients: Patient[];
    patient: Patient;
    // Add Doctor entity for filtering
    doctors: Doctor[];
    display_doctors: Doctor[];
    // Add Spacialization Id
    specialization: Specialization[];
    patientissues: PatientIssue[];
    // Add Doctor Schedule
    doctorschedules: DoctorSchedule[];
    display_schedules: DoctorSchedule[];
    // Relative Module
    payments: Payment[];
    prescriptions: Prescription[];
    appointmentdateDp: any;
    // Added Menually For Dropdown Manage
    specializationId;
    doctorId;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private appointmentService: AppointmentService,
        private doctorService: DoctorService,
        private patientService: PatientService,
        private specializationService: SpecializationService,
        private patientIssuesService: PatientIssueService,
        private doctorScheduleService: DoctorScheduleService,
        private paymentService: PaymentService,
        private prescriptionService: PrescriptionService,
        private eventManager: JhiEventManager,
    ) {
    }

    ngOnInit() {
        // this.patientControl = this.formBuilder.control('', [Validators.required]);
        this.isSaving = false;
        this.doctorService.query()
            .subscribe((res: HttpResponse<Doctor[]>) => { this.doctors = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.patientService.query()
            .subscribe((res: HttpResponse<Patient[]>) => { this.patients = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.specializationService.query()
            .subscribe((res: HttpResponse<Specialization[]>) => { this.specialization = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.patientIssuesService.query()
            .subscribe((res: HttpResponse<PatientIssue[]>) => { this.patientissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.doctorScheduleService.query()
            .subscribe((res: HttpResponse<DoctorSchedule[]>) => { this.doctorschedules = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.paymentService.query()
            .subscribe((res: HttpResponse<Payment[]>) => { this.payments = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.prescriptionService.query()
            .subscribe((res: HttpResponse<Prescription[]>) => { this.prescriptions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.getPatient();
    }
    getPatient() {
        this.patientService.getPatientByUserId().subscribe((response) => {
            this.patient = response.body;
        });
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        // this.appointment.patientId = this.patient.id;
        if (this.appointment.id !== undefined) {
            this.appointment.patientId = this.patient.id;
            this.subscribeToSaveResponse(
                this.appointmentService.update(this.appointment));
        } else {
            this.appointment.patientId = this.patient.id;
            this.subscribeToSaveResponse(
                this.appointmentService.create(this.appointment));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Appointment>>) {
        result.subscribe((res: HttpResponse<Appointment>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Appointment) {
        this.eventManager.broadcast({ name: 'appointmentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackDoctorById(index: number, item: Doctor) {
        return item.id;
    }

    trackPatientById(index: number, item: Patient) {
        return item.id;
    }

    trackSpecializationById(index: number, item: Specialization) {
        return item.id;
    }

    trackPatientIssuesById(index: number, item: PatientIssue) {
        return item.id;
    }

    trackPaymentById(index: number, item: Payment) {
        return item.id;
    }

    trackPrescriptionById(index: number, item: Prescription) {
        return item.id;
    }

    trackDoctorScheduleById(index: number, item: DoctorSchedule) {
        return item.id;
    }

    getDoctors() {
        console.log(this.appointment.specializationId);
        this.display_doctors = this.doctors.filter((doctor) => {
            return doctor.specializationId === this.appointment.specializationId;
        });
        console.log('display_doctor = ', this.display_doctors);
    }
    getSchedule() {
        console.log(this.appointment.doctorId);
        this.display_schedules = this.doctorschedules.filter((doctorschedule) => {
            return doctorschedule.doctorId === this.appointment.doctorId;
        });
        console.log('display_schedules = ', this.display_schedules);
    }
    selectedDate() {
        console.log(this.appointment.appointmentdate);
        // window.alert(this.appointment.appointmentdate);
    }
}

@Component({
    selector: 'jhi-appointment-popup',
    template: ''
})
export class AppointmentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private appointmentPopupService: AppointmentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.appointmentPopupService
                    .open(AppointmentDialogComponent as Component, params['id']);
            } else {
                this.appointmentPopupService
                    .open(AppointmentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
// import {Component, OnInit, OnDestroy} from '@angular/core';
// import { ActivatedRoute } from '@angular/router';
// import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// import { NgModule } from '@angular/core';
//
// import { Observable } from 'rxjs/Observable';
// import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
//
// import { Appointment } from './appointment.model';
// import { AppointmentPopupService } from './appointment-popup.service';
// import { AppointmentService } from './appointment.service';
// import { Doctor, DoctorService } from '../doctor';
// import { Patient, PatientService } from '../patient';
// import { Specialization, SpecializationService } from '../specialization';
// import { PatientIssue, PatientIssueService } from '../patient-issue';
// import { DoctorSchedule, DoctorScheduleService } from '../doctor-schedule';
// import { Payment, PaymentService } from '../payment';
// import { Prescription, PrescriptionService } from '../prescription';
// // import { MatSelectModule } from '@angular/material';
//
// // For Design
// import {FormControl, FormBuilder, Validators} from '@angular/forms';
//
// @Component({
//     selector: 'jhi-appointment-dialog',
//     templateUrl: './appointment-dialog.component.html'
// })
//
// @NgModule({
//     imports: [],
// })
//
// export class AppointmentDialogComponent implements OnInit {
//     appointment: Appointment;
//     isSaving: boolean;
//
//     // myForm: FormGroup;
//     patientControl =  new FormControl('', [Validators.required]);
//     patients: Patient[];
//     display_patients: Patient[];
//     // Add Doctor entity for filtering
//     doctors: Doctor[];
//     display_doctors: Doctor[];
//     // Add Spacialization Id
//     specialization: Specialization[];
//     patientissues: PatientIssue[];
//     // Add Doctor Schedule
//     doctorschedules: DoctorSchedule[];
//     display_schedules: DoctorSchedule[];
//     // Relative Module
//     payments: Payment[];
//     prescriptions: Prescription[];
//     appointmentdateDp: any;
//     // Added Menually For Dropdown Manage
//     specializationId;
//     doctorId;
//
//     constructor(
//         public activeModal: NgbActiveModal,
//         private jhiAlertService: JhiAlertService,
//         private appointmentService: AppointmentService,
//        // private doctorService: DoctorService,
//        // private patientService: PatientService,
//         private specializationService: SpecializationService,
//         private patientIssuesService: PatientIssueService,
//         private doctorScheduleService: DoctorScheduleService,
//         private paymentService: PaymentService,
//         private prescriptionService: PrescriptionService,
//         private eventManager: JhiEventManager,
//     ) {
//     }
//
//     ngOnInit() {
//         // this.patientControl = this.formBuilder.control('', [Validators.required]);
//         this.isSaving = false;
//         // this.doctorService.query()
//         //     .subscribe((res: HttpResponse<Doctor[]>) => { this.doctors = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
//         // console.log( 'doctors are:', this.appointment);
//         // this.patientService.query()
//         //     .subscribe((res: HttpResponse<Patient[]>) => { this.patients = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
//         this.specializationService.query()
//             .subscribe((res: HttpResponse<Specialization[]>) => { this.specialization = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
//         this.patientIssuesService.query()
//             .subscribe((res: HttpResponse<PatientIssue[]>) => { this.patientissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
//         this.doctorScheduleService.query()
//             .subscribe((res: HttpResponse<DoctorSchedule[]>) => { this.doctorschedules = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
//         this.paymentService.query()
//             .subscribe((res: HttpResponse<Payment[]>) => { this.payments = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
//         this.prescriptionService.query()
//             .subscribe((res: HttpResponse<Prescription[]>) => { this.prescriptions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
//     }
//
//     clear() {
//         this.activeModal.dismiss('cancel');
//     }
//
//     save() {
//         this.isSaving = true;
//         if (this.appointment.id !== undefined) {
//             this.subscribeToSaveResponse(
//                 this.appointmentService.update(this.appointment));
//         } else {
//             this.subscribeToSaveResponse(
//                 this.appointmentService.create(this.appointment));
//         }
//     }
//
//     private subscribeToSaveResponse(result: Observable<HttpResponse<Appointment>>) {
//         result.subscribe((res: HttpResponse<Appointment>) =>
//             this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
//     }
//
//     private onSaveSuccess(result: Appointment) {
//         this.eventManager.broadcast({ name: 'appointmentListModification', content: 'OK'});
//         this.isSaving = false;
//         this.activeModal.dismiss(result);
//     }
//
//     private onSaveError() {
//         this.isSaving = false;
//     }
//
//     private onError(error: any) {
//         this.jhiAlertService.error(error.message, null, null);
//     }
//
//     trackDoctorById(index: number, item: Doctor) {
//         return item.id;
//     }
//
//     trackPatientById(index: number, item: Patient) {
//         return item.id;
//     }
//
//     trackSpecializationById(index: number, item: Specialization) {
//         return item.id;
//     }
//
//     trackPatientIssuesById(index: number, item: PatientIssue) {
//         return item.id;
//     }
//
//     trackPaymentById(index: number, item: Payment) {
//         return item.id;
//     }
//
//     trackPrescriptionById(index: number, item: Prescription) {
//         return item.id;
//     }
//
//     trackDoctorScheduleById(index: number, item: DoctorSchedule) {
//         return item.id;
//     }
//
//     getDoctors() {
//         console.log(this.appointment.specializationId);
//         this.display_doctors = this.doctors.filter((doctor) => {
//             return doctor.specializationId === this.appointment.specializationId;
//         });
//         console.log('display_doctor = ', this.display_doctors);
//     }
//
//     getSchedule() {
//         console.log(this.appointment.doctorId);
//         this.display_schedules = this.doctorschedules.filter((doctorschedule) => {
//             return doctorschedule.doctorId === this.appointment.doctorId;
//         });
//         console.log('display_schedules = ', this.display_schedules);
//     }
//
//     selectedDate() {
//         console.log(this.appointment.appointmentdate);
//         window.alert(this.appointment.appointmentdate);
//     }
// }
//
// @Component({
//     selector: 'jhi-appointment-popup',
//     template: ''
// })
// export class AppointmentPopupComponent implements OnInit, OnDestroy {
//
//     routeSub: any;
//
//     constructor(
//         private route: ActivatedRoute,
//         private appointmentPopupService: AppointmentPopupService
//     ) {}
//
//     ngOnInit() {
//         this.routeSub = this.route.params.subscribe((params) => {
//             if ( params['id'] ) {
//                 this.appointmentPopupService
//                     .open(AppointmentDialogComponent as Component, params['id']);
//             } else {
//                 this.appointmentPopupService
//                     .open(AppointmentDialogComponent as Component);
//             }
//         });
//     }
//
//     ngOnDestroy() {
//         this.routeSub.unsubscribe();
//     }
// }
