import { BaseEntity } from './../../shared';

export const enum Appointment_Status {
    'CONFIRMED',
    'UNCONFIRMED',
    'RESCHEDULE'
}

export class Appointment implements BaseEntity {
    constructor(
        public id?: number,
        public appointmentdate?: any,
        public appointmenttime?: string,
        public appointmentstatus?: Appointment_Status,
        public doctorId?: number,
        public patientId?: number,
        public specializationId?: number,
        public doctorscheduleId?: number,
        public patientissueId?: number,
        public paymentId?: number,
        public prescriptionId?: number,
        public patientName?: string,
        // public doctorName?: string
    ) {
    }
}
