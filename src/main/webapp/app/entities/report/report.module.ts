import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TelemedicineApplicationSharedModule } from '../../shared';
import { ReportComponent } from './report.component';
import { ReportRoute } from './report.route';
import { ReportService } from './report.service';
const ENTITY_STATES = [
    ...ReportRoute,
];

@NgModule({
    imports: [
        TelemedicineApplicationSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ReportComponent
    ],
    entryComponents: [
        ReportComponent
    ],
    providers: [
        ReportService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TelemedicineApplicationReportModule {}
