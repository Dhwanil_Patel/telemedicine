import { BaseEntity } from './../../shared';

export class Report implements BaseEntity {
    constructor(
        public id?: number,
        public fromdate?: any,
        public todate?: any,
        public reporttype?: any
    ){
    }
}

