import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import {Patient} from '../patient/patient.model';

@Injectable()
export class ReportService {

    constructor(private http: HttpClient) { }

    generateReport(toDate: any, fromDate: any, reportType: any): Observable<HttpResponse<Patient[]>> {
        // const params: HttpParams = createRequestOption(toDate, fromDate, reportType);

        const requestURL = SERVER_API_URL + 'api/reports/' + reportType;

        return this.http.get<Patient[]>(requestURL, {
            // params,
            observe: 'response'
        });
    }

    // generateReport(toDate: any, fromDate: any, reportType: any): Observable<HttpResponse<Patient[]>> {
    //     // const options = createRequestOption(req);
    //     return this.http.get<Patient[]>(`${this.resourceUrl}/${reportType}`, { observe: 'response'} )
    //         .map((res: HttpResponse<Patient[]>) => this.convertArrayResponse(res));
    // }

    // private convertArrayResponse(res: HttpResponse<Patient[]>): HttpResponse<Patient[]> {
    //     const jsonResponse: Patient[] = res.body;
    //     const body: Patient[] = [];
    //     for (let i = 0; i < jsonResponse.length; i++) {
    //         body.push(this.convertItemFromServer(jsonResponse[i]));
    //     }
    //     return res.clone({body});
    // }
    //
    // private convertItemFromServer(prescription: Patient): Patient {
    //     const copy: Patient = Object.assign({}, prescription);
    //     return copy;
    // }
    //
    // private convert(prescription: Patient): Patient {
    //     const copy: Patient = Object.assign({}, prescription);
    //     return copy;
    // }
}
