import { Routes } from '@angular/router';
import { ReportComponent } from './report.component';

export const ReportRoute: Routes = [
    {
        path: 'report',
        component: ReportComponent,
        data: {
            authorities: []
        }
    }
];
