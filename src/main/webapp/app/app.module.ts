    import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2Webstorage, LocalStorageService, SessionStorageService  } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { TelemedicineApplicationSharedModule, UserRouteAccessService } from './shared';
import { TelemedicineApplicationAppRoutingModule} from './app-routing.module';
import { TelemedicineApplicationHomeModule } from './home/home.module';
import { TelemedicineApplicationAdminModule } from './admin/admin.module';
import { TelemedicineApplicationAccountModule } from './account/account.module';
import { TelemedicineApplicationEntityModule } from './entities/entity.module';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { NgxEditorModule } from 'ngx-editor';

// import { AngularFontAwesomeModule } from 'angular-font-awesome';
// import { EditorModule } from '@tinymce/tinymce-angular';

// jhipster-needle-angular-add-module-import JHipster will add new module here
import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent,
} from './layouts';
// import { CheckupComponent } from './entities/checkup';
@NgModule({
    imports: [
        BrowserModule,
        TelemedicineApplicationAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        TelemedicineApplicationSharedModule,
        TelemedicineApplicationHomeModule,
        TelemedicineApplicationAdminModule,
        TelemedicineApplicationAccountModule,
        TelemedicineApplicationEntityModule,
        // AngularFontAwesomeModule,
        // EditorModule
        // jhipster-needle-angular-add-module JHipster will add new module here
        NgxEditorModule
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent,
    ],
    providers: [
        ProfileService,
        PaginationConfig,
        UserRouteAccessService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [
                LocalStorageService,
                SessionStorageService
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [
                JhiEventManager
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        }
    ],
    bootstrap: [ JhiMainComponent ]
})
export class TelemedicineApplicationAppModule {}
