import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DoctorService } from '../entities/doctor/doctor.service';
import { PatientService } from '../entities/patient/patient.service';
import { PharmacistService } from '../entities/pharmacist/pharmacist.service';
import {AppointmentService} from '../entities/appointment/appointment.service';
import {SpecializationService } from '../entities/specialization/specialization.service';
import { Account, LoginModalService,   } from '../shared';
import {DatePipe} from '@angular/common';
import { Principal } from '../shared/auth/principal.service';
@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.scss'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    private chart: any;
    doctorCount: any;
    patientCount: any;
    pharmacistCount: any;
    appointment: any;
    appointmentdate: any;
    appointmentCount: any;
    totalAppointment: any;
    doctorDetails: any;
    specialization: any;
    specializationId: any;
    specializationname: any[];

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private doctorService: DoctorService,
        private patientService: PatientService,
        private pharmacistService: PharmacistService,
        private appointmentService: AppointmentService,
        private datePipe: DatePipe,
        private specializationService: SpecializationService
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        window.scrollTo(0, 0);
        this.registerAuthenticationSuccess();

        this.doctorService.query()
            .subscribe((response) => {
            this.doctorDetails = response.body;
            this.specializationId = response.body['specialization'];
                this.doctorCount = response.body.length;
                console.log('doctor:==', this.doctorDetails);
            });
        this.specializationService.query()
            .subscribe((response) => {
                this.specialization = response.body;

                const name = response.body['expertise'];
                console.log('name==', name);
                // for (let i = 0; i< 4 ; i++) {
                //     const name = response.body[i]['expertise'];
                //     this.specializationname = name;
                //     console.log('specializationname==', name);
                // }
            });
        this.patientService.query()
            .subscribe((response) => {
                this.patientCount = response.body.length;
                console.log('countdoctor:==', this.patientCount);
            });
        this.pharmacistService.query()
            .subscribe((response) => {
                this.pharmacistCount = response.body.length;
                console.log('pharmacistCount:==', this.pharmacistCount);
            });
        this.appointmentService.query().subscribe((response) => {
            this.appointment = response;
            console.log('date==', this.appointment);
            let count = 0;
            this.totalAppointment = response.body.length;
             for (let i = 0; i < response.body.length; i++ ) {
                 const resources = response.body[i]['appointmentdate'];
                 const appointmentDate = this.datePipe.transform( resources, 'EEE, MMMM d, y');
                 const currentDate = new Date( );
                 const date1 = this.datePipe.transform(currentDate, 'EEE, MMMM d, y');
                 console.log( 'hello date' , date1);
                 if (appointmentDate === date1) {
                     count++;
                     this.appointmentCount = count;
                     if (count < 1) {
                         this.appointmentCount = 0;
                     }
                     console.log( 'hello rajan' , this.appointmentCount);
                }
                console.log('resourcedata==' , appointmentDate);
            }
        });

    }
    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
}
