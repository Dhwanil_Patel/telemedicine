import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiLanguageService } from 'ng-jhipster';

import { ProfileService } from '../profiles/profile.service';
import { Doctor } from '../../entities/doctor/doctor.model';
import { DoctorService } from '../../entities/doctor/doctor.service';
import { JhiLanguageHelper, Principal, LoginModalService, LoginService } from '../../shared';

import { VERSION } from '../../app.constants';
import { JhiEventManager } from 'ng-jhipster';
import {Patient} from '../../entities/patient/patient.model';
import {Pharmacist} from '../../entities/pharmacist/pharmacist.model';
import {PatientService} from '../../entities/patient/patient.service';
import {PharmacistService} from '../../entities/pharmacist/pharmacist.service';
@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [
        'navbar.scss'
    ]
})
export class NavbarComponent implements OnInit {

    doctor: Doctor;
    patient: Patient;
    pharmacist: Pharmacist;
    inProduction: boolean;
    account: Account;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    doctors: Doctor[];
    value: boolean;
    public id: number;

    constructor(
        private loginService: LoginService,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router,
        private eventManager: JhiEventManager,
        private doctorService: DoctorService,
        private patientService: PatientService,
        private pharmacistService: PharmacistService

    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
    }
    // getuserbyid
    /*loadAll() {
        this.doctorService.getDoctorByUserId().subscribe((response) => {
            this.doctor = response.body;
             // console.log('res ', response);
        });
        this.patientServcie.getPatientByUserId().subscribe((response) => {
            this.patient = response.body;
            // console.log('res ', response);
        });
        this.pharmacistService.getPharmacistByUserId().subscribe((response) => {
            this.pharmacist = response.body;
            // console.log('res ', response);
        });
    }
*/    ngOnInit() {
        // this.loadAll();
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });
        this.principal.identity().then((account) => {
            this.account = account;
            if ( account.usertype === 'Patient') {
                this.patientService.getPatientByUserId().subscribe((response) => {
                    this.patient = response.body;
                });
            }
            if ( account.usertype === 'Doctor') {
                this.doctorService.getDoctorByUserId().subscribe((response) => {
                    this.doctor = response.body;
                });
            }
            if ( account.usertype === 'Pharmacist') {
                this.pharmacistService.getPharmacistByUserId().subscribe((response) => {
                    this.pharmacist = response.body;
                });
            }
        });
        this.profileService.getProfileInfo().then((profileInfo) => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
        this.registerAuthenticationSuccess();

    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }
    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }
}

// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// import { JhiLanguageService } from 'ng-jhipster';
//
// import { ProfileService } from '../profiles/profile.service';
// import { Doctor } from '../../entities/doctor/doctor.model';
// import { DoctorService } from '../../entities/doctor/doctor.service';
// import { JhiLanguageHelper, Principal, LoginModalService, LoginService } from '../../shared';
//
// import { VERSION } from '../../app.constants';
// import { JhiEventManager } from 'ng-jhipster';
// @Component({
//     selector: 'jhi-navbar',
//     templateUrl: './navbar.component.html',
//     styleUrls: [
//         'navbar.scss'
//     ]
// })
// export class NavbarComponent implements OnInit {
//
//     doctor: Doctor;
//     inProduction: boolean;
//     account: Account;
//     isNavbarCollapsed: boolean;
//     languages: any[];
//     swaggerEnabled: boolean;
//     modalRef: NgbModalRef;
//     version: string;
//     doctors: Doctor[];
//
//     constructor(
//         private loginService: LoginService,
//         private languageService: JhiLanguageService,
//         private languageHelper: JhiLanguageHelper,
//         private principal: Principal,
//         private loginModalService: LoginModalService,
//         private profileService: ProfileService,
//         private router: Router,
//         private eventManager: JhiEventManager,
//         private doctorService: DoctorService
//
//     ) {
//         this.version = VERSION ? 'v' + VERSION : '';
//         this.isNavbarCollapsed = true;
//     }
//
//     ngOnInit() {
//         // this.doctorService.find(1);
//         this.languageHelper.getAll().then((languages) => {
//             this.languages = languages;
//         });
//         this.principal.identity().then((account) => {
//             this.account = account;
//         });
//         this.profileService.getProfileInfo().then((profileInfo) => {
//             this.inProduction = profileInfo.inProduction;
//             this.swaggerEnabled = profileInfo.swaggerEnabled;
//         });
//         this.registerAuthenticationSuccess();
//     }
//
//     registerAuthenticationSuccess() {
//         this.eventManager.subscribe('authenticationSuccess', (message) => {
//             this.principal.identity().then((account) => {
//                 this.account = account;
//             });
//         });
//     }
//     changeLanguage(languageKey: string) {
//       this.languageService.changeLanguage(languageKey);
//     }
//
//     collapseNavbar() {
//         this.isNavbarCollapsed = true;
//     }
//
//     isAuthenticated() {
//         return this.principal.isAuthenticated();
//     }
//
//     login() {
//         this.modalRef = this.loginModalService.open();
//     }
//
//     logout() {
//         this.collapseNavbar();
//         this.loginService.logout();
//         this.router.navigate(['']);
//     }
//
//     toggleNavbar() {
//         this.isNavbarCollapsed = !this.isNavbarCollapsed;
//     }
//
//     getImageUrl() {
//         return this.isAuthenticated() ? this.principal.getImageUrl() : null;
//     }
// }
