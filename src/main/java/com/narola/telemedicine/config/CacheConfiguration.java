package com.narola.telemedicine.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.narola.telemedicine.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.SocialUserConnection.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Degree.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Specialization.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Doctor.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Patient.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Pharmacist.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.DoctorSchedule.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.PatientIssue.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Appointment.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Payment.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.Prescription.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.OrderMedicine.class.getName(), jcacheConfiguration);
            cm.createCache(com.narola.telemedicine.domain.PharmacistPayment.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
