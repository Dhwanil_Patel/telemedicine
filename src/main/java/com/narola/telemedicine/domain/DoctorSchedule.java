package com.narola.telemedicine.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

import com.narola.telemedicine.domain.enumeration.Days;

import com.narola.telemedicine.domain.enumeration.Schedule_Status;

/**
 * A DoctorSchedule.
 */
@Entity
@Table(name = "doctor_schedule")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "doctorschedule")
public class DoctorSchedule extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "workingday", nullable = false)
    private Days workingday;

    @NotNull
    @Column(name = "fromtime", nullable = false)
    private String fromtime;

    @NotNull
    @Column(name = "totime", nullable = false)
    private String totime;

    @NotNull
    @Column(name = "noofpatient", nullable = false)
    private Integer noofpatient;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Schedule_Status status;

    @ManyToOne
    private Doctor doctor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Days getWorkingday() {
        return workingday;
    }

    public DoctorSchedule workingday(Days workingday) {
        this.workingday = workingday;
        return this;
    }

    public void setWorkingday(Days workingday) {
        this.workingday = workingday;
    }

    public String getFromtime() {
        return fromtime;
    }

    public DoctorSchedule fromtime(String fromtime) {
        this.fromtime = fromtime;
        return this;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public DoctorSchedule totime(String totime) {
        this.totime = totime;
        return this;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    public Integer getNoofpatient() {
        return noofpatient;
    }

    public DoctorSchedule noofpatient(Integer noofpatient) {
        this.noofpatient = noofpatient;
        return this;
    }

    public void setNoofpatient(Integer noofpatient) {
        this.noofpatient = noofpatient;
    }

    public Schedule_Status getStatus() {
        return status;
    }

    public DoctorSchedule status(Schedule_Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Schedule_Status status) {
        this.status = status;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public DoctorSchedule doctor(Doctor doctor) {
        this.doctor = doctor;
        return this;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DoctorSchedule doctorSchedule = (DoctorSchedule) o;
        if (doctorSchedule.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), doctorSchedule.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DoctorSchedule{" +
            "id=" + getId() +
            ", workingday='" + getWorkingday() + "'" +
            ", fromtime='" + getFromtime() + "'" +
            ", totime='" + getTotime() + "'" +
            ", noofpatient=" + getNoofpatient() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
