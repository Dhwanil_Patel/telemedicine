package com.narola.telemedicine.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A OrderMedicine.
 */
@Entity
@Table(name = "order_medicine")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ordermedicine")
public class
OrderMedicine extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "medicinedetail", nullable = false)
    private String medicinedetail;

    @NotNull
    @Column(name = "totalprice", nullable = false)
    private Integer totalprice;

    // add field status
    @NotNull
    @Column(name = "status", nullable = false)
    private String status;

    @OneToOne
    @JoinColumn(unique = true)
    private Prescription prescription;

//    @OneToOne
//    @JoinColumn(unique = true)
//    private Pharmacist pharmacist;

    @OneToOne(mappedBy = "orderMedicine")
    @JsonIgnore
    private PharmacistPayment pharmacistPayment;

    @ManyToOne
    private Pharmacist pharmacist;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMedicinedetail() {
        return medicinedetail;
    }

    public OrderMedicine medicinedetail(String medicinedetail) {
        this.medicinedetail = medicinedetail;
        return this;
    }

    public void setMedicinedetail(String medicinedetail) {
        this.medicinedetail = medicinedetail;
    }

    public Integer getTotalprice() {
        return totalprice;
    }

    public OrderMedicine totalprice(Integer totalprice) {
        this.totalprice = totalprice;
        return this;
    }

    public void setTotalprice(Integer totalprice) {
        this.totalprice = totalprice;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public OrderMedicine prescription(Prescription prescription) {
        this.prescription = prescription;
        return this;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    public Pharmacist getPharmacist() {
        return pharmacist;
    }

    public OrderMedicine pharmacist(Pharmacist pharmacist) {
        this.pharmacist = pharmacist;
        return this;
    }

    public void setPharmacist(Pharmacist pharmacist) {
        this.pharmacist = pharmacist;
    }

    public PharmacistPayment getPharmacistPayment() {
        return pharmacistPayment;
    }

    public OrderMedicine pharmacistPayment(PharmacistPayment pharmacistPayment) {
        this.pharmacistPayment = pharmacistPayment;
        return this;
    }

    public void setPharmacistPayment(PharmacistPayment pharmacistPayment) {
        this.pharmacistPayment = pharmacistPayment;
    }

    // getter setter
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderMedicine orderMedicine = (OrderMedicine) o;
        if (orderMedicine.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderMedicine.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderMedicine{" +
            "id=" + getId() +
            ", medicinedetail='" + getMedicinedetail() + "'" +
            ", totalprice=" + getTotalprice() +
            ", status=" + getStatus() +
            "}";
    }
}
