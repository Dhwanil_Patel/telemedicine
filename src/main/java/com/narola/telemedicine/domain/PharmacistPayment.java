package com.narola.telemedicine.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.narola.telemedicine.domain.enumeration.Payment_Status;

/**
 * A PharmacistPayment.
 */
@Entity
@Table(name = "pharmacist_payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pharmacistpayment")
public class PharmacistPayment extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "payment_date", nullable = false)
    private LocalDate paymentDate;

    @NotNull
    @Column(name = "totalamount", nullable = false)
    private Integer totalamount;

    @NotNull
    @Column(name = "paymentmode", nullable = false)
    private String paymentmode;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status", nullable = false)
    private Payment_Status paymentStatus;

    @NotNull
    @Column(name = "transcationid", nullable = false)
    private String transcationid;

    @OneToOne
    @JoinColumn(unique = true)
    private OrderMedicine orderMedicine;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public PharmacistPayment paymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
        return this;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Integer getTotalamount() {
        return totalamount;
    }

    public PharmacistPayment totalamount(Integer totalamount) {
        this.totalamount = totalamount;
        return this;
    }

    public void setTotalamount(Integer totalamount) {
        this.totalamount = totalamount;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public PharmacistPayment paymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
        return this;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public Payment_Status getPaymentStatus() {
        return paymentStatus;
    }

    public PharmacistPayment paymentStatus(Payment_Status paymentStatus) {
        this.paymentStatus = paymentStatus;
        return this;
    }

    public void setPaymentStatus(Payment_Status paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTranscationid() {
        return transcationid;
    }

    public PharmacistPayment transcationid(String transcationid) {
        this.transcationid = transcationid;
        return this;
    }

    public void setTranscationid(String transcationid) {
        this.transcationid = transcationid;
    }

    public OrderMedicine getOrderMedicine() {
        return orderMedicine;
    }

    public PharmacistPayment orderMedicine(OrderMedicine orderMedicine) {
        this.orderMedicine = orderMedicine;
        return this;
    }

    public void setOrderMedicine(OrderMedicine orderMedicine) {
        this.orderMedicine = orderMedicine;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PharmacistPayment pharmacistPayment = (PharmacistPayment) o;
        if (pharmacistPayment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pharmacistPayment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PharmacistPayment{" +
            "id=" + getId() +
            ", paymentDate='" + getPaymentDate() + "'" +
            ", totalamount=" + getTotalamount() +
            ", paymentmode='" + getPaymentmode() + "'" +
            ", paymentStatus='" + getPaymentStatus() + "'" +
            ", transcationid='" + getTranscationid() + "'" +
            "}";
    }
}
