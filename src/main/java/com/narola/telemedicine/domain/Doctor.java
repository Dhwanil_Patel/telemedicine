package com.narola.telemedicine.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.narola.telemedicine.domain.enumeration.Process_Status;

import com.narola.telemedicine.domain.enumeration.Gender;

/**
 * A Doctor.
 */
@Entity
@Table(name = "doctor")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "doctor")
public class Doctor extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @NotNull
    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Process_Status status;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "gender", nullable = false)
    private Gender gender;

    @Column(name = "dateofbirth")
    private LocalDate dateofbirth;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @NotNull
    @Column(name = "mobileno", nullable = false)
    private String mobileno;

    @Column(name = "bloodgroup")
    private String bloodgroup;

    @NotNull
    @Column(name = "exprience", nullable = false)
    private Float exprience;

    @NotNull
    @Column(name = "licenceno", nullable = false)
    private String licenceno;

    @NotNull
    @Lob
    @Column(name = "documents", nullable = false)
    private byte[] documents;

    @Column(name = "documents_content_type", nullable = false)
    private String documentsContentType;

    @Column(name = "googleplusid")
    private String googleplusid;

    @Column(name = "facebookid")
    private String facebookid;

    @Column(name = "twitterid")
    private String twitterid;

    @NotNull
    @Column(name = "consultationcharge", nullable = false)
    private Integer consultationcharge;

    //added four more fileds: skype,city state
    @NotNull
    @Column(name = "skype", nullable = false)
    private String skype;

    @NotNull
    @Column(name = "city", nullable = false)
    private String city;

    @NotNull
    @Column(name = "state", nullable = false)
    private String state;

    @NotNull
    @Column(name = "country", nullable = false)
    private String country;

    //    added user_id from User
    @OneToOne
    private User user;

    @ManyToOne
    private Degree degree;

    @ManyToOne
    private Specialization specialization;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public Doctor firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Doctor lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Process_Status getStatus() {
        return status;
    }

    public Doctor status(Process_Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Process_Status status) {
        this.status = status;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public Doctor photo(byte[] photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public Doctor photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public Gender getGender() {
        return gender;
    }

    public Doctor gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDateofbirth() {
        return dateofbirth;
    }

    public Doctor dateofbirth(LocalDate dateofbirth) {
        this.dateofbirth = dateofbirth;
        return this;
    }

    public void setDateofbirth(LocalDate dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getAddress() {
        return address;
    }

    public Doctor address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileno() {
        return mobileno;
    }

    public Doctor mobileno(String mobileno) {
        this.mobileno = mobileno;
        return this;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public Doctor bloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
        return this;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public Float getExprience() {
        return exprience;
    }

    public Doctor exprience(Float exprience) {
        this.exprience = exprience;
        return this;
    }

    public void setExprience(Float exprience) {
        this.exprience = exprience;
    }

    public String getLicenceno() {
        return licenceno;
    }

    public Doctor licenceno(String licenceno) {
        this.licenceno = licenceno;
        return this;
    }

    public void setLicenceno(String licenceno) {
        this.licenceno = licenceno;
    }

    public byte[] getDocuments() {
        return documents;
    }

    public Doctor documents(byte[] documents) {
        this.documents = documents;
        return this;
    }

    public void setDocuments(byte[] documents) {
        this.documents = documents;
    }

    public String getDocumentsContentType() {
        return documentsContentType;
    }

    public Doctor documentsContentType(String documentsContentType) {
        this.documentsContentType = documentsContentType;
        return this;
    }

    public void setDocumentsContentType(String documentsContentType) {
        this.documentsContentType = documentsContentType;
    }

    public String getGoogleplusid() {
        return googleplusid;
    }

    public Doctor googleplusid(String googleplusid) {
        this.googleplusid = googleplusid;
        return this;
    }

    public void setGoogleplusid(String googleplusid) {
        this.googleplusid = googleplusid;
    }

    public String getFacebookid() {
        return facebookid;
    }

    public Doctor facebookid(String facebookid) {
        this.facebookid = facebookid;
        return this;
    }

    public void setFacebookid(String facebookid) {
        this.facebookid = facebookid;
    }

    public String getTwitterid() {
        return twitterid;
    }

    public Doctor twitterid(String twitterid) {
        this.twitterid = twitterid;
        return this;
    }

    public void setTwitterid(String twitterid) {
        this.twitterid = twitterid;
    }

    public Integer getConsultationcharge() {
        return consultationcharge;
    }

    public Doctor consultationcharge(Integer consultationcharge) {
        this.consultationcharge = consultationcharge;
        return this;
    }

    public void setConsultationcharge(Integer consultationcharge) {
        this.consultationcharge = consultationcharge;
    }

    //added getter setter for four fields

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Degree getDegree() {
        return degree;
    }

    public Doctor degree(Degree degree) {
        this.degree = degree;
        return this;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public Doctor specialization(Specialization specialization) {
        this.specialization = specialization;
        return this;
    }

//    added getter setter to link user_id in doctor

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Doctor user(User user){
        this.user = user;
        return this;
    }



    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Doctor doctor = (Doctor) o;
        if (doctor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), doctor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Doctor{" + "id=" + getId() + ", firstname='" + getFirstname() + "'" + ", lastname='" + getLastname() + "'" + ", status='" + getStatus() + "'" + ", photo='" + getPhoto() + "'" + ", photoContentType='" + getPhotoContentType() + "'" + ", gender='" + getGender() + "'" + ", dateofbirth='" + getDateofbirth() + "'" + ", address='" + getAddress() + "'" + ", mobileno='" + getMobileno() + "'" + ", bloodgroup='" + getBloodgroup() + "'" + ", exprience=" + getExprience() + ", licenceno='" + getLicenceno() + "'" + ", documents='" + getDocuments() + "'" + ", documentsContentType='" + getDocumentsContentType() + "'" + ", googleplusid='" + getGoogleplusid() + "'" + ", facebookid='" + getFacebookid() + "'" + ", twitterid='" + getTwitterid() + "'" + ", consultationcharge=" + getConsultationcharge() +      ", skype=" + getSkype() +
            ", city=" + getCity() +
            ", state=" + getState() +
            ", country=" + getCountry() +"}";
    }
}
