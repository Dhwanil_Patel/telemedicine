package com.narola.telemedicine.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Prescription.
 */
@Entity
@Table(name = "prescription")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "prescription")
public class Prescription extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "prescriptiondetail", nullable = false)
    private String prescriptiondetail;

    @OneToOne
    @JoinColumn(unique = true)
    private Appointment appointment;

    @OneToOne(mappedBy = "prescription")
    @JsonIgnore
    private OrderMedicine orderMedicine;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrescriptiondetail() {
        return prescriptiondetail;
    }

    public Prescription prescriptiondetail(String prescriptiondetail) {
        this.prescriptiondetail = prescriptiondetail;
        return this;
    }

    public void setPrescriptiondetail(String prescriptiondetail) {
        this.prescriptiondetail = prescriptiondetail;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public Prescription appointment(Appointment appointment) {
        this.appointment = appointment;
        return this;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public OrderMedicine getOrderMedicine() {
        return orderMedicine;
    }

    public Prescription orderMedicine(OrderMedicine orderMedicine) {
        this.orderMedicine = orderMedicine;
        return this;
    }

    public void setOrderMedicine(OrderMedicine orderMedicine) {
        this.orderMedicine = orderMedicine;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Prescription prescription = (Prescription) o;
        if (prescription.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), prescription.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Prescription{" +
            "id=" + getId() +
            ", prescriptiondetail='" + getPrescriptiondetail() + "'" +
            "}";
    }
}
