package com.narola.telemedicine.domain;
import com.narola.telemedicine.domain.Patient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.narola.telemedicine.domain.enumeration.Appointment_Status;
// import static com.narola.telemedicine.domain.Appointment_.patient;

/**
 * A Appointment.
 */
@Entity
@Table(name = "appointment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "appointment")
public class Appointment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "appointmentdate", nullable = false)
    private LocalDate appointmentdate;

//    @NotNull
//    @Column(name = "appointmenttime", nullable = false)
//    private String appointmenttime;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "appointmentstatus", nullable = false)
    private Appointment_Status appointmentstatus;

    @ManyToOne
    private Doctor doctor;

    @ManyToOne
    private Patient patient;

    @ManyToOne
    private Specialization specialization;

    @ManyToOne
    private PatientIssue patientissue;

    @ManyToOne
    private DoctorSchedule doctorschedule;

    @OneToOne(mappedBy = "appointment")
    @JsonIgnore
    private Payment payment;

    @OneToOne(mappedBy = "appointment")
    @JsonIgnore
    private Prescription prescription;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getAppointmentdate() {
        return appointmentdate;
    }

    public Appointment appointmentdate(LocalDate appointmentdate) {
        this.appointmentdate = appointmentdate;
        return this;
    }

    public void setAppointmentdate(LocalDate appointmentdate) {
        this.appointmentdate = appointmentdate;
    }

//    public String getAppointmenttime() {
//        return appointmenttime;
//    }
//
//    public Appointment appointmenttime(String appointmenttime) {
//        this.appointmenttime = appointmenttime;
//        return this;
//    }
//
//    public void setAppointmenttime(String appointmenttime) {
//        this.appointmenttime = appointmenttime;
//    }

    public Appointment_Status getAppointmentstatus() {
        return appointmentstatus;
    }

    public Appointment appointmentstatus(Appointment_Status appointmentstatus) {
        this.appointmentstatus = appointmentstatus;
        return this;
    }

    public void setAppointmentstatus(Appointment_Status appointmentstatus) {
        this.appointmentstatus = appointmentstatus;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Appointment doctor(Doctor doctor) {
        this.doctor = doctor;
        return this;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    // doctor name
   // public void setDoctorName(Doctor doctor) {
    public Appointment doctorName(Doctor doctor) {
     //   this.doctor = doctor;
        this.doctor = doctor;
    // }
        return this;
    }

    public void setDoctorName(Doctor doctor) {
        this.doctor = doctor;
     }

    public Patient getPatient() {
        return patient;
    }

    public Appointment patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public Appointment specialization(Specialization specialization){
        this.specialization = specialization;
        return this;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public PatientIssue getPatientissue() {
        return patientissue;
    }

    public Appointment patientissue(PatientIssue patientIssues) {
        this.patientissue = patientIssues;
        return this;
    }

    public void setPatientissue(PatientIssue patientissue) {
        this.patientissue = patientissue;
    }

    public DoctorSchedule getDoctorschedule() {
        return doctorschedule;
    }


    public Appointment doctorschedule(DoctorSchedule doctorSchedule){
        this.doctorschedule = doctorSchedule;
        return this;
    }

    public void setDoctorschedule(DoctorSchedule doctorschedule) {
        this.doctorschedule = doctorschedule;
    }

    public Payment getPayment() {
        return payment;
    }

    public Appointment payment(Payment payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public Appointment prescription(Prescription prescription) {
        this.prescription = prescription;
        return this;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Appointment appointment = (Appointment) o;
        if (appointment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), appointment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Appointment{" +
            "id=" + getId() +
            ", appointmentdate='" + getAppointmentdate() + "'" +
//            ", appointmenttime='" + getAppointmenttime() + "'" +
            ", appointmentstatus='" + getAppointmentstatus() + "'" +
            "}";
    }
}
