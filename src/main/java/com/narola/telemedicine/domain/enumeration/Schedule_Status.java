package com.narola.telemedicine.domain.enumeration;

/**
 * The Schedule_Status enumeration.
 */
public enum Schedule_Status {
    ENABLE, DISABLE
}
