package com.narola.telemedicine.domain.enumeration;

/**
 * The Appointment_Status enumeration.
 */
public enum Appointment_Status {
    CONFIRMED, UNCONFIRMED, RESCHEDULE
}
