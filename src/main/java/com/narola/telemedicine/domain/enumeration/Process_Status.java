package com.narola.telemedicine.domain.enumeration;

/**
 * The Process_Status enumeration.
 */
public enum Process_Status {
    ACTIVE, DEACTIVE
}
