package com.narola.telemedicine.domain.enumeration;

/**
 * The Days enumeration.
 */
public enum Days {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}
