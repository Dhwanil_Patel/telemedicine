package com.narola.telemedicine.domain.enumeration;

/**
 * The Payment_Status enumeration.
 */
public enum Payment_Status {
    PAID, UNPAID
}
