package com.narola.telemedicine.domain.enumeration;

/**
 * Created by pdhwanil on 24-03-2018.
 */
public enum  Time_Slots {
    BEFORE_BREAKFAST, AFTER_BREAKFAST, BEFORE_LUNCH, AFTER_LUNCH, BEFORE_EVENING_MEAL, AFTER_EVENING_MEAL, BEFORE_DINNER, AFTER_DINNER
}
