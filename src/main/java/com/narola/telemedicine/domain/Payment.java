package com.narola.telemedicine.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Payment.
 */
@Entity
@Table(name = "payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "payment")
public class Payment extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "paymentdate", nullable = false)
    private LocalDate paymentdate;

    @NotNull
    @Column(name = "totalamount", nullable = false)
    private Integer totalamount;

    @NotNull
    @Column(name = "paymentmethod", nullable = false)
    private String paymentmethod;

    @NotNull
    @Column(name = "transactionid", nullable = false)
    private String transactionid;

    @OneToOne
    @JoinColumn(unique = true)
    private Appointment appointment;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPaymentdate() {
        return paymentdate;
    }

    public Payment paymentdate(LocalDate paymentdate) {
        this.paymentdate = paymentdate;
        return this;
    }

    public void setPaymentdate(LocalDate paymentdate) {
        this.paymentdate = paymentdate;
    }

    public Integer getTotalamount() {
        return totalamount;
    }

    public Payment totalamount(Integer totalamount) {
        this.totalamount = totalamount;
        return this;
    }

    public void setTotalamount(Integer totalamount) {
        this.totalamount = totalamount;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public Payment paymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
        return this;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public Payment transactionid(String transactionid) {
        this.transactionid = transactionid;
        return this;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public Payment appointment(Appointment appointment) {
        this.appointment = appointment;
        return this;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payment payment = (Payment) o;
        if (payment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), payment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", paymentdate='" + getPaymentdate() + "'" +
            ", totalamount=" + getTotalamount() +
            ", paymentmethod='" + getPaymentmethod() + "'" +
            ", transactionid='" + getTransactionid() + "'" +
            "}";
    }
}
