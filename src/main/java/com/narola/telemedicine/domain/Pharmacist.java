package com.narola.telemedicine.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

import com.narola.telemedicine.domain.enumeration.Process_Status;

/**
 * A Pharmacist.
 */
@Entity
@Table(name = "pharmacist")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pharmacist")
public class  Pharmacist extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "shopname", nullable = false)
    private String shopname;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Process_Status status;

    @NotNull
    @Column(name = "shopaddress", nullable = false)
    private String shopaddress;

    @NotNull
    @Column(name = "phoneno", nullable = false)
    private String phoneno;

    @NotNull
    @Column(name = "licenceno", nullable = false)
    private String licenceno;

    @NotNull
    @Lob
    @Column(name = "documents", nullable = false)
    private byte[] documents;

    @Column(name = "documents_content_type", nullable = false)
    private String documentsContentType;

//    @OneToOne(mappedBy = "pharmacist")
//    @JsonIgnore
//    private OrderMedicine orderMedicine;


    //    added user_id from User
    @OneToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShopname() {
        return shopname;
    }

    public Pharmacist shopname(String shopname) {
        this.shopname = shopname;
        return this;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public Process_Status getStatus() {
        return status;
    }

    public Pharmacist status(Process_Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Process_Status status) {
        this.status = status;
    }

    public String getShopaddress() {
        return shopaddress;
    }

    public Pharmacist shopaddress(String shopaddress) {
        this.shopaddress = shopaddress;
        return this;
    }

    public void setShopaddress(String shopaddress) {
        this.shopaddress = shopaddress;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public Pharmacist phoneno(String phoneno) {
        this.phoneno = phoneno;
        return this;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getLicenceno() {
        return licenceno;
    }

    public Pharmacist licenceno(String licenceno) {
        this.licenceno = licenceno;
        return this;
    }

    public void setLicenceno(String licenceno) {
        this.licenceno = licenceno;
    }

    public byte[] getDocuments() {
        return documents;
    }

    public Pharmacist documents(byte[] documents) {
        this.documents = documents;
        return this;
    }

    public void setDocuments(byte[] documents) {
        this.documents = documents;
    }

    public String getDocumentsContentType() {
        return documentsContentType;
    }

    public Pharmacist documentsContentType(String documentsContentType) {
        this.documentsContentType = documentsContentType;
        return this;
    }

    public void setDocumentsContentType(String documentsContentType) {
        this.documentsContentType = documentsContentType;
    }

   /* public OrderMedicine getOrderMedicine() {
        return orderMedicine;
    }

    public Pharmacist orderMedicine(OrderMedicine orderMedicine) {
        this.orderMedicine = orderMedicine;
        return this;
    }

    public void setOrderMedicine(OrderMedicine orderMedicine) {
        this.orderMedicine = orderMedicine;
    }*/
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    //    added getter setter to link user_id in doctor
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Pharmacist user(User user){
        this.user = user;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pharmacist pharmacist = (Pharmacist) o;
        if (pharmacist.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pharmacist.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pharmacist{" +
            "id=" + getId() +
            ", shopname='" + getShopname() + "'" +
            ", status='" + getStatus() + "'" +
            ", shopaddress='" + getShopaddress() + "'" +
            ", phoneno='" + getPhoneno() + "'" +
            ", licenceno='" + getLicenceno() + "'" +
            ", documents='" + getDocuments() + "'" +
            ", documentsContentType='" + getDocumentsContentType() + "'" +
            "}";
    }
}
