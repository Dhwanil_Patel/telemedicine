package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Prescription;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Prescription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {

    @Query(nativeQuery = true,value = "SELECT pre.* FROM prescription pre, appointment a, patient p WHERE pre.appointment_id= a.id and a.patient_id= p.id and p.id=:id")
    List<Prescription> findPrescriptionByPatientId(@Param("id") Long id);

    @Query(nativeQuery = true,value = "SELECT pre.* FROM prescription pre, appointment a, doctor d WHERE pre.appointment_id= a.id and a.doctor_id= d.id and d.id=:id")
    List<Prescription> findPrescriptionByDoctorId(@Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT pre.* FROM prescription pre,patient p WHERE p.id=:id and pre.id NOT in (SELECT order_medicine.prescription_id FROM order_medicine)")
    List<Prescription> findPrescriptionByPatientIdWhereOrderMedicineIsNull(@Param("id") Long id);

}
