package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.PatientIssue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PatientIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientIssueRepository extends JpaRepository<PatientIssue, Long> {

}
