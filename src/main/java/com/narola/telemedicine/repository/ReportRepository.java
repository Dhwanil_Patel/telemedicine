package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pdhwanil on 01-06-2018.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportRepository extends JpaRepository<Patient, Long> {
}
