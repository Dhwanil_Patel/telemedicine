package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Specialization;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;


/**
 * Spring Data JPA repository for the Specialization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpecializationRepository extends JpaRepository<Specialization, Long> {

    Optional<Specialization> findOneByExpertiseIgnoreCase(String expertise);
}
