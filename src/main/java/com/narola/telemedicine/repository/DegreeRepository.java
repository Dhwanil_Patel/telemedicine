package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Degree;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Degree entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DegreeRepository extends JpaRepository<Degree, Long> {

}
