package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Doctor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Doctor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    //find bu usreid
    Doctor findByUserId(Long id);
    @Query(value = "select COUNT(id) from doctor;",nativeQuery = true)
    int countOfDoctor();
}
