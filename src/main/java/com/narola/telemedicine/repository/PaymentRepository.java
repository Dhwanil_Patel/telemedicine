package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Payment;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


/**
 * Spring Data JPA repository for the Payment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    @Query(nativeQuery = true, value = "SELECT p.* FROM payment p, appointment a, doctor d WHERE p.appointment_id=a.id and a.doctor_id=d.id and d.id=:id")
    List<Payment> findPaymentByDoctorId(@Param("id") Long id);

    @Query(nativeQuery = true , value = "SELECT p.* FROM payment p, appointment a, patient pa WHERE p.appointment_id=a.id and a.patient_id=pa.id and pa.id=:id")
    List<Payment> findPaymentByPatientId(@Param("id") Long id);
}
