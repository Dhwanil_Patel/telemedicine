package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.Prescription;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Prescription entity.
 */
public interface PrescriptionSearchRepository extends ElasticsearchRepository<Prescription, Long> {
}
