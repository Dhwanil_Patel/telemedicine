package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.OrderMedicine;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the OrderMedicine entity.
 */
public interface OrderMedicineSearchRepository extends ElasticsearchRepository<OrderMedicine, Long> {
}
