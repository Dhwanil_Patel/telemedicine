package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.Patient;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by pdhwanil on 01-06-2018.
 */
public interface ReportSearchRepository extends ElasticsearchRepository<Patient, Long> {
}
