package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.PatientIssue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PatientIssue entity.
 */
public interface PatientIssueSearchRepository extends ElasticsearchRepository<PatientIssue, Long> {
}
