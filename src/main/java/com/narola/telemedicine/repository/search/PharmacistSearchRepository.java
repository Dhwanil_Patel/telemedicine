package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.Pharmacist;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Pharmacist entity.
 */
public interface PharmacistSearchRepository extends ElasticsearchRepository<Pharmacist, Long> {
}
