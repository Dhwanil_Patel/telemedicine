package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.Specialization;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Specialization entity.
 */
public interface SpecializationSearchRepository extends ElasticsearchRepository<Specialization, Long> {
}
