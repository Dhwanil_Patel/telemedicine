package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.DoctorSchedule;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DoctorSchedule entity.
 */
public interface DoctorScheduleSearchRepository extends ElasticsearchRepository<DoctorSchedule, Long> {
}
