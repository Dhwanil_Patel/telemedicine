package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.PharmacistPayment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PharmacistPayment entity.
 */
public interface PharmacistPaymentSearchRepository extends ElasticsearchRepository<PharmacistPayment, Long> {
}
