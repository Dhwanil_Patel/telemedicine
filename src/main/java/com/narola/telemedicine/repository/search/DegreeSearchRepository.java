package com.narola.telemedicine.repository.search;

import com.narola.telemedicine.domain.Degree;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Degree entity.
 */
public interface DegreeSearchRepository extends ElasticsearchRepository<Degree, Long> {
}
