package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.PharmacistPayment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the PharmacistPayment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PharmacistPaymentRepository extends JpaRepository<PharmacistPayment, Long> {

    @Query( nativeQuery = true, value = "select p.* from pharmacist_payment p,order_medicine o,pharmacist ph where o.pharmacist_id=:id and ph.id =:id and p.order_medicine_id=o.id")
    List<PharmacistPayment> findByPharmacistId(@Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT DISTINCT p.* from pharmacist_payment p , order_medicine o , prescription pr , appointment a, patient pa WHERE p.order_medicine_id = o.id and o.prescription_id= pr.id and pr.appointment_id= a.id and a.patient_id=:id")
    List<PharmacistPayment> findByPatientId(@Param("id") Long id);
}
