package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Appointment;
import com.narola.telemedicine.domain.DoctorSchedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DoctorSchedule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoctorScheduleRepository extends JpaRepository<DoctorSchedule, Long> {
    Page<DoctorSchedule> findByDoctorId(Long id,Pageable pageable);
}
