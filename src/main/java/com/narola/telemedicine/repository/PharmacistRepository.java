package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Pharmacist;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Pharmacist entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PharmacistRepository extends JpaRepository<Pharmacist, Long> {

    //find by userid
    Pharmacist findByUserId(Long id);
}
