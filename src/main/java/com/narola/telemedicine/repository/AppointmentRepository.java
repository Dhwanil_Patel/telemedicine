package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Appointment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

//    @Query(nativeQuery = true, value = "SELECT a.*,d.firstname,p.firstname,pa.issue FROM appointment a, doctor d, patient p ,patient_issue pa WHERE a.doctor_id=d.id and a.patient_id=p.id and a.patientissue_id=pa.id and d.id=:id")
//    List<Appointment> findAppointmentWithDetails(@Param("id") Long id);

    Page<Appointment> findByDoctorId(Long id, org.springframework.data.domain.Pageable pageable);
    Page<Appointment> findByPatientId(Long id, org.springframework.data.domain.Pageable pageable);

    // new query for doctor name
//    @Query(nativeQuery = true, value = "SELECT a.*,d.firstname FROM appointment a,doctor d WHERE a.doctor_id=d.id and d.id=:id")
//    List<Appointment> finddoctor(@Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM `appointment` WHERE patient_id=:id and `id` NOT IN (SELECT payment.appointment_id FROM payment)")
    List<Appointment> findByPatientIdWherePaymentIsNull(@Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM `appointment` WHERE doctor_id=:id and `id` NOT IN (SELECT prescription.appointment_id FROM prescription)")
    List<Appointment> findByDoctorIdWherePrescriptionIsNull(@Param("id") Long id);
}
