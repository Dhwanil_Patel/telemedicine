package com.narola.telemedicine.repository;

import afu.org.checkerframework.checker.oigj.qual.O;
import com.narola.telemedicine.domain.OrderMedicine;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the OrderMedicine entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderMedicineRepository extends JpaRepository<OrderMedicine, Long> {
    // get ordermedicine by patientid
    @Query(nativeQuery = true,value = "SELECT o.* FROM order_medicine o , prescription pre , appointment a, patient p WHERE o.prescription_id=pre.id and pre.appointment_id=a.id and a.patient_id=:id and p.id=:id")
    List<OrderMedicine> findOrderMedicineByPatientId(@Param("id") Long id);

    // get ordermedicine by pharmacist
    @Query(nativeQuery = true, value = "SELECT o.* FROM order_medicine o , pharmacist p WHERE o.pharmacist_id=p.id and p.id=:id")
    List<OrderMedicine> findOrderMedicineByPharmacistId(@Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT o.* FROM order_medicine o , prescription pre , appointment a, patient p WHERE o.prescription_id=pre.id and pre.appointment_id=a.id and a.patient_id=:id and p.id=:id and o.id not in (SELECT pharmacist_payment.order_medicine_id FROM pharmacist_payment)")
    List<OrderMedicine> findOrderMedicineByPatientIdWherePharmacistPaymentIsNull(@Param("id") Long id);
}
