package com.narola.telemedicine.repository;

import com.narola.telemedicine.domain.Patient;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Patient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    //find by userid
    Patient findByUserId(Long id);
}
