package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.PharmacistDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Pharmacist.
 */
public interface PharmacistService {

    /**
     * Save a pharmacist.
     *
     * @param pharmacistDTO the entity to save
     * @return the persisted entity
     */
    PharmacistDTO save(PharmacistDTO pharmacistDTO);

    /**
     * Get all the pharmacists.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PharmacistDTO> findAll(Pageable pageable);
    /**
     * Get all the PharmacistDTO where OrderMedicine is null.
     *
     * @return the list of entities
     */
    // List<PharmacistDTO> findAllWhereOrderMedicineIsNull();

    /**
     * Get the "id" pharmacist.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PharmacistDTO findOne(Long id);

    //find by usreid
    PharmacistDTO findByUserId(Long id);

    /**
     * Delete the "id" pharmacist.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the pharmacist corresponding to the query.
     *
     * @param query the query of the search
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PharmacistDTO> search(String query, Pageable pageable);
}
