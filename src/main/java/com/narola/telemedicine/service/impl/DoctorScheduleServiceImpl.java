package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.service.DoctorScheduleService;
import com.narola.telemedicine.domain.DoctorSchedule;
import com.narola.telemedicine.repository.DoctorScheduleRepository;
import com.narola.telemedicine.repository.search.DoctorScheduleSearchRepository;
import com.narola.telemedicine.service.dto.AppointmentDTO;
import com.narola.telemedicine.service.dto.DoctorScheduleDTO;
import com.narola.telemedicine.service.mapper.DoctorScheduleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DoctorSchedule.
 */
@Service
@Transactional
public class DoctorScheduleServiceImpl implements DoctorScheduleService {

    private final Logger log = LoggerFactory.getLogger(DoctorScheduleServiceImpl.class);

    private final DoctorScheduleRepository doctorScheduleRepository;

    private final DoctorScheduleMapper doctorScheduleMapper;

    private final DoctorScheduleSearchRepository doctorScheduleSearchRepository;

    public DoctorScheduleServiceImpl(DoctorScheduleRepository doctorScheduleRepository, DoctorScheduleMapper doctorScheduleMapper, DoctorScheduleSearchRepository doctorScheduleSearchRepository) {
        this.doctorScheduleRepository = doctorScheduleRepository;
        this.doctorScheduleMapper = doctorScheduleMapper;
        this.doctorScheduleSearchRepository = doctorScheduleSearchRepository;
    }

    /**
     * Save a doctorSchedule.
     *
     * @param doctorScheduleDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DoctorScheduleDTO save(DoctorScheduleDTO doctorScheduleDTO) {
        log.debug("Request to save DoctorSchedule : {}", doctorScheduleDTO);
        DoctorSchedule doctorSchedule = doctorScheduleMapper.toEntity(doctorScheduleDTO);
        doctorSchedule = doctorScheduleRepository.save(doctorSchedule);
        DoctorScheduleDTO result = doctorScheduleMapper.toDto(doctorSchedule);
        doctorScheduleSearchRepository.save(doctorSchedule);
        return result;
    }

    /**
     * Get all the doctorSchedules.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DoctorScheduleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DoctorSchedules");
        return doctorScheduleRepository.findAll(pageable)
            .map(doctorScheduleMapper::toDto);
    }

    /**
     * Get one doctorSchedule by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DoctorScheduleDTO findOne(Long id) {
        log.debug("Request to get DoctorSchedule : {}", id);
        DoctorSchedule doctorSchedule = doctorScheduleRepository.findOne(id);
        return doctorScheduleMapper.toDto(doctorSchedule);
    }

    /**
     * Delete the doctorSchedule by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DoctorSchedule : {}", id);
        doctorScheduleRepository.delete(id);
        doctorScheduleSearchRepository.delete(id);
    }

//    //doctorschedule by doctor id
//    @Override
//    @Transactional(readOnly = true)
//    public Page<DoctorScheduleDTO> findByDoctorId(Long id, Pageable pageable) {
//        return doctorScheduleRepository.findByDoctorId(id,pageable)
//            .map(doctorScheduleMapper::toDto);
//    }

    @Override
    @Transactional(readOnly = true)
    public Page<DoctorScheduleDTO> findByDoctorId(Long id,Pageable pageable) {
        log.debug("Request to get all DoctorSchedules");
        return doctorScheduleRepository.findByDoctorId(id,pageable)
            .map(doctorScheduleMapper::toDto);
    }
    /**
     * Search for the doctorSchedule corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DoctorScheduleDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DoctorSchedules for query {}", query);
        Page<DoctorSchedule> result = doctorScheduleSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(doctorScheduleMapper::toDto);
    }
}
