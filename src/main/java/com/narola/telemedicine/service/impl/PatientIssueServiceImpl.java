package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.service.PatientIssueService;
import com.narola.telemedicine.domain.PatientIssue;
import com.narola.telemedicine.repository.PatientIssueRepository;
import com.narola.telemedicine.repository.search.PatientIssueSearchRepository;
import com.narola.telemedicine.service.dto.PatientIssueDTO;
import com.narola.telemedicine.service.mapper.PatientIssueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PatientIssue.
 */
@Service
@Transactional
public class PatientIssueServiceImpl implements PatientIssueService {

    private final Logger log = LoggerFactory.getLogger(PatientIssueServiceImpl.class);

    private final PatientIssueRepository patientIssueRepository;

    private final PatientIssueMapper PatientIssueMapper;

    private final PatientIssueSearchRepository PatientIssueSearchRepository;

    public PatientIssueServiceImpl(PatientIssueRepository patientIssueRepository, PatientIssueMapper PatientIssueMapper, PatientIssueSearchRepository PatientIssueSearchRepository) {
        this.patientIssueRepository = patientIssueRepository;
        this.PatientIssueMapper = PatientIssueMapper;
        this.PatientIssueSearchRepository = PatientIssueSearchRepository;
    }

    /**
     * Save a PatientIssue.
     *
     * @param PatientIssueDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PatientIssueDTO save(PatientIssueDTO PatientIssueDTO) {
        log.debug("Request to save PatientIssue : {}", PatientIssueDTO);
        PatientIssue PatientIssue = PatientIssueMapper.toEntity(PatientIssueDTO);
        PatientIssue = patientIssueRepository.save(PatientIssue);
        PatientIssueDTO result = PatientIssueMapper.toDto(PatientIssue);
        PatientIssueSearchRepository.save(PatientIssue);
        return result;
    }

    /**
     * Get all the PatientIssue.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PatientIssueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PatientIssue");
        return patientIssueRepository.findAll(pageable)
            .map(PatientIssueMapper::toDto);
    }

    /**
     * Get one PatientIssue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PatientIssueDTO findOne(Long id) {
        log.debug("Request to get PatientIssue : {}", id);
        PatientIssue PatientIssue = patientIssueRepository.findOne(id);
        return PatientIssueMapper.toDto(PatientIssue);
    }

    /**
     * Delete the PatientIssue by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PatientIssue : {}", id);
        patientIssueRepository.delete(id);
        PatientIssueSearchRepository.delete(id);
    }

    /**
     * Search for the PatientIssue corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PatientIssueDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PatientIssue for query {}", query);
        Page<PatientIssue> result = PatientIssueSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(PatientIssueMapper::toDto);
    }
}
