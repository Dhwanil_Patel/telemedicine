package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.service.PrescriptionService;
import com.narola.telemedicine.domain.Prescription;
import com.narola.telemedicine.repository.PrescriptionRepository;
import com.narola.telemedicine.repository.search.PrescriptionSearchRepository;
import com.narola.telemedicine.service.dto.PrescriptionDTO;
import com.narola.telemedicine.service.mapper.PrescriptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Prescription.
 */
@Service
@Transactional
public class PrescriptionServiceImpl implements PrescriptionService {

    private final Logger log = LoggerFactory.getLogger(PrescriptionServiceImpl.class);

    private final PrescriptionRepository prescriptionRepository;

    private final PrescriptionMapper prescriptionMapper;

    private final PrescriptionSearchRepository prescriptionSearchRepository;

    public PrescriptionServiceImpl(PrescriptionRepository prescriptionRepository, PrescriptionMapper prescriptionMapper, PrescriptionSearchRepository prescriptionSearchRepository) {
        this.prescriptionRepository = prescriptionRepository;
        this.prescriptionMapper = prescriptionMapper;
        this.prescriptionSearchRepository = prescriptionSearchRepository;
    }

    /**
     * Save a prescription.
     *
     * @param prescriptionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PrescriptionDTO save(PrescriptionDTO prescriptionDTO) {
        log.debug("Request to save Prescription : {}", prescriptionDTO);
        Prescription prescription = prescriptionMapper.toEntity(prescriptionDTO);
        prescription = prescriptionRepository.save(prescription);
        PrescriptionDTO result = prescriptionMapper.toDto(prescription);
        prescriptionSearchRepository.save(prescription);
        return result;
    }

    /**
     * Get all the prescriptions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PrescriptionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Prescriptions");
        return prescriptionRepository.findAll(pageable)
            .map(prescriptionMapper::toDto);
    }

    // find by doctor id
    @Override
    public List<PrescriptionDTO> findPrescriptionByDoctorId(Long id) {
        log.debug("Request to get all Prescriptions");
        return prescriptionMapper.toDto(prescriptionRepository.findPrescriptionByDoctorId(id));
    }

    // find by patient id
    @Override
    public List<PrescriptionDTO> findPrescriptionByPatientId(Long id) {
        log.debug("Request to get all Prescriptions");
        return prescriptionMapper.toDto(prescriptionRepository.findPrescriptionByPatientId(id));
    }


    /**
     *  get all the prescriptions where OrderMedicine is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PrescriptionDTO> findAllWhereOrderMedicineIsNull() {
        log.debug("Request to get all prescriptions where OrderMedicine is null");
        return StreamSupport
            .stream(prescriptionRepository.findAll().spliterator(), false)
            .filter(prescription -> prescription.getOrderMedicine() == null)
            .map(prescriptionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    // find PrescriptionBy PatientId WhereOrderMedicine Is Null
    @Override
    public List<PrescriptionDTO> findPrescriptionByPatientIdWhereOrderMedicineIsNull(Long id) {
        List<Prescription> prescriptions = prescriptionRepository.findPrescriptionByPatientIdWhereOrderMedicineIsNull(id);
        return prescriptionMapper.toDto(prescriptions);
    }

    /**
     * Get one prescription by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PrescriptionDTO findOne(Long id) {
        log.debug("Request to get Prescription : {}", id);
        Prescription prescription = prescriptionRepository.findOne(id);
        return prescriptionMapper.toDto(prescription);
    }

    /**
     * Delete the prescription by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Prescription : {}", id);
        prescriptionRepository.delete(id);
        prescriptionSearchRepository.delete(id);
    }

    /**
     * Search for the prescription corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PrescriptionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Prescriptions for query {}", query);
        Page<Prescription> result = prescriptionSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(prescriptionMapper::toDto);
    }
}
