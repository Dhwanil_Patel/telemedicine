package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.service.DoctorService;
import com.narola.telemedicine.service.PatientService;
import com.narola.telemedicine.service.ReportService;
import com.narola.telemedicine.service.dto.PatientDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by pdhwanil on 01-06-2018.
 */
@Service
public class ReportServiceImpl implements ReportService {

    private final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);

    private final PatientService patientService;

    public ReportServiceImpl(PatientService patientService) {
        this.patientService = patientService;
    }

    @Override
    public Page<PatientDTO> findAllPatient(Pageable pageable) {
        return patientService.findAll(pageable);
    }
}
