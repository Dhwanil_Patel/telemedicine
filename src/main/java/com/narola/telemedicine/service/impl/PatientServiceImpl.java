package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.service.PatientService;
import com.narola.telemedicine.domain.Patient;
import com.narola.telemedicine.repository.PatientRepository;
import com.narola.telemedicine.repository.search.PatientSearchRepository;
import com.narola.telemedicine.service.dto.PatientDTO;
import com.narola.telemedicine.service.mapper.PatientMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Patient.
 */
@Service
@Transactional
public class PatientServiceImpl implements PatientService {

    private final Logger log = LoggerFactory.getLogger(PatientServiceImpl.class);

    private final PatientRepository patientRepository;

    private final PatientMapper patientMapper;

    private final PatientSearchRepository patientSearchRepository;

    public PatientServiceImpl(PatientRepository patientRepository, PatientMapper patientMapper, PatientSearchRepository patientSearchRepository) {
        this.patientRepository = patientRepository;
        this.patientMapper = patientMapper;
        this.patientSearchRepository = patientSearchRepository;
    }

    /**
     * Save a patient.
     *
     * @param patientDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PatientDTO save(PatientDTO patientDTO) {
        log.debug("Request to save Patient : {}", patientDTO);
        User user = new User();
        Patient patient = patientMapper.toEntity(patientDTO);
        user.setId(patientDTO.getUserId());
        patient.setUser(user);
        patient = patientRepository.save(patient);
        PatientDTO result = patientMapper.toDto(patient);
        patientSearchRepository.save(patient);
        return result;
    }

    /**
     * Get all the patients.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PatientDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Patients");
        return patientRepository.findAll(pageable)
            .map(patientMapper::toDto);
    }

    /**
     * Get one patient by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PatientDTO findOne(Long id) {
        log.debug("Request to get Patient : {}", id);
        Patient patient = patientRepository.findOne(id);
        return patientMapper.toDto(patient);
    }

    // get patient by userid
    @Override
    @Transactional(readOnly = true)
    public PatientDTO findByUserId(Long id){
        log.debug("Request to get patient : {}", id);
        Patient patient = patientRepository.findByUserId(id);
        return patientMapper.toDto(patient);
    }

    /**
     * Delete the patient by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Patient : {}", id);
        patientRepository.delete(id);
        patientSearchRepository.delete(id);
    }

    /**
     * Search for the patient corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PatientDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Patients for query {}", query);
        Page<Patient> result = patientSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(patientMapper::toDto);
    }
}
