package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.service.OrderMedicineService;
import com.narola.telemedicine.domain.OrderMedicine;
import com.narola.telemedicine.repository.OrderMedicineRepository;
import com.narola.telemedicine.repository.search.OrderMedicineSearchRepository;
import com.narola.telemedicine.service.dto.OrderMedicineDTO;
import com.narola.telemedicine.service.mapper.OrderMedicineMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing OrderMedicine.
 */
@Service
@Transactional
public class OrderMedicineServiceImpl implements OrderMedicineService {

    private final Logger log = LoggerFactory.getLogger(OrderMedicineServiceImpl.class);

    private final OrderMedicineRepository orderMedicineRepository;

    private final OrderMedicineMapper orderMedicineMapper;

    private final OrderMedicineSearchRepository orderMedicineSearchRepository;

    public OrderMedicineServiceImpl(OrderMedicineRepository orderMedicineRepository, OrderMedicineMapper orderMedicineMapper, OrderMedicineSearchRepository orderMedicineSearchRepository) {
        this.orderMedicineRepository = orderMedicineRepository;
        this.orderMedicineMapper = orderMedicineMapper;
        this.orderMedicineSearchRepository = orderMedicineSearchRepository;
    }

    /**
     * Save a orderMedicine.
     *
     * @param orderMedicineDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public OrderMedicineDTO save(OrderMedicineDTO orderMedicineDTO) {
        log.debug("Request to save OrderMedicine : {}", orderMedicineDTO);
        OrderMedicine orderMedicine = orderMedicineMapper.toEntity(orderMedicineDTO);
        orderMedicine = orderMedicineRepository.save(orderMedicine);
        OrderMedicineDTO result = orderMedicineMapper.toDto(orderMedicine);
        orderMedicineSearchRepository.save(orderMedicine);
        return result;
    }

    /**
     * Get all the orderMedicines.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrderMedicineDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderMedicines");
        return orderMedicineRepository.findAll(pageable)
            .map(orderMedicineMapper::toDto);
    }


    /**
     *  get all the orderMedicines where PharmacistPayment is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<OrderMedicineDTO> findAllWherePharmacistPaymentIsNull() {
        log.debug("Request to get all orderMedicines where PharmacistPayment is null");
        return StreamSupport
            .stream(orderMedicineRepository.findAll().spliterator(), false)
            .filter(orderMedicine -> orderMedicine.getPharmacistPayment() == null)
            .map(orderMedicineMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<OrderMedicineDTO> findOrderMedicineByPatientIdWherePharmacistPaymentIsNull(Long id) {
        log.debug("Request to get all orderMedicines by patient id where PharmacistPayment is null");
        List<OrderMedicine> orderMedicines = orderMedicineRepository.findOrderMedicineByPatientIdWherePharmacistPaymentIsNull(id);
        return orderMedicineMapper.toDto(orderMedicines);
    }

    @Override
    public List<OrderMedicineDTO> findOrderMedicineByPatientId(Long id) {
        return orderMedicineMapper.toDto(orderMedicineRepository.findOrderMedicineByPatientId(id));
    }

    @Override
    public List<OrderMedicineDTO> findOrderMedicineByPharmacistId(Long id) {
        return orderMedicineMapper.toDto(orderMedicineRepository.findOrderMedicineByPharmacistId(id));
    }

    /**
     * Get one orderMedicine by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public OrderMedicineDTO findOne(Long id) {
        log.debug("Request to get OrderMedicine : {}", id);
        OrderMedicine orderMedicine = orderMedicineRepository.findOne(id);
        return orderMedicineMapper.toDto(orderMedicine);
    }

    /**
     * Delete the orderMedicine by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderMedicine : {}", id);
        orderMedicineRepository.delete(id);
        orderMedicineSearchRepository.delete(id);
    }

    /**
     * Search for the orderMedicine corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrderMedicineDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of OrderMedicines for query {}", query);
        Page<OrderMedicine> result = orderMedicineSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(orderMedicineMapper::toDto);
    }
}
