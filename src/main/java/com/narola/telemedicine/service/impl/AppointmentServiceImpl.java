package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.service.AppointmentService;
import com.narola.telemedicine.domain.Appointment;
import com.narola.telemedicine.repository.AppointmentRepository;
import com.narola.telemedicine.repository.search.AppointmentSearchRepository;
import com.narola.telemedicine.service.PaymentService;
import com.narola.telemedicine.service.dto.AppointmentDTO;
import com.narola.telemedicine.service.dto.PaymentDTO;
import com.narola.telemedicine.service.mapper.AppointmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Appointment.
 */
@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

    EntityManager entityManager;

    private final Logger log = LoggerFactory.getLogger(AppointmentServiceImpl.class);

    private final AppointmentRepository appointmentRepository;

    private final AppointmentMapper appointmentMapper;

    private final AppointmentSearchRepository appointmentSearchRepository;

    @Inject
    private PaymentService paymentService;

    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, AppointmentMapper appointmentMapper, AppointmentSearchRepository appointmentSearchRepository) {
        this.appointmentRepository = appointmentRepository;
        this.appointmentMapper = appointmentMapper;
        this.appointmentSearchRepository = appointmentSearchRepository;
    }

    /**
     * Save a appointment.
     *
     * @param appointmentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AppointmentDTO save(AppointmentDTO appointmentDTO) {
        PaymentDTO newPaymentDTO =new PaymentDTO();
        String txnid = "";
            Random rand = new Random();
            String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            txnid = rndm;
        log.debug("Request to save Appointment : {}", appointmentDTO);
        Appointment appointment = appointmentMapper.toEntity(appointmentDTO);
        appointment = appointmentRepository.save(appointment);
        AppointmentDTO result = appointmentMapper.toDto(appointment);
        newPaymentDTO.setAppointmentId(result.getId());
        newPaymentDTO.setPaymentdate(result.getAppointmentdate());
        newPaymentDTO.setPaymentmethod("card");
        newPaymentDTO.setTotalamount(10);
        newPaymentDTO.setTransactionid(txnid);
        PaymentDTO resultdate = paymentService.save(newPaymentDTO);
        appointmentSearchRepository.save(appointment);
        return result;
    }

    /**
     * Get all the appointments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AppointmentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Appointments");
        return appointmentRepository.findAll(pageable)
            .map(appointmentMapper::toDto);
    }

    // doctor name
//    @Override
//    @Transactional
//    public List<Appointment> finddoctor(Long id) {
//        return appointmentRepository.finddoctor(id);
////        return appointmentRepository.finddoctor(id)
////            .map(appointmentMapper::toDto);
//    }

    /**
     *  get all the appointments where Payment is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AppointmentDTO> findAllWherePaymentIsNull() {
        log.debug("Request to get all appointments where Payment is null");
        return StreamSupport
            .stream(appointmentRepository.findAll().spliterator(), false)
            .filter(appointment -> appointment.getPayment() == null)
            .map(appointmentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  get all the appointments where Prescription is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AppointmentDTO> findAllWherePrescriptionIsNull() {
        log.debug("Request to get all appointments where Prescription is null");
        return StreamSupport
            .stream(appointmentRepository.findAll().spliterator(), false)
            .filter(appointment -> appointment.getPrescription() == null)
            .map(appointmentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<AppointmentDTO> findPatientIdWherePaymentIsNull(Long id) {
        log.debug("Request to get all appointments where payment is null by patient id");
        List<Appointment> appointments = appointmentRepository.findByPatientIdWherePaymentIsNull(id);
        return appointmentMapper.toDto(appointments);
    }

    @Override
    public List<AppointmentDTO> findDoctorIdWherePrescriptionIsNull(Long id) {
        log.debug("Request to get all appointments where Prescription is null by doctor id");
        List<Appointment> appointments = appointmentRepository.findByDoctorIdWherePrescriptionIsNull(id);
        return appointmentMapper.toDto(appointments);
    }

    /**
     * Get one appointment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */

    @Override
    @Transactional(readOnly = true)
    public AppointmentDTO findOne(Long id) {
        log.debug("Request to get Appointment : {}", id);
        Appointment appointment = appointmentRepository.findOne(id);
        return appointmentMapper.toDto(appointment);
    }
    //appointment by doctor id
    @Override
    @Transactional(readOnly = true)
    public Page<AppointmentDTO> findByDoctorId(Long id,Pageable pageable) {
        return appointmentRepository.findByDoctorId(id,pageable)
            .map(appointmentMapper::toDto);
    }

//    @Override
//    @Transactional(readOnly = true)
//    public List<AppointmentDTO> findAppointmentWithDetails(Long id) {
//        List<Appointment> appointments = appointmentRepository.findAppointmentWithDetails(id);
//        return appointmentMapper.toDto(appointments);
//    }

    // appointment by patient id
    @Override
    @Transactional(readOnly = true)
    public Page<AppointmentDTO> findByPatientId(Long id,Pageable pageable) {
        return appointmentRepository.findByPatientId(id,pageable)
            .map(appointmentMapper::toDto);
    }
    /**
     * Delete the appointment by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Appointment : {}", id);
        appointmentRepository.delete(id);
        appointmentSearchRepository.delete(id);
    }

    /**
     * Search for the appointment corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AppointmentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Appointments for query {}", query);
        Page<Appointment> result = appointmentSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(appointmentMapper::toDto);
    }
//    @Transactional(readOnly = true)
//    public List<AppointmentDTO> appointmentPatientDetails() {
//        List<Appointment> appointments = entityManager.createNativeQuery(
//            "SELECT a.*, p.firstname FROM appointment a,patient p WHERE p.id = a.patient_id", Appointment.class).getResultList();
////
//        return appointments.map(appointmentMapper::toDto);
//    }
}

//package com.narola.telemedicine.service.impl;
//
//import com.narola.telemedicine.service.AppointmentService;
//import com.narola.telemedicine.domain.Appointment;
//import com.narola.telemedicine.repository.AppointmentRepository;
//import com.narola.telemedicine.repository.search.AppointmentSearchRepository;
//import com.narola.telemedicine.service.dto.AppointmentDTO;
//import com.narola.telemedicine.service.mapper.AppointmentMapper;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.LinkedList;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.StreamSupport;
//
//import static org.elasticsearch.index.query.QueryBuilders.*;
//
///**
// * Service Implementation for managing Appointment.
// */
//@Service
//@Transactional
//public class AppointmentServiceImpl implements AppointmentService {
//
//    private final Logger log = LoggerFactory.getLogger(AppointmentServiceImpl.class);
//
//    private final AppointmentRepository appointmentRepository;
//
//    private final AppointmentMapper appointmentMapper;
//
//    private final AppointmentSearchRepository appointmentSearchRepository;
//
//    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, AppointmentMapper appointmentMapper, AppointmentSearchRepository appointmentSearchRepository) {
//        this.appointmentRepository = appointmentRepository;
//        this.appointmentMapper = appointmentMapper;
//        this.appointmentSearchRepository = appointmentSearchRepository;
//    }
//
//    /**
//     * Save a appointment.
//     *
//     * @param appointmentDTO the entity to save
//     * @return the persisted entity
//     */
//    @Override
//    public AppointmentDTO save(AppointmentDTO appointmentDTO) {
//        log.debug("Request to save Appointment : {}", appointmentDTO);
//        Appointment appointment = appointmentMapper.toEntity(appointmentDTO);
//        appointment = appointmentRepository.save(appointment);
//        AppointmentDTO result = appointmentMapper.toDto(appointment);
//        appointmentSearchRepository.save(appointment);
//        return result;
//    }
//
//    /**
//     * Get all the appointments.
//     *
//     * @param pageable the pagination information
//     * @return the list of entities
//     */
//    @Override
//    @Transactional(readOnly = true)
//    public Page<AppointmentDTO> findAll(Pageable pageable) {
//        log.debug("Request to get all Appointments");
//        return appointmentRepository.findAll(pageable)
//            .map(appointmentMapper::toDto);
//    }
//
//
//    /**
//     *  get all the appointments where Payment is null.
//     *  @return the list of entities
//     */
//    @Transactional(readOnly = true)
//    public List<AppointmentDTO> findAllWherePaymentIsNull() {
//        log.debug("Request to get all appointments where Payment is null");
//        return StreamSupport
//            .stream(appointmentRepository.findAll().spliterator(), false)
//            .filter(appointment -> appointment.getPayment() == null)
//            .map(appointmentMapper::toDto)
//            .collect(Collectors.toCollection(LinkedList::new));
//    }
//
//
//    /**
//     *  get all the appointments where Prescription is null.
//     *  @return the list of entities
//     */
//    @Transactional(readOnly = true)
//    public List<AppointmentDTO> findAllWherePrescriptionIsNull() {
//        log.debug("Request to get all appointments where Prescription is null");
//        return StreamSupport
//            .stream(appointmentRepository.findAll().spliterator(), false)
//            .filter(appointment -> appointment.getPrescription() == null)
//            .map(appointmentMapper::toDto)
//            .collect(Collectors.toCollection(LinkedList::new));
//    }
//
//    /**
//     * Get one appointment by id.
//     *
//     * @param id the id of the entity
//     * @return the entity
//     */
//    @Override
//    @Transactional(readOnly = true)
//    public AppointmentDTO findOne(Long id) {
//        log.debug("Request to get Appointment : {}", id);
//        Appointment appointment = appointmentRepository.findOne(id);
//        return appointmentMapper.toDto(appointment);
//    }
//
//    /**
//     * Delete the appointment by id.
//     *
//     * @param id the id of the entity
//     */
//    @Override
//    public void delete(Long id) {
//        log.debug("Request to delete Appointment : {}", id);
//        appointmentRepository.delete(id);
//        appointmentSearchRepository.delete(id);
//    }
//
//    /**
//     * Search for the appointment corresponding to the query.
//     *
//     * @param query the query of the search
//     * @param pageable the pagination information
//     * @return the list of entities
//     */
//    @Override
//    @Transactional(readOnly = true)
//    public Page<AppointmentDTO> search(String query, Pageable pageable) {
//        log.debug("Request to search for a page of Appointments for query {}", query);
//        Page<Appointment> result = appointmentSearchRepository.search(queryStringQuery(query), pageable);
//        return result.map(appointmentMapper::toDto);
//    }
//}
