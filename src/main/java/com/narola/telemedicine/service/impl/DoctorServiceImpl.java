package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.service.DoctorService;
import com.narola.telemedicine.domain.Doctor;
import com.narola.telemedicine.repository.DoctorRepository;
import com.narola.telemedicine.repository.search.DoctorSearchRepository;
import com.narola.telemedicine.service.dto.DoctorDTO;
import com.narola.telemedicine.service.mapper.DoctorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.inject.Inject;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Doctor.
 */
@Service
@Transactional
public class DoctorServiceImpl implements DoctorService {

    private final Logger log = LoggerFactory.getLogger(DoctorServiceImpl.class);

    private final DoctorRepository doctorRepository;
    @Inject
    private final DoctorMapper doctorMapper;

    private final DoctorSearchRepository doctorSearchRepository;

    public DoctorServiceImpl(DoctorRepository doctorRepository, DoctorMapper doctorMapper, DoctorSearchRepository doctorSearchRepository) {
        this.doctorRepository = doctorRepository;
        this.doctorMapper = doctorMapper;
        this.doctorSearchRepository = doctorSearchRepository;
    }

    /**
     * Save a doctor.
     *
     * @param doctorDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DoctorDTO save(DoctorDTO doctorDTO) {
        log.debug("Request to save Doctor : {}", doctorDTO);
        User user=new User();
//        user.setId(doctorDTO.getUserId());
        user.setId(doctorDTO.getUserId());
        Doctor doctor = doctorMapper.toEntity(doctorDTO);
        doctor.setUser(user);
        doctor = doctorRepository.save(doctor);
        DoctorDTO result = doctorMapper.toDto(doctor);
        doctorSearchRepository.save(doctor);
        return result;
    }

    /**
     * Get all the doctors.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DoctorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Doctors");
        return doctorRepository.findAll(pageable)
            .map(doctorMapper::toDto);
    }

    /**
     * Get one doctor by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DoctorDTO findOne(Long id) {
        log.debug("Request to get Doctor : {}", id);
        Doctor doctor = doctorRepository.findOne(id);
        return doctorMapper.toDto(doctor);
    }

    // get doctor by userid
    @Override
    @Transactional(readOnly = true)
    public DoctorDTO findByUserId(Long id){
        log.debug("Request to get Doctor : {}", id);
        Doctor doctor = doctorRepository.findByUserId(id);
        return doctorMapper.toDto(doctor);
    }

    /**
     * Delete the doctor by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Doctor : {}", id);
        doctorRepository.delete(id);
        doctorSearchRepository.delete(id);
    }

    /**
     * Search for the doctor corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DoctorDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Doctors for query {}", query);
        Page<Doctor> result = doctorSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(doctorMapper::toDto);
    }

    @Override
    public int countOfDoctor() {
    int count=doctorRepository.countOfDoctor();
        return count;
    }
}
