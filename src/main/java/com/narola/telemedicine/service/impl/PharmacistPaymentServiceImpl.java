package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.domain.OrderMedicine;
import com.narola.telemedicine.repository.OrderMedicineRepository;
import com.narola.telemedicine.service.PharmacistPaymentService;
import com.narola.telemedicine.domain.PharmacistPayment;
import com.narola.telemedicine.repository.PharmacistPaymentRepository;
import com.narola.telemedicine.repository.search.PharmacistPaymentSearchRepository;
import com.narola.telemedicine.service.dto.AppointmentDTO;
import com.narola.telemedicine.service.dto.PharmacistPaymentDTO;
import com.narola.telemedicine.service.mapper.PharmacistPaymentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PharmacistPayment.
 */
@Service
@Transactional
public class PharmacistPaymentServiceImpl implements PharmacistPaymentService {

    private final Logger log = LoggerFactory.getLogger(PharmacistPaymentServiceImpl.class);

    private final PharmacistPaymentRepository pharmacistPaymentRepository;

    private final PharmacistPaymentMapper pharmacistPaymentMapper;

    private final PharmacistPaymentSearchRepository pharmacistPaymentSearchRepository;

    private final OrderMedicineRepository orderMedicineRepository;
    public PharmacistPaymentServiceImpl(OrderMedicineRepository orderMedicineRepository, PharmacistPaymentRepository pharmacistPaymentRepository, PharmacistPaymentMapper pharmacistPaymentMapper, PharmacistPaymentSearchRepository pharmacistPaymentSearchRepository) {
        this.orderMedicineRepository = orderMedicineRepository;
        this.pharmacistPaymentRepository = pharmacistPaymentRepository;
        this.pharmacistPaymentMapper = pharmacistPaymentMapper;
        this.pharmacistPaymentSearchRepository = pharmacistPaymentSearchRepository;
    }

    /**
     * Save a pharmacistPayment.
     *
     * @param pharmacistPaymentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PharmacistPaymentDTO save(PharmacistPaymentDTO pharmacistPaymentDTO) {
        log.debug("Request to save PharmacistPayment : {}", pharmacistPaymentDTO);
        PharmacistPayment pharmacistPayment = pharmacistPaymentMapper.toEntity(pharmacistPaymentDTO);
        pharmacistPayment = pharmacistPaymentRepository.save(pharmacistPayment);
        PharmacistPaymentDTO result = pharmacistPaymentMapper.toDto(pharmacistPayment);
        pharmacistPaymentSearchRepository.save(pharmacistPayment);
        return result;
    }

    /**
     * Get all the pharmacistPayments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PharmacistPaymentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PharmacistPayments");
        return pharmacistPaymentRepository.findAll(pageable)
            .map(pharmacistPaymentMapper::toDto);
    }

    // find pharmacist_payments by phamacist id
    @Override
    @Transactional
    public List<PharmacistPaymentDTO> findByPharmacistId(Long id) {
        return pharmacistPaymentMapper.toDto(pharmacistPaymentRepository.findByPharmacistId(id));
    }
    // find pharmacist_payments by patient id
    @Override
    public List<PharmacistPaymentDTO> findByPatientId(Long id) {
        return pharmacistPaymentMapper.toDto(pharmacistPaymentRepository.findByPatientId(id));
    }

    /**
     * Get one pharmacistPayment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PharmacistPaymentDTO findOne(Long id) {
        log.debug("Request to get PharmacistPayment : {}", id);
        PharmacistPayment pharmacistPayment = pharmacistPaymentRepository.findOne(id);
        return pharmacistPaymentMapper.toDto(pharmacistPayment);
    }

    /**
     * Delete the pharmacistPayment by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PharmacistPayment : {}", id);
        pharmacistPaymentRepository.delete(id);
        pharmacistPaymentSearchRepository.delete(id);
    }

    /**
     * Search for the pharmacistPayment corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PharmacistPaymentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PharmacistPayments for query {}", query);
        Page<PharmacistPayment> result = pharmacistPaymentSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(pharmacistPaymentMapper::toDto);
    }
}
