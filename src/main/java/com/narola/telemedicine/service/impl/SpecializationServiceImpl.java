package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.service.SpecializationService;
import com.narola.telemedicine.domain.Specialization;
import com.narola.telemedicine.repository.SpecializationRepository;
import com.narola.telemedicine.repository.search.SpecializationSearchRepository;
import com.narola.telemedicine.service.dto.SpecializationDTO;
import com.narola.telemedicine.service.mapper.SpecializationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Specialization.
 */
@Service
@Transactional
public class SpecializationServiceImpl implements SpecializationService {

    private final Logger log = LoggerFactory.getLogger(SpecializationServiceImpl.class);

    private final SpecializationRepository specializationRepository;

    private final SpecializationMapper specializationMapper;

    private final SpecializationSearchRepository specializationSearchRepository;

    public SpecializationServiceImpl(SpecializationRepository specializationRepository, SpecializationMapper specializationMapper, SpecializationSearchRepository specializationSearchRepository) {
        this.specializationRepository = specializationRepository;
        this.specializationMapper = specializationMapper;
        this.specializationSearchRepository = specializationSearchRepository;
    }

    /**
     * Save a specialization.
     *
     * @param specializationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SpecializationDTO save(SpecializationDTO specializationDTO) {
        log.debug("Request to save Specialization : {}", specializationDTO);
        Specialization specialization = specializationMapper.toEntity(specializationDTO);
        specialization = specializationRepository.save(specialization);
        SpecializationDTO result = specializationMapper.toDto(specialization);
        specializationSearchRepository.save(specialization);
        return result;
    }

    /**
     * Get all the specializations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SpecializationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Specializations");
        return specializationRepository.findAll(pageable)
            .map(specializationMapper::toDto);
    }

    /**
     * Get one specialization by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SpecializationDTO findOne(Long id) {
        log.debug("Request to get Specialization : {}", id);
        Specialization specialization = specializationRepository.findOne(id);
        return specializationMapper.toDto(specialization);
    }

    /**
     * Delete the specialization by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Specialization : {}", id);
        specializationRepository.delete(id);
        specializationSearchRepository.delete(id);
    }

    /**
     * Search for the specialization corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SpecializationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Specializations for query {}", query);
        Page<Specialization> result = specializationSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(specializationMapper::toDto);
    }
}
