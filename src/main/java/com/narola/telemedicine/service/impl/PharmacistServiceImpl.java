package com.narola.telemedicine.service.impl;

import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.service.PharmacistService;
import com.narola.telemedicine.domain.Pharmacist;
import com.narola.telemedicine.repository.PharmacistRepository;
import com.narola.telemedicine.repository.search.PharmacistSearchRepository;
import com.narola.telemedicine.service.dto.PharmacistDTO;
import com.narola.telemedicine.service.mapper.PharmacistMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Pharmacist.
 */
@Service
@Transactional
public class PharmacistServiceImpl implements PharmacistService {

    private final Logger log = LoggerFactory.getLogger(PharmacistServiceImpl.class);

    private final PharmacistRepository pharmacistRepository;

    private final PharmacistMapper pharmacistMapper;

    private final PharmacistSearchRepository pharmacistSearchRepository;

    public PharmacistServiceImpl(PharmacistRepository pharmacistRepository, PharmacistMapper pharmacistMapper, PharmacistSearchRepository pharmacistSearchRepository) {
        this.pharmacistRepository = pharmacistRepository;
        this.pharmacistMapper = pharmacistMapper;
        this.pharmacistSearchRepository = pharmacistSearchRepository;
    }

    /**
     * Save a pharmacist.
     *
     * @param pharmacistDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PharmacistDTO save(PharmacistDTO pharmacistDTO) {
        log.debug("Request to save Pharmacist : {}", pharmacistDTO);
        User user = new User();
        Pharmacist pharmacist = pharmacistMapper.toEntity(pharmacistDTO);
        user.setId(pharmacistDTO.getUserId());
        pharmacist.setUser(user);
        pharmacist = pharmacistRepository.save(pharmacist);
        PharmacistDTO result = pharmacistMapper.toDto(pharmacist);
        pharmacistSearchRepository.save(pharmacist);
        return result;
    }

    /**
     * Get all the pharmacists.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PharmacistDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pharmacists");
        return pharmacistRepository.findAll(pageable)
            .map(pharmacistMapper::toDto);
    }


    /**
     *  get all the pharmacists where OrderMedicine is null.
     *  @return the list of entities
     */
   /* @Transactional(readOnly = true)
    public List<PharmacistDTO> findAllWhereOrderMedicineIsNull() {
        log.debug("Request to get all pharmacists where OrderMedicine is null");
        return StreamSupport
            .stream(pharmacistRepository.findAll().spliterator(), false)
            .filter(pharmacist -> pharmacist.getOrderMedicine() == null)
            .map(pharmacistMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }*/
    /**
     * Get one pharmacist by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PharmacistDTO findOne(Long id) {
        log.debug("Request to get Pharmacist : {}", id);
        Pharmacist pharmacist = pharmacistRepository.findOne(id);
        return pharmacistMapper.toDto(pharmacist);
    }

    //get pharmacist by userid
    @Override
    @Transactional(readOnly = true)
    public PharmacistDTO findByUserId(Long id) {
        log.debug("Request to get pharmacist : {}", id);
        Pharmacist pharmacist = pharmacistRepository.findByUserId(id);
        return pharmacistMapper.toDto(pharmacist);
    }

    /**
     * Delete the pharmacist by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pharmacist : {}", id);
        pharmacistRepository.delete(id);
        pharmacistSearchRepository.delete(id);
    }

    /**
     * Search for the pharmacist corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PharmacistDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Pharmacists for query {}", query);
        Page<Pharmacist> result = pharmacistSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(pharmacistMapper::toDto);
    }
}
