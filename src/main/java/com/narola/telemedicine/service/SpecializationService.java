package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.SpecializationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Specialization.
 */
public interface SpecializationService {

    /**
     * Save a specialization.
     *
     * @param specializationDTO the entity to save
     * @return the persisted entity
     */
    SpecializationDTO save(SpecializationDTO specializationDTO);

    /**
     * Get all the specializations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SpecializationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" specialization.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SpecializationDTO findOne(Long id);

    /**
     * Delete the "id" specialization.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the specialization corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SpecializationDTO> search(String query, Pageable pageable);
}
