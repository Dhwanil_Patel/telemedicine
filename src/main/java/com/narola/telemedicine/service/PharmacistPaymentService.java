package com.narola.telemedicine.service;

import com.narola.telemedicine.domain.PharmacistPayment;
import com.narola.telemedicine.service.dto.PharmacistPaymentDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing PharmacistPayment.
 */
public interface PharmacistPaymentService {

    /**
     * Save a pharmacistPayment.
     *
     * @param pharmacistPaymentDTO the entity to save
     * @return the persisted entity
     */
    PharmacistPaymentDTO save(PharmacistPaymentDTO pharmacistPaymentDTO);

    /**
     * Get all the pharmacistPayments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PharmacistPaymentDTO> findAll(Pageable pageable);

    // load pharmacist_payment by pharmacist id
    List<PharmacistPaymentDTO> findByPharmacistId(Long id);

    // load pharmacist_payment by patient id
    List<PharmacistPaymentDTO> findByPatientId(Long id);

    /**
     * Get the "id" pharmacistPayment.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PharmacistPaymentDTO findOne(Long id);

    /**
     * Delete the "id" pharmacistPayment.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the pharmacistPayment corresponding to the query.
     *
     * @param query the query of the search
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PharmacistPaymentDTO> search(String query, Pageable pageable);
}
