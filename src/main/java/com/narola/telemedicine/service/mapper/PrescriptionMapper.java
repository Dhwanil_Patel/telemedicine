package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.PrescriptionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Prescription and its DTO PrescriptionDTO.
 */
@Mapper(componentModel = "spring", uses = {AppointmentMapper.class})
public interface PrescriptionMapper extends EntityMapper<PrescriptionDTO, Prescription> {

    @Mapping(source = "appointment.id", target = "appointmentId")
    PrescriptionDTO toDto(Prescription prescription);

    @Mapping(source = "appointmentId", target = "appointment")
    @Mapping(target = "orderMedicine", ignore = true)
    Prescription toEntity(PrescriptionDTO prescriptionDTO);

    default Prescription fromId(Long id) {
        if (id == null) {
            return null;
        }
        Prescription prescription = new Prescription();
        prescription.setId(id);
        return prescription;
    }
}
