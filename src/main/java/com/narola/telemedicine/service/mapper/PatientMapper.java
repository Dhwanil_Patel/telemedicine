package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.PatientDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Patient and its DTO PatientDTO.
 */

//added User Mapper class
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface PatientMapper extends EntityMapper<PatientDTO, Patient> {

    //    added
    @Mapping(source = "user.id", target = "userId")
    //    added (doubtful)
    @Mapping(source = "userId", target = "user")

    default Patient fromId(Long id) {
        if (id == null) {
            return null;
        }
        Patient patient = new Patient();
        patient.setId(id);
        return patient;
    }
}
