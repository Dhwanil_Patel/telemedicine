package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.PharmacistDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Pharmacist and its DTO PharmacistDTO.
 */

//added User Mapper class
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface PharmacistMapper extends EntityMapper<PharmacistDTO, Pharmacist> {

    //    added
    //@Mapping(source = "user.id", target = "userId")
    //    added (doubtful)
    @Mapping(source = "userId", target = "user")

    // @Mapping(target = "orderMedicine", ignore = true)
    Pharmacist toEntity(PharmacistDTO pharmacistDTO);

    default Pharmacist fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pharmacist pharmacist = new Pharmacist();
        pharmacist.setId(id);
        return pharmacist;
    }
}
