package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.DoctorScheduleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DoctorSchedule and its DTO DoctorScheduleDTO.
 */
@Mapper(componentModel = "spring", uses = {DoctorMapper.class})
public interface DoctorScheduleMapper extends EntityMapper<DoctorScheduleDTO, DoctorSchedule> {

    @Mapping(source = "doctor.id", target = "doctorId")
    DoctorScheduleDTO toDto(DoctorSchedule doctorSchedule);

    @Mapping(source = "doctorId", target = "doctor")
    DoctorSchedule toEntity(DoctorScheduleDTO doctorScheduleDTO);

    default DoctorSchedule fromId(Long id) {
        if (id == null) {
            return null;
        }
        DoctorSchedule doctorSchedule = new DoctorSchedule();
        doctorSchedule.setId(id);
        return doctorSchedule;
    }
}
