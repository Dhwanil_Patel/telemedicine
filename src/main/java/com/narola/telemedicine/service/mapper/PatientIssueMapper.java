package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.PatientIssueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PatientIssue and its DTO PatientIssueDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PatientIssueMapper extends EntityMapper<PatientIssueDTO, PatientIssue> {



    default PatientIssue fromId(Long id) {
        if (id == null) {
            return null;
        }
        PatientIssue PatientIssue = new PatientIssue();
        PatientIssue.setId(id);
        return PatientIssue;
    }
}
