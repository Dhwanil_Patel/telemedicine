package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.PharmacistPaymentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PharmacistPayment and its DTO PharmacistPaymentDTO.
 */
@Mapper(componentModel = "spring", uses = {OrderMedicineMapper.class})
public interface PharmacistPaymentMapper extends EntityMapper<PharmacistPaymentDTO, PharmacistPayment> {

    @Mapping(source = "orderMedicine.id", target = "orderMedicineId")
    PharmacistPaymentDTO toDto(PharmacistPayment pharmacistPayment);

    @Mapping(source = "orderMedicineId", target = "orderMedicine")
    PharmacistPayment toEntity(PharmacistPaymentDTO pharmacistPaymentDTO);

    default PharmacistPayment fromId(Long id) {
        if (id == null) {
            return null;
        }
        PharmacistPayment pharmacistPayment = new PharmacistPayment();
        pharmacistPayment.setId(id);
        return pharmacistPayment;
    }
}
