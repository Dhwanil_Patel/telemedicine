package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.SpecializationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Specialization and its DTO SpecializationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SpecializationMapper extends EntityMapper<SpecializationDTO, Specialization> {



    default Specialization fromId(Long id) {
        if (id == null) {
            return null;
        }
        Specialization specialization = new Specialization();
        specialization.setId(id);
        return specialization;
    }
}
