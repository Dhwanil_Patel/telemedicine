package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.OrderMedicineDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity OrderMedicine and its DTO OrderMedicineDTO.
 */
@Mapper(componentModel = "spring", uses = {PrescriptionMapper.class, PharmacistMapper.class})
public interface OrderMedicineMapper extends EntityMapper<OrderMedicineDTO, OrderMedicine> {

    @Mapping(source = "prescription.id", target = "prescriptionId")
    @Mapping(source = "pharmacist.id", target = "pharmacistId")
    OrderMedicineDTO toDto(OrderMedicine orderMedicine);

    @Mapping(source = "prescriptionId", target = "prescription")
    @Mapping(source = "pharmacistId", target = "pharmacist")
    @Mapping(target = "pharmacistPayment", ignore = true)
    OrderMedicine toEntity(OrderMedicineDTO orderMedicineDTO);

    default OrderMedicine fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderMedicine orderMedicine = new OrderMedicine();
        orderMedicine.setId(id);
        return orderMedicine;
    }
}
