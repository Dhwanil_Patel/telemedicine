package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.DoctorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Doctor and its DTO DoctorDTO.
 */

@Mapper(componentModel = "spring", uses = {DegreeMapper.class, SpecializationMapper.class, UserMapper.class})
//added User Mapper class
public interface DoctorMapper extends EntityMapper<DoctorDTO, Doctor> {

    @Mapping(source = "degree.id", target = "degreeId")
    @Mapping(source = "specialization.id", target = "specializationId")
    //    added
    @Mapping(source = "user.id", target = "userId")
    DoctorDTO toDto(Doctor doctor);

    @Mapping(source = "degreeId", target = "degree")
    @Mapping(source = "specializationId", target = "specialization")

//    added (doubtful)
    @Mapping(source = "userId", target = "user")
    Doctor toEntity(DoctorDTO doctorDTO);

    default Doctor fromId(Long id) {
        if (id == null) {
            return null;
        }
        Doctor doctor = new Doctor();
        doctor.setId(id);
        return doctor;
    }
    default Doctor fromFirstName (String firstname) {
        if (firstname == null) {
            return null;
        }
        Doctor doctor = new Doctor();
        doctor.setFirstname(firstname);
        return doctor;
    }
}
