package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.DegreeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Degree and its DTO DegreeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DegreeMapper extends EntityMapper<DegreeDTO, Degree> {



    default Degree fromId(Long id) {
        if (id == null) {
            return null;
        }
        Degree degree = new Degree();
        degree.setId(id);
        return degree;
    }
}
