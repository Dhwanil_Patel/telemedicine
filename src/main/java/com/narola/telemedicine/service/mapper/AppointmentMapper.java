package com.narola.telemedicine.service.mapper;

import com.narola.telemedicine.domain.*;
import com.narola.telemedicine.service.dto.AppointmentDTO;

import org.elasticsearch.index.mapper.SourceToParse;
import org.mapstruct.*;

/**
 * Mapper for the entity Appointment and its DTO AppointmentDTO.
 */
@Mapper(componentModel = "spring", uses = {DoctorMapper.class, PatientMapper.class, SpecializationMapper.class, PatientIssueMapper.class, DoctorScheduleMapper.class})
public interface AppointmentMapper extends EntityMapper<AppointmentDTO, Appointment> {

    @Mapping(source = "doctor.id", target = "doctorId")
    // add doctor name
//    @Mapping(source = "doctor.name", target = "doctorName")
    @Mapping(source = "patient.id", target = "patientId")
    @Mapping(source = "specialization.id", target = "specializationId")
    @Mapping(source = "patientissue.id", target = "patientissueId")
    @Mapping(source = "doctorschedule.id", target = "doctorscheduleId")
    AppointmentDTO toDto(Appointment appointment);

    @Mapping(source = "doctorId", target = "doctor")
//    @Mapping(source = "doctorName", target = "doctor")
    @Mapping(source = "patientId", target = "patient")
    @Mapping(source = "specializationId", target = "specialization")
    @Mapping(source = "patientissueId", target = "patientissue")
    @Mapping(source = "doctorscheduleId", target = "doctorschedule")
    @Mapping(target = "payment", ignore = true)
    @Mapping(target = "prescription", ignore = true)
    Appointment toEntity(AppointmentDTO appointmentDTO);

    default Appointment fromId(Long id) {
        if (id == null) {
            return null;
        }
        Appointment appointment = new Appointment();
        appointment.setId(id);
        return appointment;
    }
}
