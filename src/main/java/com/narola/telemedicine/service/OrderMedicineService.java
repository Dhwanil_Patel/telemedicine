package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.OrderMedicineDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing OrderMedicine.
 */
public interface OrderMedicineService {

    /**
     * Save a orderMedicine.
     *
     * @param orderMedicineDTO the entity to save
     * @return the persisted entity
     */
    OrderMedicineDTO save(OrderMedicineDTO orderMedicineDTO);

    /**
     * Get all the orderMedicines.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OrderMedicineDTO> findAll(Pageable pageable);
    /**
     * Get all the OrderMedicineDTO where PharmacistPayment is null.
     *
     * @return the list of entities
     */
    List<OrderMedicineDTO> findAllWherePharmacistPaymentIsNull();

    //find OrderMedicine By PatientId Where PharmacistPayment Is Null
    List<OrderMedicineDTO> findOrderMedicineByPatientIdWherePharmacistPaymentIsNull(Long id);
    //get ordermedicine by patient id
    List<OrderMedicineDTO> findOrderMedicineByPatientId(Long id);

    //get ordermedicine by pharmacist id
    List<OrderMedicineDTO> findOrderMedicineByPharmacistId(Long id);

    /**
     * Get the "id" orderMedicine.
     *
     * @param id the id of the entity
     * @return the entity
     */
    OrderMedicineDTO findOne(Long id);

    /**
     * Delete the "id" orderMedicine.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the orderMedicine corresponding to the query.
     *
     * @param query the query of the search
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OrderMedicineDTO> search(String query, Pageable pageable);
}
