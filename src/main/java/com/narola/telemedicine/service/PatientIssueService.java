package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.PatientIssueDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PatientIssue.
 */
public interface PatientIssueService {

    /**
     * Save a PatientIssue.
     *
     * @param PatientIssueDTO the entity to save
     * @return the persisted entity
     */
    PatientIssueDTO save(PatientIssueDTO PatientIssueDTO);

    /**
     * Get all the PatientIssue.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PatientIssueDTO> findAll(Pageable pageable);

    /**
     * Get the "id" PatientIssue.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PatientIssueDTO findOne(Long id);

    /**
     * Delete the "id" PatientIssue.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the PatientIssue corresponding to the query.
     *
     * @param query the query of the search
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PatientIssueDTO> search(String query, Pageable pageable);
}
