package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.AppointmentDTO;
import com.narola.telemedicine.service.dto.DoctorScheduleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing DoctorSchedule.
 */
public interface DoctorScheduleService {

    /**
     * Save a doctorSchedule.
     *
     * @param doctorScheduleDTO the entity to save
     * @return the persisted entity
     */
    DoctorScheduleDTO save(DoctorScheduleDTO doctorScheduleDTO);

    /**
     * Get all the doctorSchedules.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DoctorScheduleDTO> findAll(Pageable pageable);

    /**
     * Get the "id" doctorSchedule.
     *
     * @param id the id of the entity
     * @return the entity
     */
    DoctorScheduleDTO findOne(Long id);

    //getappointment by doctor
    Page<DoctorScheduleDTO> findByDoctorId(Long id, Pageable pageable);

    /**
     * Delete the "id" doctorSchedule.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the doctorSchedule corresponding to the query.
     *
     * @param query the query of the search
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DoctorScheduleDTO> search(String query, Pageable pageable);
}
