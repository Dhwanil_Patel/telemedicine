package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.PatientDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by pdhwanil on 01-06-2018.
 */
public interface ReportService {

    Page<PatientDTO> findAllPatient(Pageable pageable);
}
