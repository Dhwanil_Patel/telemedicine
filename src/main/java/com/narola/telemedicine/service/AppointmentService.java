package com.narola.telemedicine.service;

import com.narola.telemedicine.domain.Appointment;
import com.narola.telemedicine.service.dto.AppointmentDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Appointment.
 */
public interface AppointmentService {

    /**
     * Save a appointment.
     *
     * @param appointmentDTO the entity to save
     * @return the persisted entity
     */
    AppointmentDTO save(AppointmentDTO appointmentDTO);

    /**
     * Get all the appointments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AppointmentDTO> findAll(Pageable pageable);
    /**
     * Get all the AppointmentDTO where Payment is null.
     *
     * @return the list of entities
     */

    // for doctor name
//    List<Appointment> finddoctor(Long id);

    List<AppointmentDTO> findAllWherePaymentIsNull();
    /**
     * Get all the AppointmentDTO where Prescription is null.
     *
     * @return the list of entities
     */
    List<AppointmentDTO> findAllWherePrescriptionIsNull();

    // appointmentDto where payment is null by patinet id
    List<AppointmentDTO> findPatientIdWherePaymentIsNull(Long id);

    // appointmentDto where prescription is null by doctor id
    List<AppointmentDTO> findDoctorIdWherePrescriptionIsNull(Long id);
    /**
     * Get the "id" appointment.
     *
     * @param id the id of the entity
     * @return the entity
     */
    AppointmentDTO findOne(Long id);

    //getappointment by doctor
    Page<AppointmentDTO> findByDoctorId(Long id,Pageable pageable);

//    List<AppointmentDTO> findAppointmentWithDetails(Long id);

    //getappointment by patient
    Page<AppointmentDTO> findByPatientId(Long id,Pageable pageable);


    /**
     * Delete the "id" appointment.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the appointment corresponding to the query.
     *
     * @param query the query of the search
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AppointmentDTO> search(String query, Pageable pageable);
}

//package com.narola.telemedicine.service;
//
//import com.narola.telemedicine.service.dto.AppointmentDTO;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import java.util.List;
//
///**
// * Service Interface for managing Appointment.
// */
//public interface AppointmentService {
//
//    /**
//     * Save a appointment.
//     *
//     * @param appointmentDTO the entity to save
//     * @return the persisted entity
//     */
//    AppointmentDTO save(AppointmentDTO appointmentDTO);
//
//    /**
//     * Get all the appointments.
//     *
//     * @param pageable the pagination information
//     * @return the list of entities
//     */
//    Page<AppointmentDTO> findAll(Pageable pageable);
//    /**
//     * Get all the AppointmentDTO where Payment is null.
//     *
//     * @return the list of entities
//     */
//    List<AppointmentDTO> findAllWherePaymentIsNull();
//    /**
//     * Get all the AppointmentDTO where Prescription is null.
//     *
//     * @return the list of entities
//     */
//    List<AppointmentDTO> findAllWherePrescriptionIsNull();
//
//    /**
//     * Get the "id" appointment.
//     *
//     * @param id the id of the entity
//     * @return the entity
//     */
//    AppointmentDTO findOne(Long id);
//
//    /**
//     * Delete the "id" appointment.
//     *
//     * @param id the id of the entity
//     */
//    void delete(Long id);
//
//    /**
//     * Search for the appointment corresponding to the query.
//     *
//     * @param query the query of the search
//     *
//     * @param pageable the pagination information
//     * @return the list of entities
//     */
//    Page<AppointmentDTO> search(String query, Pageable pageable);
//}
