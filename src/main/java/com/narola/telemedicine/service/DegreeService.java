package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.DegreeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Degree.
 */
public interface DegreeService {

    /**
     * Save a degree.
     *
     * @param degreeDTO the entity to save
     * @return the persisted entity
     */
    DegreeDTO save(DegreeDTO degreeDTO);

    /**
     * Get all the degrees.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DegreeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" degree.
     *
     * @param id the id of the entity
     * @return the entity
     */
    DegreeDTO findOne(Long id);

    /**
     * Delete the "id" degree.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the degree corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DegreeDTO> search(String query, Pageable pageable);
}
