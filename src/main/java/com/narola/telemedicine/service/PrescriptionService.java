package com.narola.telemedicine.service;

import com.narola.telemedicine.service.dto.PrescriptionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Prescription.
 */
public interface PrescriptionService {

    /**
     * Save a prescription.
     *
     * @param prescriptionDTO the entity to save
     * @return the persisted entity
     */
    PrescriptionDTO save(PrescriptionDTO prescriptionDTO);

    /**
     * Get all the prescriptions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PrescriptionDTO> findAll(Pageable pageable);

    // find by doctor id
    List<PrescriptionDTO> findPrescriptionByDoctorId(Long id);

    //find by patient id
    List<PrescriptionDTO> findPrescriptionByPatientId(Long id);
    /**
     * Get all the PrescriptionDTO where OrderMedicine is null.
     *
     * @return the list of entities
     */
    List<PrescriptionDTO> findAllWhereOrderMedicineIsNull();

    // findPrescriptionBy PatientId WhereOrderMedicineIsNull
    List<PrescriptionDTO> findPrescriptionByPatientIdWhereOrderMedicineIsNull(Long id);

    /**
     * Get the "id" prescription.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PrescriptionDTO findOne(Long id);

    /**
     * Delete the "id" prescription.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the prescription corresponding to the query.
     *
     * @param query the query of the search
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PrescriptionDTO> search(String query, Pageable pageable);
}
