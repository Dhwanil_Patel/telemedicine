package com.narola.telemedicine.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.narola.telemedicine.domain.enumeration.Payment_Status;

/**
 * A DTO for the PharmacistPayment entity.
 */
public class PharmacistPaymentDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate paymentDate;

    @NotNull
    private Integer totalamount;

    @NotNull
    private String paymentmode;

    @NotNull
    private Payment_Status paymentStatus;

    @NotNull
    private String transcationid;

    private Long orderMedicineId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Integer getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Integer totalamount) {
        this.totalamount = totalamount;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public Payment_Status getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Payment_Status paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTranscationid() {
        return transcationid;
    }

    public void setTranscationid(String transcationid) {
        this.transcationid = transcationid;
    }

    public Long getOrderMedicineId() {
        return orderMedicineId;
    }

    public void setOrderMedicineId(Long orderMedicineId) {
        this.orderMedicineId = orderMedicineId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PharmacistPaymentDTO pharmacistPaymentDTO = (PharmacistPaymentDTO) o;
        if(pharmacistPaymentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pharmacistPaymentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PharmacistPaymentDTO{" +
            "id=" + getId() +
            ", paymentDate='" + getPaymentDate() + "'" +
            ", totalamount=" + getTotalamount() +
            ", paymentmode='" + getPaymentmode() + "'" +
            ", paymentStatus='" + getPaymentStatus() + "'" +
            ", transcationid='" + getTranscationid() + "'" +
            "}";
    }
}
