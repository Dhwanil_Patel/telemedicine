package com.narola.telemedicine.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Degree entity.
 */
public class DegreeDTO implements Serializable {

    private Long id;

    @NotNull
    private String degreename;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDegreename() {
        return degreename;
    }

    public void setDegreename(String degreename) {
        this.degreename = degreename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DegreeDTO degreeDTO = (DegreeDTO) o;
        if(degreeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), degreeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DegreeDTO{" +
            "id=" + getId() +
            ", degreename='" + getDegreename() + "'" +
            "}";
    }
}
