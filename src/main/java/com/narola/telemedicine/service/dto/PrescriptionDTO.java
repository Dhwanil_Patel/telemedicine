package com.narola.telemedicine.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Prescription entity.
 */
public class PrescriptionDTO implements Serializable {

    private Long id;

    @NotNull
    private String prescriptiondetail;

    private Long appointmentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrescriptiondetail() {
        return prescriptiondetail;
    }

    public void setPrescriptiondetail(String prescriptiondetail) {
        this.prescriptiondetail = prescriptiondetail;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PrescriptionDTO prescriptionDTO = (PrescriptionDTO) o;
        if(prescriptionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), prescriptionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PrescriptionDTO{" +
            "id=" + getId() +
            ", prescriptiondetail='" + getPrescriptiondetail() + "'" +
            "}";
    }
}
