package com.narola.telemedicine.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;
import com.narola.telemedicine.domain.enumeration.Process_Status;

/**
 * A DTO for the Pharmacist entity.
 */
public class PharmacistDTO implements Serializable {

    private Long id;

    @NotNull
    private String shopname;

    //    added userId
    private Long userId;

    private Process_Status status;

    @NotNull
    private String shopaddress;

    @NotNull
    private String phoneno;

    @NotNull
    private String licenceno;

    @NotNull
    @Lob
    private byte[] documents;
    private String documentsContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    //    getter setter methods fro userId
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Process_Status getStatus() {
        return status;
    }

    public void setStatus(Process_Status status) {
        this.status = status;
    }

    public String getShopaddress() {
        return shopaddress;
    }

    public void setShopaddress(String shopaddress) {
        this.shopaddress = shopaddress;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getLicenceno() {
        return licenceno;
    }

    public void setLicenceno(String licenceno) {
        this.licenceno = licenceno;
    }

    public byte[] getDocuments() {
        return documents;
    }

    public void setDocuments(byte[] documents) {
        this.documents = documents;
    }

    public String getDocumentsContentType() {
        return documentsContentType;
    }

    public void setDocumentsContentType(String documentsContentType) {
        this.documentsContentType = documentsContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PharmacistDTO pharmacistDTO = (PharmacistDTO) o;
        if(pharmacistDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pharmacistDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PharmacistDTO{" +
            "id=" + getId() +
            ", shopname='" + getShopname() + "'" +
            ", status='" + getStatus() + "'" +
            ", shopaddress='" + getShopaddress() + "'" +
            ", phoneno='" + getPhoneno() + "'" +
            ", licenceno='" + getLicenceno() + "'" +
            ", documents='" + getDocuments() + "'" +
            "}";
    }
}
