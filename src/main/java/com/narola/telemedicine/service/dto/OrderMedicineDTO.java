package com.narola.telemedicine.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the OrderMedicine entity.
 */
public class OrderMedicineDTO implements Serializable {

    private Long id;

    @NotNull
    private String medicinedetail;

    @NotNull
    private Integer totalprice;

    @NotNull
    private String status;

    private Long prescriptionId;

    private Long pharmacistId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMedicinedetail() {
        return medicinedetail;
    }

    public void setMedicinedetail(String medicinedetail) {
        this.medicinedetail = medicinedetail;
    }

    public Integer getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Integer totalprice) {
        this.totalprice = totalprice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
      public Long getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(Long prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public Long getPharmacistId() {
        return pharmacistId;
    }

    public void setPharmacistId(Long pharmacistId) {
        this.pharmacistId = pharmacistId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderMedicineDTO orderMedicineDTO = (OrderMedicineDTO) o;
        if(orderMedicineDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderMedicineDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderMedicineDTO{" +
            "id=" + getId() +
            ", medicinedetail='" + getMedicinedetail() + "'" +
            ", totalprice=" + getTotalprice() +
            ", status=" + getStatus() +
            "}";
    }
}
