package com.narola.telemedicine.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.narola.telemedicine.domain.enumeration.Days;
import com.narola.telemedicine.domain.enumeration.Schedule_Status;

/**
 * A DTO for the DoctorSchedule entity.
 */
public class DoctorScheduleDTO implements Serializable {

    private Long id;

    @NotNull
    private Days workingday;

    @NotNull
    private String fromtime;

    @NotNull
    private String totime;

    @NotNull
    private Integer noofpatient;

    @NotNull
    private Schedule_Status status;

    private Long doctorId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Days getWorkingday() {
        return workingday;
    }

    public void setWorkingday(Days workingday) {
        this.workingday = workingday;
    }

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    public Integer getNoofpatient() {
        return noofpatient;
    }

    public void setNoofpatient(Integer noofpatient) {
        this.noofpatient = noofpatient;
    }

    public Schedule_Status getStatus() {
        return status;
    }

    public void setStatus(Schedule_Status status) {
        this.status = status;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoctorScheduleDTO doctorScheduleDTO = (DoctorScheduleDTO) o;
        if(doctorScheduleDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), doctorScheduleDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DoctorScheduleDTO{" +
            "id=" + getId() +
            ", workingday='" + getWorkingday() + "'" +
            ", fromtime='" + getFromtime() + "'" +
            ", totime='" + getTotime() + "'" +
            ", noofpatient=" + getNoofpatient() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
