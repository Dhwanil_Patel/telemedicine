package com.narola.telemedicine.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Specialization entity.
 */
public class SpecializationDTO implements Serializable {

    private Long id;

    @NotNull
    private String expertise;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpecializationDTO specializationDTO = (SpecializationDTO) o;
        if(specializationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), specializationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpecializationDTO{" +
            "id=" + getId() +
            ", expertise='" + getExpertise() + "'" +
            "}";
    }
}
