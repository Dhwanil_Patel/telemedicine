package com.narola.telemedicine.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PatientIssue entity.
 */
public class PatientIssueDTO implements Serializable {

    private Long id;

    @NotNull
    private String issue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PatientIssueDTO PatientIssueDTO = (PatientIssueDTO) o;
        if(PatientIssueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), PatientIssueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PatientIssueDTO{" +
            "id=" + getId() +
            ", issue='" + getIssue() + "'" +
            "}";
    }
}
