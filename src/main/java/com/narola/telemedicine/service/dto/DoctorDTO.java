package com.narola.telemedicine.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;
import com.narola.telemedicine.domain.enumeration.Process_Status;
import com.narola.telemedicine.domain.enumeration.Gender;

/**
 * A DTO for the Doctor entity.
 */
public class DoctorDTO implements Serializable {

    private Long id;

    @NotNull
    private String firstname;

    @NotNull
    private String lastname;

//    added userId
    private Long userId;

    private Process_Status status;

    @Lob
    private byte[] photo;
    private String photoContentType;

    @NotNull
    private Gender gender;

    private LocalDate dateofbirth;

    @NotNull
    private String address;

    @NotNull
    private String mobileno;

    private String bloodgroup;

    @NotNull
    private Float exprience;

    @NotNull
    private String licenceno;

    @NotNull
    @Lob
    private byte[] documents;
    private String documentsContentType;

    private String googleplusid;

    private String facebookid;

    private String twitterid;

    @NotNull
    private Integer consultationcharge;

    //added skype,city and state
   @NotNull
    private String skype;

    @NotNull
    private String city;

    @NotNull
    private String state;

    @NotNull
    private String country;

    private Long degreeId;

    private Long specializationId;

      public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

//    getter setter methods fro userId
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Process_Status getStatus() {
        return status;
    }

    public void setStatus(Process_Status status) {
        this.status = status;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(LocalDate dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public Float getExprience() {
        return exprience;
    }

    public void setExprience(Float exprience) {
        this.exprience = exprience;
    }

    public String getLicenceno() {
        return licenceno;
    }

    public void setLicenceno(String licenceno) {
        this.licenceno = licenceno;
    }

    public byte[] getDocuments() {
        return documents;
    }

    public void setDocuments(byte[] documents) {
        this.documents = documents;
    }

    public String getDocumentsContentType() {
        return documentsContentType;
    }

    public void setDocumentsContentType(String documentsContentType) {
        this.documentsContentType = documentsContentType;
    }

    public String getGoogleplusid() {
        return googleplusid;
    }

    public void setGoogleplusid(String googleplusid) {
        this.googleplusid = googleplusid;
    }

    public String getFacebookid() {
        return facebookid;
    }

    public void setFacebookid(String facebookid) {
        this.facebookid = facebookid;
    }

    public String getTwitterid() {
        return twitterid;
    }

    public void setTwitterid(String twitterid) {
        this.twitterid = twitterid;
    }

    public Integer getConsultationcharge() {
        return consultationcharge;
    }

    public void setConsultationcharge(Integer consultationcharge) {
        this.consultationcharge = consultationcharge;
    }

    //added getter setter for three fields
    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    public Long getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(Long degreeId) {
        this.degreeId = degreeId;
    }

    public Long getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(Long specializationId) {
        this.specializationId = specializationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoctorDTO doctorDTO = (DoctorDTO) o;
        if(doctorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), doctorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DoctorDTO{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", lastname='" + getLastname() + "'" +
            ", userId='" + getUserId() + "'" +
            ", status='" + getStatus() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", gender='" + getGender() + "'" +
            ", dateofbirth='" + getDateofbirth() + "'" +
            ", address='" + getAddress() + "'" +
            ", mobileno='" + getMobileno() + "'" +
            ", bloodgroup='" + getBloodgroup() + "'" +
            ", exprience=" + getExprience() +
            ", licenceno='" + getLicenceno() + "'" +
            ", documents='" + getDocuments() + "'" +
            ", googleplusid='" + getGoogleplusid() + "'" +
            ", facebookid='" + getFacebookid() + "'" +
            ", twitterid='" + getTwitterid() + "'" +
            ", consultationcharge=" + getConsultationcharge() +
            ", skype=" + getSkype() +
            ", city=" + getCity() +
            ", state=" + getState() +
            ", country=" + getCountry() +
            "}";
    }
}
