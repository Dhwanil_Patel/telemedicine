package com.narola.telemedicine.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.narola.telemedicine.domain.enumeration.Appointment_Status;

/**
 * A DTO for the Appointment entity.
 */
public class AppointmentDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate appointmentdate;

//    @NotNull
//    private String appointmenttime;

    @NotNull
    private Appointment_Status appointmentstatus;

    private Long doctorId;

    // doctor name
    // private String doctorName;
    private String doctorName;

    private Long patientId;

    private Long specializationId;

    private Long patientissueId;

    private Long doctorscheduleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getAppointmentdate() {
        return appointmentdate;
    }

    public void setAppointmentdate(LocalDate appointmentdate) {
        this.appointmentdate = appointmentdate;
    }

//    public String getAppointmenttime() {
//        return appointmenttime;
//    }
////
//    public void setAppointmenttime(String appointmenttime) {
//        this.appointmenttime = appointmenttime;
//    }

    public Appointment_Status getAppointmentstatus() {
        return appointmentstatus;
    }

    public void setAppointmentstatus(Appointment_Status appointmentstatus) {
        this.appointmentstatus = appointmentstatus;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

//    public String getDoctorName() {
    public String getDoctorName() {
//        return doctorName;
        return doctorName;
    }
//
//    public void setDoctorName(String doctorName) {
//        this.doctorName = doctorName;
//    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Long getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(Long specializationId) {
        this.specializationId = specializationId;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Long getPatientissueId() {
        return patientissueId;
    }

    public void setPatientissueId(Long patientIssuesId) {
        this.patientissueId = patientIssuesId;
    }

    public Long getDoctorscheduleId() {
        return doctorscheduleId;
    }

    public void setDoctorscheduleId(Long doctorscheduleId) {
        this.doctorscheduleId = doctorscheduleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AppointmentDTO appointmentDTO = (AppointmentDTO) o;
        if(appointmentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), appointmentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AppointmentDTO{" +
            "id=" + getId() +
            ", appointmentdate='" + getAppointmentdate() + "'" +
            ", appointmentstatus='" + getAppointmentstatus() + "'" +
            "}";
    }
}
