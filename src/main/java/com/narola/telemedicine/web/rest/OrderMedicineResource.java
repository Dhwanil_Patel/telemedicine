package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.domain.Patient;
import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.repository.PatientRepository;
import com.narola.telemedicine.repository.UserRepository;
import com.narola.telemedicine.security.SecurityUtils;
import com.narola.telemedicine.service.OrderMedicineService;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.errors.InternalServerErrorException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.OrderMedicineDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing OrderMedicine.
 */
@RestController
@RequestMapping("/api")
public class OrderMedicineResource {

    private final Logger log = LoggerFactory.getLogger(OrderMedicineResource.class);

    private static final String ENTITY_NAME = "orderMedicine";

    private final OrderMedicineService orderMedicineService;

    @Autowired
    private PatientRepository patientRepository;
    Patient patient = new Patient();

    @Autowired
    private UserRepository userRepository;
    User user= new User();

    public OrderMedicineResource(OrderMedicineService orderMedicineService) {
        this.orderMedicineService = orderMedicineService;
    }

    /**
     * POST  /order-medicines : Create a new orderMedicine.
     *
     * @param orderMedicineDTO the orderMedicineDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderMedicineDTO, or with status 400 (Bad Request) if the orderMedicine has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-medicines")
    @Timed
    public ResponseEntity<OrderMedicineDTO> createOrderMedicine(@Valid @RequestBody OrderMedicineDTO orderMedicineDTO) throws URISyntaxException {
        log.debug("REST request to save OrderMedicine : {}", orderMedicineDTO);
        if (orderMedicineDTO.getId() != null) {
            throw new BadRequestAlertException("A new orderMedicine cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderMedicineDTO result = orderMedicineService.save(orderMedicineDTO);
        return ResponseEntity.created(new URI("/api/order-medicines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /order-medicines : Updates an existing orderMedicine.
     *
     * @param orderMedicineDTO the orderMedicineDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderMedicineDTO,
     * or with status 400 (Bad Request) if the orderMedicineDTO is not valid,
     * or with status 500 (Internal Server Error) if the orderMedicineDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/order-medicines")
    @Timed
    public ResponseEntity<OrderMedicineDTO> updateOrderMedicine(@Valid @RequestBody OrderMedicineDTO orderMedicineDTO) throws URISyntaxException {
        log.debug("REST request to update OrderMedicine : {}", orderMedicineDTO);
        if (orderMedicineDTO.getId() == null) {
            return createOrderMedicine(orderMedicineDTO);
        }
        OrderMedicineDTO result = orderMedicineService.save(orderMedicineDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, orderMedicineDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /order-medicines : get all the orderMedicines.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of orderMedicines in body
     */
    @GetMapping("/order-medicines")
    @Timed
    public ResponseEntity<List<OrderMedicineDTO>> getAllOrderMedicines(Pageable pageable, @RequestParam(required = false) String filter) {
        if ("pharmacistpayment-is-null".equals(filter)) {
            log.debug("REST request to get all OrderMedicines where pharmacistPayment is null");
            return new ResponseEntity<>(orderMedicineService.findAllWherePharmacistPaymentIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of OrderMedicines");
        Page<OrderMedicineDTO> page = orderMedicineService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-medicines");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /order-medicines/:id : get the "id" orderMedicine.
     *
     * @param id the id of the orderMedicineDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderMedicineDTO, or with status 404 (Not Found)
     */
    @GetMapping("/order-medicines/{id}")
    @Timed
    public ResponseEntity<OrderMedicineDTO> getOrderMedicine(@PathVariable Long id) {
        log.debug("REST request to get OrderMedicine : {}", id);
        OrderMedicineDTO orderMedicineDTO = orderMedicineService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(orderMedicineDTO));
    }

    // get ordermedicine by patient id
    @GetMapping("/order-medicines/patient")
    @Timed
    public ResponseEntity<List<OrderMedicineDTO>> getOrderMedicineByPatientId(String filter) {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        user = userRepository.findByLogin(userLogin);
        patient = patientRepository.findByUserId(user.getId());
        log.debug("REST request to get order-medicines : {}", patient.getId());
        if ("pharmacistpayment-is-null".equals(filter)) {
            log.debug("REST request to get all OrderMedicines where pharmacistPayment is null");
            return new ResponseEntity<>(orderMedicineService.findOrderMedicineByPatientIdWherePharmacistPaymentIsNull(patient.getId()),
                HttpStatus.OK);
        }
        List<OrderMedicineDTO> list = orderMedicineService.findOrderMedicineByPatientId(patient.getId());
        HttpHeaders headers = new HttpHeaders();
        headers.add("patient-ordermedicine", "api/order-medicines/patient");
        return new ResponseEntity<List<OrderMedicineDTO>>(list,headers,HttpStatus.OK);
    }

    // get ordermedicine by pharmacist id
    @GetMapping("/order-medicines/pharmacist/{id}")
    @Timed
    public ResponseEntity<List<OrderMedicineDTO>> getOrderMedicineByPharmacistId(@PathVariable Long id) {
        log.debug("REST request to get OrderMedicine : {}", id);
        List<OrderMedicineDTO> list = orderMedicineService.findOrderMedicineByPharmacistId(id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("pharmacist-ordermedicine","api/order-medicine/pharmacist");
        return new ResponseEntity<List<OrderMedicineDTO>>(list,headers,HttpStatus.OK);
    }
    /**
     * DELETE  /order-medicines/:id : delete the "id" orderMedicine.
     *
     * @param id the id of the orderMedicineDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-medicines/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderMedicine(@PathVariable Long id) {
        log.debug("REST request to delete OrderMedicine : {}", id);
        orderMedicineService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/order-medicines?query=:query : search for the orderMedicine corresponding
     * to the query.
     *
     * @param query the query of the orderMedicine search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/order-medicines")
    @Timed
    public ResponseEntity<List<OrderMedicineDTO>> searchOrderMedicines(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrderMedicines for query {}", query);
        Page<OrderMedicineDTO> page = orderMedicineService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/order-medicines");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
