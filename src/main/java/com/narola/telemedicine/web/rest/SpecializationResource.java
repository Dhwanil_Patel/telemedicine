package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.domain.Specialization;
import com.narola.telemedicine.repository.SpecializationRepository;
import com.narola.telemedicine.service.SpecializationService;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.errors.ErrorConstants;
import com.narola.telemedicine.web.rest.errors.SpecializationAlreadyUsedException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.SpecializationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Specialization.
 */
@RestController
@RequestMapping("/api")
public class SpecializationResource {

    private final Logger log = LoggerFactory.getLogger(SpecializationResource.class);

    private static final String ENTITY_NAME = "specialization";

    private final SpecializationService specializationService;

    private final SpecializationRepository specializationRepository;

    public SpecializationResource(SpecializationService specializationService,SpecializationRepository specializationRepository) {
        this.specializationService = specializationService;
        this.specializationRepository=specializationRepository;
    }

    /**
     * POST  /specializations : Create a new specialization.
     *
     * @param specializationDTO the specializationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new specializationDTO, or with status 400 (Bad Request) if the specialization has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/specializations")
    @Timed
    public ResponseEntity<SpecializationDTO> createSpecialization(@Valid @RequestBody SpecializationDTO specializationDTO) throws URISyntaxException, SpecializationAlreadyUsedException {
        log.debug("REST request to save Specialization : {}", specializationDTO);
        if (specializationDTO.getId() != null) {
            throw new BadRequestAlertException("A new specialization cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<Specialization> existingSpecialization = specializationRepository.findOneByExpertiseIgnoreCase(specializationDTO.getExpertise());
        if (existingSpecialization.isPresent()) {
            throw new SpecializationAlreadyUsedException(ErrorConstants.SPECIALIZATION_AREADLY_EXISTS);
        }
        SpecializationDTO result = specializationService.save(specializationDTO);
        return ResponseEntity.created(new URI("/api/specializations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /specializations : Updates an existing specialization.
     *
     * @param specializationDTO the specializationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated specializationDTO,
     * or with status 400 (Bad Request) if the specializationDTO is not valid,
     * or with status 500 (Internal Server Error) if the specializationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/specializations")
    @Timed
    public ResponseEntity<SpecializationDTO> updateSpecialization(@Valid @RequestBody SpecializationDTO specializationDTO) throws URISyntaxException, SpecializationAlreadyUsedException {
        log.debug("REST request to update Specialization : {}", specializationDTO);
        if (specializationDTO.getId() == null) {
            return createSpecialization(specializationDTO);
        }
        SpecializationDTO result = specializationService.save(specializationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, specializationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /specializations : get all the specializations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of specializations in body
     */
    @GetMapping("/specializations")
    @Timed
    public ResponseEntity<List<SpecializationDTO>> getAllSpecializations(Pageable pageable) {
        log.debug("REST request to get a page of Specializations");
        Page<SpecializationDTO> page = specializationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/specializations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /specializations/:id : get the "id" specialization.
     *
     * @param id the id of the specializationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the specializationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/specializations/{id}")
    @Timed
    public ResponseEntity<SpecializationDTO> getSpecialization(@PathVariable Long id) {
        log.debug("REST request to get Specialization : {}", id);
        SpecializationDTO specializationDTO = specializationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(specializationDTO));
    }

    /**
     * DELETE  /specializations/:id : delete the "id" specialization.
     *
     * @param id the id of the specializationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/specializations/{id}")
    @Timed
    public ResponseEntity<Void> deleteSpecialization(@PathVariable Long id) {
        log.debug("REST request to delete Specialization : {}", id);
        specializationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/specializations?query=:query : search for the specialization corresponding
     * to the query.
     *
     * @param query the query of the specialization search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/specializations")
    @Timed
    public ResponseEntity<List<SpecializationDTO>> searchSpecializations(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Specializations for query {}", query);
        Page<SpecializationDTO> page = specializationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/specializations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
