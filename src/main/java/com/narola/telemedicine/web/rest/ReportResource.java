package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.service.ReportService;
import com.narola.telemedicine.service.dto.PatientDTO;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pdhwanil on 31-05-2018.
 */
@RestController
@RequestMapping("/api")
public class ReportResource {

    private final Logger log = LoggerFactory.getLogger(ReportResource.class);

    private final ReportService reportService;

    public ReportResource(ReportService reportService) {
        this.reportService = reportService;
    }


    @GetMapping("/reports/total_members")
    @Timed
//    @RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate, @RequestParam(value = "reportType") String reportType, String filter
    public ResponseEntity<List<PatientDTO>> generateReport(Pageable pageable) {
        Page<PatientDTO> page = reportService.findAllPatient(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reports/total_members");
        log.info("Reports call successfully.");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
