package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.repository.UserRepository;
import com.narola.telemedicine.security.SecurityUtils;
import com.narola.telemedicine.service.PharmacistService;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.errors.InternalServerErrorException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.PharmacistDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pharmacist.
 */
@RestController
@RequestMapping("/api")
public class PharmacistResource {

    private final Logger log = LoggerFactory.getLogger(PharmacistResource.class);

    private static final String ENTITY_NAME = "pharmacist";

    private final PharmacistService pharmacistService;

    @Autowired
    private UserRepository userRepository;
    User user = new User();

    public PharmacistResource(PharmacistService pharmacistService) {
        this.pharmacistService = pharmacistService;
    }

    /**
     * POST  /pharmacists : Create a new pharmacist.
     *
     * @param pharmacistDTO the pharmacistDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pharmacistDTO, or with status 400 (Bad Request) if the pharmacist has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pharmacists")
    @Timed
    public ResponseEntity<PharmacistDTO> createPharmacist(@Valid @RequestBody PharmacistDTO pharmacistDTO) throws URISyntaxException {
        log.debug("REST request to save Pharmacist : {}", pharmacistDTO);
        if (pharmacistDTO.getId() != null) {
            throw new BadRequestAlertException("A new pharmacist cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PharmacistDTO result = pharmacistService.save(pharmacistDTO);
        return ResponseEntity.created(new URI("/api/pharmacists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pharmacists : Updates an existing pharmacist.
     *
     * @param pharmacistDTO the pharmacistDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pharmacistDTO,
     * or with status 400 (Bad Request) if the pharmacistDTO is not valid,
     * or with status 500 (Internal Server Error) if the pharmacistDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pharmacists")
    @Timed
    public ResponseEntity<PharmacistDTO> updatePharmacist(@Valid @RequestBody PharmacistDTO pharmacistDTO) throws URISyntaxException {
        log.debug("REST request to update Pharmacist : {}", pharmacistDTO);
        if (pharmacistDTO.getId() == null) {
            return createPharmacist(pharmacistDTO);
        }
        PharmacistDTO result = pharmacistService.save(pharmacistDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pharmacistDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pharmacists : get all the pharmacists.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of pharmacists in body
     */
    @GetMapping("/pharmacists")
    @Timed
    public ResponseEntity<List<PharmacistDTO>> getAllPharmacists(Pageable pageable, @RequestParam(required = false) String filter) {
        /*if ("ordermedicine-is-null".equals(filter)) {
            log.debug("REST request to get all Pharmacists where orderMedicine is null");
            return new ResponseEntity<>(pharmacistService.findAllWhereOrderMedicineIsNull(),
                    HttpStatus.OK);
        }*/
        log.debug("REST request to get a page of Pharmacists");
        Page<PharmacistDTO> page = pharmacistService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pharmacists");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pharmacists/:id : get the "id" pharmacist.
     *
     * @param id the id of the pharmacistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pharmacistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pharmacists/{id}")
    @Timed
    public ResponseEntity<PharmacistDTO> getPharmacist(@PathVariable Long id) {
        log.debug("REST request to get Pharmacist : {}", id);
        PharmacistDTO pharmacistDTO = pharmacistService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pharmacistDTO));
    }

    //get user by id
    @GetMapping("/pharmacists/userId")
    @Timed
    public ResponseEntity<PharmacistDTO> getPharmacistByUserID() {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        user = userRepository.findByLogin(userLogin);
        PharmacistDTO pharmacistDTO = pharmacistService.findByUserId(user.getId());
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pharmacistDTO));
    }

    /**
     * DELETE  /pharmacists/:id : delete the "id" pharmacist.
     *
     * @param id the id of the pharmacistDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pharmacists/{id}")
    @Timed
    public ResponseEntity<Void> deletePharmacist(@PathVariable Long id) {
        log.debug("REST request to delete Pharmacist : {}", id);
        pharmacistService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/pharmacists?query=:query : search for the pharmacist corresponding
     * to the query.
     *
     * @param query the query of the pharmacist search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/pharmacists")
    @Timed
    public ResponseEntity<List<PharmacistDTO>> searchPharmacists(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Pharmacists for query {}", query);
        Page<PharmacistDTO> page = pharmacistService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/pharmacists");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
