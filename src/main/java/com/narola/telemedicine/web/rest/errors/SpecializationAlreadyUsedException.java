package com.narola.telemedicine.web.rest.errors;

import org.zalando.problem.Status;

import java.net.URI;

/**
 * Created by skirti on 18-06-2018.
 */
public class SpecializationAlreadyUsedException extends Throwable {
    public SpecializationAlreadyUsedException(URI defaultType) {
        super(String.valueOf(ErrorConstants.DEFAULT_TYPE));
    }


}
