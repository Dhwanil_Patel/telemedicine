package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.domain.Doctor;
import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.repository.DoctorRepository;
import com.narola.telemedicine.repository.UserRepository;
import com.narola.telemedicine.security.SecurityUtils;
import com.narola.telemedicine.service.DoctorScheduleService;
import com.narola.telemedicine.service.dto.AppointmentDTO;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.errors.InternalServerErrorException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.DoctorScheduleDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DoctorSchedule.
 */
@RestController
@RequestMapping("/api")
public class DoctorScheduleResource {

    private final Logger log = LoggerFactory.getLogger(DoctorScheduleResource.class);

    private static final String ENTITY_NAME = "doctorSchedule";

    private final DoctorScheduleService doctorScheduleService;

    @Autowired
    private DoctorRepository doctorRepository;
    Doctor doctor= new Doctor();

    @Autowired
    private UserRepository userRepository;
    User user= new User();

    public DoctorScheduleResource(DoctorScheduleService doctorScheduleService) {
        this.doctorScheduleService = doctorScheduleService;
    }

    /**
     * POST  /doctor-schedules : Create a new doctorSchedule.
     *
     * @param doctorScheduleDTO the doctorScheduleDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new doctorScheduleDTO, or with status 400 (Bad Request) if the doctorSchedule has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/doctor-schedules")
    @Timed
    public ResponseEntity<DoctorScheduleDTO> createDoctorSchedule(@Valid @RequestBody DoctorScheduleDTO doctorScheduleDTO) throws URISyntaxException {
        log.debug("REST request to save DoctorSchedule : {}", doctorScheduleDTO);
        if (doctorScheduleDTO.getId() != null) {
            throw new BadRequestAlertException("A new doctorSchedule cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DoctorScheduleDTO result = doctorScheduleService.save(doctorScheduleDTO);
        return ResponseEntity.created(new URI("/api/doctor-schedules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /doctor-schedules : Updates an existing doctorSchedule.
     *
     * @param doctorScheduleDTO the doctorScheduleDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated doctorScheduleDTO,
     * or with status 400 (Bad Request) if the doctorScheduleDTO is not valid,
     * or with status 500 (Internal Server Error) if the doctorScheduleDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/doctor-schedules")
    @Timed
    public ResponseEntity<DoctorScheduleDTO> updateDoctorSchedule(@Valid @RequestBody DoctorScheduleDTO doctorScheduleDTO) throws URISyntaxException {
        log.debug("REST request to update DoctorSchedule : {}", doctorScheduleDTO);
        if (doctorScheduleDTO.getId() == null) {
            return createDoctorSchedule(doctorScheduleDTO);
        }
        DoctorScheduleDTO result = doctorScheduleService.save(doctorScheduleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, doctorScheduleDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /doctor-schedules : get all the doctorSchedules.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of doctorSchedules in body
     */
    @GetMapping("/doctor-schedules")
    @Timed
    public ResponseEntity<List<DoctorScheduleDTO>> getAllDoctorSchedules(Pageable pageable) {
        log.debug("REST request to get a page of DoctorSchedules");
        Page<DoctorScheduleDTO> page = doctorScheduleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/doctor-schedules");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /doctor-schedules/:id : get the "id" doctorSchedule.
     *
     * @param id the id of the doctorScheduleDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the doctorScheduleDTO, or with status 404 (Not Found)
     */
    @GetMapping("/doctor-schedules/{id}")
    @Timed
    public ResponseEntity<DoctorScheduleDTO> getDoctorSchedule(@PathVariable Long id) {
        log.debug("REST request to get DoctorSchedule : {}", id);
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(doctorScheduleDTO));
    }

    /**
     * DELETE  /doctor-schedules/:id : delete the "id" doctorSchedule.
     *
     * @param id the id of the doctorScheduleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/doctor-schedules/{id}")
    @Timed
    public ResponseEntity<Void> deleteDoctorSchedule(@PathVariable Long id) {
        log.debug("REST request to delete DoctorSchedule : {}", id);
        doctorScheduleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    //get doctorschedule by doctor id
    @GetMapping("/doctor-schedules/DoctorId")
    @Timed
    public ResponseEntity<List<DoctorScheduleDTO>> getScheduleByDoctorID(Pageable pageable, Long id) {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        user = userRepository.findByLogin(userLogin);
        doctor = doctorRepository.findByUserId(user.getId());;
        Page<DoctorScheduleDTO> page = doctorScheduleService.findByDoctorId(doctor.getId(),pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/doctor-schedules/DoctorId");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * SEARCH  /_search/doctor-schedules?query=:query : search for the doctorSchedule corresponding
     * to the query.
     *
     * @param query the query of the doctorSchedule search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/doctor-schedules")
    @Timed
    public ResponseEntity<List<DoctorScheduleDTO>> searchDoctorSchedules(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of DoctorSchedules for query {}", query);
        Page<DoctorScheduleDTO> page = doctorScheduleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/doctor-schedules");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
