/**
 * View Models used by Spring MVC REST controllers.
 */
package com.narola.telemedicine.web.rest.vm;
