package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.domain.Appointment;
import com.narola.telemedicine.domain.Doctor;
import com.narola.telemedicine.domain.Patient;
import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.repository.DoctorRepository;
import com.narola.telemedicine.repository.PatientRepository;
import com.narola.telemedicine.repository.UserRepository;
import com.narola.telemedicine.security.SecurityUtils;
import com.narola.telemedicine.service.AppointmentService;
import com.narola.telemedicine.service.DoctorService;
import com.narola.telemedicine.service.dto.DoctorDTO;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.errors.InternalServerErrorException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.AppointmentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.google.api.plus.Person;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Appointment.
 */
@RestController
@RequestMapping("/api")
public class AppointmentResource {

    private final Logger log = LoggerFactory.getLogger(AppointmentResource.class);

    private static final String ENTITY_NAME = "appointment";

    private final AppointmentService appointmentService;

    EntityManager entityManager;

    @Autowired
    private PatientRepository patientRepository;
    Patient patient = new Patient();

    @Autowired
    private DoctorRepository doctorRepository;
    Doctor doctor= new Doctor();

    @Autowired
    private UserRepository userRepository;
    User user= new User();

    public AppointmentResource(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    /**
     * POST  /appointments : Create a new appointment.
     *
     * @param appointmentDTO the appointmentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new appointmentDTO, or with status 400 (Bad Request) if the appointment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/appointments")
    @Timed
    public ResponseEntity<AppointmentDTO> createAppointment(@Valid @RequestBody AppointmentDTO appointmentDTO) throws URISyntaxException {
        log.debug("REST request to save Appointment : {}", appointmentDTO);
        if (appointmentDTO.getId() != null) {
            throw new BadRequestAlertException("A new appointment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AppointmentDTO result = appointmentService.save(appointmentDTO);
        return ResponseEntity.created(new URI("/api/appointments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /appointments : Updates an existing appointment.
     *
     * @param appointmentDTO the appointmentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated appointmentDTO,
     * or with status 400 (Bad Request) if the appointmentDTO is not valid,
     * or with status 500 (Internal Server Error) if the appointmentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/appointments")
    @Timed
    public ResponseEntity<AppointmentDTO> updateAppointment(@Valid @RequestBody AppointmentDTO appointmentDTO) throws URISyntaxException {
        log.debug("REST request to update Appointment : {}", appointmentDTO);
        if (appointmentDTO.getId() == null) {
            return createAppointment(appointmentDTO);
        }
        AppointmentDTO result = appointmentService.save(appointmentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, appointmentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /appointments : get all the appointments.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of appointments in body
     */
    @GetMapping("/appointments")
    @Timed
    public ResponseEntity<List<AppointmentDTO>> getAllAppointments(Pageable pageable, @RequestParam(required = false) String filter) {
        if ("payment-is-null".equals(filter)) {
            log.debug("REST request to get all Appointments where payment is null");
            return new ResponseEntity<>(appointmentService.findAllWherePaymentIsNull(),
                HttpStatus.OK);
        }
        if ("prescription-is-null".equals(filter)) {
            log.debug("REST request to get all Appointments where prescription is null");
            return new ResponseEntity<>(appointmentService.findAllWherePrescriptionIsNull(),
                HttpStatus.OK);
        }
        log.debug("REST request to get a page of Appointments");
        Page<AppointmentDTO> page = appointmentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    // get doctor name
//    @GetMapping("/appointments/id/{id}")
//    @Timed
//    public ResponseEntity<List<Appointment>> getbyDoctorId(@PathVariable Long id) {
//        // Page<AppointmentDTO> page =appointmentService.find(id);
//        List<Appointment> page = appointmentService.finddoctor(id);
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("doctorname","api/appointments/id");
//            //PaginationUtil.generatePaginationHttpHeaders("api/appointments/id");
//        return new ResponseEntity<>(page,headers,HttpStatus.OK);
//    }

    //get appointment by doctor id
    @GetMapping("/appointments/DoctorId")
    @Timed
    public ResponseEntity<List<AppointmentDTO>> getAppointmentByDoctorID(Pageable pageable,String filter) {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        user = userRepository.findByLogin(userLogin);
        doctor = doctorRepository.findByUserId(user.getId());;

        if("prescription-is-null".equals(filter)) {
            log.debug("REST request to get all Appointments where prescription is null");
            return new ResponseEntity<>(appointmentService.findDoctorIdWherePrescriptionIsNull(doctor.getId()),HttpStatus.OK);
        }
        Page<AppointmentDTO> page = appointmentService.findByDoctorId(doctor.getId(),pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointments/DoctorId");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
 //get appointment by doctor id
//    @GetMapping("/appointments/DoctorId")
//    @Timed
//    public ResponseEntity<List<AppointmentDTO>> getAppointmentByDoctorID(Pageable pageable,String filter) {
//        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
//        user = userRepository.findByLogin(userLogin);
//        doctor = doctorRepository.findByUserId(user.getId());;
//
//        if("prescription-is-null".equals(filter)) {
//            log.debug("REST request to get all Appointments where prescription is null");
//            return new ResponseEntity<>(appointmentService.findDoctorIdWherePrescriptionIsNull(doctor.getId()),HttpStatus.OK);
//        }
// //        List<AppointmentDTO> page = appointmentService.findAppointmentWithDetails(doctor.getId());
//        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointments/DoctorId");
//        return new ResponseEntity<>(appointmentService.findAppointmentWithDetails(doctor.getId()), HttpStatus.OK);
//    }
    //get appointment by patient id
    @GetMapping("/appointments/PatientId")
    @Timed
    public ResponseEntity<List<AppointmentDTO>> getAppointmentByPatientID(Pageable pageable,String filter) {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        user = userRepository.findByLogin(userLogin);
        patient = patientRepository.findByUserId(user.getId());

        if ("payment-is-null".equals(filter)) {
            log.debug("REST request to get all Appointments where payment is null");
            return new ResponseEntity<>(appointmentService.findPatientIdWherePaymentIsNull(patient.getId()),
                HttpStatus.OK);
        }
        Page<AppointmentDTO> page = appointmentService.findByPatientId(patient.getId(),pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointments/PatientId");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /appointments/:id : get the "id" appointment.
     *
     * @param id the id of the appointmentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the appointmentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/appointments/{id}")
    @Timed
    public ResponseEntity<AppointmentDTO> getAppointment(@PathVariable Long id) {
        log.debug("REST request to get Appointment : {}", id);
        AppointmentDTO appointmentDTO = appointmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(appointmentDTO));
    }

    /**
     * DELETE  /appointments/:id : delete the "id" appointment.
     *
     * @param id the id of the appointmentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/appointments/{id}")
    @Timed
    public ResponseEntity<Void> deleteAppointment(@PathVariable Long id) {
        log.debug("REST request to delete Appointment : {}", id);
        appointmentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/appointments?query=:query : search for the appointment corresponding
     * to the query.
     *
     * @param query the query of the appointment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/appointments")
    @Timed
    public ResponseEntity<List<AppointmentDTO>> searchAppointments(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Appointments for query {}", query);
        Page<AppointmentDTO> page = appointmentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/appointments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
//    @GetMapping("/appointment_patientdata")
//    @Timed
//    public ResponseEntity<List<AppointmentDTO>> appointmentPatientDetails(String query) {
//        List<Appointment> appointments = entityManager.createNativeQuery(
//            "SELECT a.*, p.firstname FROM appointment a,patient p WHERE p.id = a.patient_id", Appointment.class).getResultList();
////        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(appointments));
//      HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query,appointments, "/api/appointment_patientdata");
//        return new ResponseEntity<List<AppointmentDTO>>((MultiValueMap<String, String>) appointments, HttpStatus.OK);
//    }
//    public ResponseEntity<List<Appointment>> appointmentPatientDetails() {
//        List<Appointment> appointments = entityManager.createNativeQuery(
//            "SELECT a.*, p.firstname FROM appointment a,patient p WHERE p.id = a.patient_id", Appointment.class).getResultList();
//        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(appointments));
//        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders((MultiValueMap<String, String>) appointments,"/api/_search/appointments");
//        return new ResponseEntity<List<Appointment>>(appointments, HttpStatus.OK);
//    }
}

//package com.narola.telemedicine.web.rest;
//
//import com.codahale.metrics.annotation.Timed;
//import com.narola.telemedicine.service.AppointmentService;
//import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
//import com.narola.telemedicine.web.rest.util.HeaderUtil;
//import com.narola.telemedicine.web.rest.util.PaginationUtil;
//import com.narola.telemedicine.service.dto.AppointmentDTO;
//import io.github.jhipster.web.util.ResponseUtil;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//import java.net.URI;
//import java.net.URISyntaxException;
//
//import java.util.List;
//import java.util.Optional;
//import java.util.stream.StreamSupport;
//
//import static org.elasticsearch.index.query.QueryBuilders.*;
//
///**
// * REST controller for managing Appointment.
// */
//@RestController
//@RequestMapping("/api")
//public class AppointmentResource {
//
//    private final Logger log = LoggerFactory.getLogger(AppointmentResource.class);
//
//    private static final String ENTITY_NAME = "appointment";
//
//    private final AppointmentService appointmentService;
//
//    public AppointmentResource(AppointmentService appointmentService) {
//        this.appointmentService = appointmentService;
//    }
//
//    /**
//     * POST  /appointments : Create a new appointment.
//     *
//     * @param appointmentDTO the appointmentDTO to create
//     * @return the ResponseEntity with status 201 (Created) and with body the new appointmentDTO, or with status 400 (Bad Request) if the appointment has already an ID
//     * @throws URISyntaxException if the Location URI syntax is incorrect
//     */
//    @PostMapping("/appointments")
//    @Timed
//    public ResponseEntity<AppointmentDTO> createAppointment(@Valid @RequestBody AppointmentDTO appointmentDTO) throws URISyntaxException {
//        log.debug("REST request to save Appointment : {}", appointmentDTO);
//        if (appointmentDTO.getId() != null) {
//            throw new BadRequestAlertException("A new appointment cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        AppointmentDTO result = appointmentService.save(appointmentDTO);
//        return ResponseEntity.created(new URI("/api/appointments/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }
//
//    /**
//     * PUT  /appointments : Updates an existing appointment.
//     *
//     * @param appointmentDTO the appointmentDTO to update
//     * @return the ResponseEntity with status 200 (OK) and with body the updated appointmentDTO,
//     * or with status 400 (Bad Request) if the appointmentDTO is not valid,
//     * or with status 500 (Internal Server Error) if the appointmentDTO couldn't be updated
//     * @throws URISyntaxException if the Location URI syntax is incorrect
//     */
//    @PutMapping("/appointments")
//    @Timed
//    public ResponseEntity<AppointmentDTO> updateAppointment(@Valid @RequestBody AppointmentDTO appointmentDTO) throws URISyntaxException {
//        log.debug("REST request to update Appointment : {}", appointmentDTO);
//        if (appointmentDTO.getId() == null) {
//            return createAppointment(appointmentDTO);
//        }
//        AppointmentDTO result = appointmentService.save(appointmentDTO);
//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, appointmentDTO.getId().toString()))
//            .body(result);
//    }
//
//    /**
//     * GET  /appointments : get all the appointments.
//     *
//     * @param pageable the pagination information
//     * @param filter the filter of the request
//     * @return the ResponseEntity with status 200 (OK) and the list of appointments in body
//     */
//    @GetMapping("/appointments")
//    @Timed
//    public ResponseEntity<List<AppointmentDTO>> getAllAppointments(Pageable pageable, @RequestParam(required = false) String filter) {
//        if ("payment-is-null".equals(filter)) {
//            log.debug("REST request to get all Appointments where payment is null");
//            return new ResponseEntity<>(appointmentService.findAllWherePaymentIsNull(),
//                    HttpStatus.OK);
//        }
//        if ("prescription-is-null".equals(filter)) {
//            log.debug("REST request to get all Appointments where prescription is null");
//            return new ResponseEntity<>(appointmentService.findAllWherePrescriptionIsNull(),
//                    HttpStatus.OK);
//        }
//        log.debug("REST request to get a page of Appointments");
//        Page<AppointmentDTO> page = appointmentService.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointments");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }
//
//    /**
//     * GET  /appointments/:id : get the "id" appointment.
//     *
//     * @param id the id of the appointmentDTO to retrieve
//     * @return the ResponseEntity with status 200 (OK) and with body the appointmentDTO, or with status 404 (Not Found)
//     */
//    @GetMapping("/appointments/{id}")
//    @Timed
//    public ResponseEntity<AppointmentDTO> getAppointment(@PathVariable Long id) {
//        log.debug("REST request to get Appointment : {}", id);
//        AppointmentDTO appointmentDTO = appointmentService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(appointmentDTO));
//    }
//
//    /**
//     * DELETE  /appointments/:id : delete the "id" appointment.
//     *
//     * @param id the id of the appointmentDTO to delete
//     * @return the ResponseEntity with status 200 (OK)
//     */
//    @DeleteMapping("/appointments/{id}")
//    @Timed
//    public ResponseEntity<Void> deleteAppointment(@PathVariable Long id) {
//        log.debug("REST request to delete Appointment : {}", id);
//        appointmentService.delete(id);
//        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
//    }
//
//    /**
//     * SEARCH  /_search/appointments?query=:query : search for the appointment corresponding
//     * to the query.
//     *
//     * @param query the query of the appointment search
//     * @param pageable the pagination information
//     * @return the result of the search
//     */
//    @GetMapping("/_search/appointments")
//    @Timed
//    public ResponseEntity<List<AppointmentDTO>> searchAppointments(@RequestParam String query, Pageable pageable) {
//        log.debug("REST request to search for a page of Appointments for query {}", query);
//        Page<AppointmentDTO> page = appointmentService.search(query, pageable);
//        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/appointments");
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
//    }
//
//}
