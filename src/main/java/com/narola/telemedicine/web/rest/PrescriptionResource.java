package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.domain.Doctor;
import com.narola.telemedicine.domain.Patient;
import com.narola.telemedicine.domain.User;
import com.narola.telemedicine.repository.DoctorRepository;
import com.narola.telemedicine.repository.PatientRepository;
import com.narola.telemedicine.repository.UserRepository;
import com.narola.telemedicine.security.SecurityUtils;
import com.narola.telemedicine.service.PrescriptionService;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.errors.InternalServerErrorException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.PrescriptionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import io.undertow.util.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Prescription.
 */
@RestController
@RequestMapping("/api")
public class PrescriptionResource {

    private final Logger log = LoggerFactory.getLogger(PrescriptionResource.class);

    private static final String ENTITY_NAME = "prescription";

    private final PrescriptionService prescriptionService;

    @Autowired
    private PatientRepository patientRepository;
    Patient patient = new Patient();

    @Autowired
    private UserRepository userRepository;
    User user= new User();

    public PrescriptionResource(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }


    /**
     * POST  /prescriptions : Create a new prescription.
     *
     * @param prescriptionDTO the prescriptionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new prescriptionDTO, or with status 400 (Bad Request) if the prescription has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/prescriptions")
    @Timed
    public ResponseEntity<PrescriptionDTO> createPrescription(@Valid @RequestBody PrescriptionDTO prescriptionDTO) throws URISyntaxException {
        log.debug("REST request to save Prescription : {}", prescriptionDTO);
        if (prescriptionDTO.getId() != null) {
            throw new BadRequestAlertException("A new prescription cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PrescriptionDTO result = prescriptionService.save(prescriptionDTO);
        return ResponseEntity.created(new URI("/api/prescriptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /prescriptions : Updates an existing prescription.
     *
     * @param prescriptionDTO the prescriptionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated prescriptionDTO,
     * or with status 400 (Bad Request) if the prescriptionDTO is not valid,
     * or with status 500 (Internal Server Error) if the prescriptionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/prescriptions")
    @Timed
    public ResponseEntity<PrescriptionDTO> updatePrescription(@Valid @RequestBody PrescriptionDTO prescriptionDTO) throws URISyntaxException {
        log.debug("REST request to update Prescription : {}", prescriptionDTO);
        if (prescriptionDTO.getId() == null) {
            return createPrescription(prescriptionDTO);
        }
        PrescriptionDTO result = prescriptionService.save(prescriptionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, prescriptionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /prescriptions : get all the prescriptions.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of prescriptions in body
     */
    @GetMapping("/prescriptions")
    @Timed
    public ResponseEntity<List<PrescriptionDTO>> getAllPrescriptions(Pageable pageable, @RequestParam(required = false) String filter) {
        if ("ordermedicine-is-null".equals(filter)) {
            log.debug("REST request to get all Prescriptions where orderMedicine is null");
            return new ResponseEntity<>(prescriptionService.findAllWhereOrderMedicineIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of Prescriptions");
        Page<PrescriptionDTO> page = prescriptionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prescriptions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /prescriptions/:id : get the "id" prescription.
     *
     * @param id the id of the prescriptionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the prescriptionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/prescriptions/{id}")
    @Timed
    public ResponseEntity<PrescriptionDTO> getPrescription(@PathVariable Long id) {
        log.debug("REST request to get Prescription : {}", id);
        PrescriptionDTO prescriptionDTO = prescriptionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(prescriptionDTO));
    }

    // find by doctor id
    @GetMapping("/prescriptions/doctor/{id}")
    @Timed
    public ResponseEntity<List<PrescriptionDTO>> getPrescriptionByDoctorId(@PathVariable Long id) {
        log.debug("REST request to get Prescription : {}", id);
        List<PrescriptionDTO> list = prescriptionService.findPrescriptionByDoctorId(id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Doctor-prescription", "/api/prescriptions/doctor");
        return new ResponseEntity<List<PrescriptionDTO>>(list,headers,HttpStatus.OK);
    }

    // find by patient id
    @GetMapping("/prescriptions/patient")
    @Timed
    public ResponseEntity<List<PrescriptionDTO>> findPrescriptionByPatientId(String filter) {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        user = userRepository.findByLogin(userLogin);
        patient = patientRepository.findByUserId(user.getId());
        log.debug("REST request to get Prescription : {}", patient.getId());
        if ("ordermedicine-is-null".equals(filter)) {
            log.debug("REST request to get all Prescriptions by patient id  where orderMedicine is null");
            return new ResponseEntity<>(prescriptionService.findPrescriptionByPatientIdWhereOrderMedicineIsNull(patient.getId()),
                HttpStatus.OK);
        }
        List<PrescriptionDTO> list = prescriptionService.findPrescriptionByPatientId(patient.getId());
        HttpHeaders headers = new HttpHeaders();
        headers.add("patient-prescription", "/api/prescriptions/patient");
        return new ResponseEntity<List<PrescriptionDTO>>(list,headers,HttpStatus.OK);
    }

    /**
     * DELETE  /prescriptions/:id : delete the "id" prescription.
     *
     * @param id the id of the prescriptionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/prescriptions/{id}")
    @Timed
    public ResponseEntity<Void> deletePrescription(@PathVariable Long id) {
        log.debug("REST request to delete Prescription : {}", id);
        prescriptionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/prescriptions?query=:query : search for the prescription corresponding
     * to the query.
     *
     * @param query the query of the prescription search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/prescriptions")
    @Timed
    public ResponseEntity<List<PrescriptionDTO>> searchPrescriptions(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Prescriptions for query {}", query);
        Page<PrescriptionDTO> page = prescriptionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/prescriptions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
