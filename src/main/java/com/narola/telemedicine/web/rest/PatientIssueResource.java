package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.service.PatientIssueService;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.PatientIssueDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PatientIssue.
 */
@RestController
@RequestMapping("/api")
public class PatientIssueResource {

    private final Logger log = LoggerFactory.getLogger(PatientIssueResource.class);

    private static final String ENTITY_NAME = "PatientIssue";

    private final PatientIssueService patientIssueService;

    public PatientIssueResource(PatientIssueService patientIssueService) {
        this.patientIssueService = patientIssueService;
    }

    /**
     * POST  /patient-issue : Create a new PatientIssue.
     *
     * @param PatientIssueDTO the PatientIssueDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new PatientIssueDTO, or with status 400 (Bad Request) if the PatientIssue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/patient-issue")
    @Timed
    public ResponseEntity<PatientIssueDTO> createPatientIssue(@Valid @RequestBody PatientIssueDTO PatientIssueDTO) throws URISyntaxException {
        log.debug("REST request to save PatientIssue : {}", PatientIssueDTO);
        if (PatientIssueDTO.getId() != null) {
            throw new BadRequestAlertException("A new PatientIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PatientIssueDTO result = patientIssueService.save(PatientIssueDTO);
        return ResponseEntity.created(new URI("/api/patient-issue/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /patient-issue : Updates an existing PatientIssue.
     *
     * @param PatientIssueDTO the PatientIssueDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated PatientIssueDTO,
     * or with status 400 (Bad Request) if the PatientIssueDTO is not valid,
     * or with status 500 (Internal Server Error) if the PatientIssueDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/patient-issue")
    @Timed
    public ResponseEntity<PatientIssueDTO> updatePatientIssue(@Valid @RequestBody PatientIssueDTO PatientIssueDTO) throws URISyntaxException {
        log.debug("REST request to update PatientIssue : {}", PatientIssueDTO);
        if (PatientIssueDTO.getId() == null) {
            return createPatientIssue(PatientIssueDTO);
        }
        PatientIssueDTO result = patientIssueService.save(PatientIssueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, PatientIssueDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /patient-issue : get all the PatientIssue.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of PatientIssue in body
     */
    @GetMapping("/patient-issue")
    @Timed
    public ResponseEntity<List<PatientIssueDTO>> getAllPatientIssue(Pageable pageable) {
        log.debug("REST request to get a page of PatientIssue");
        Page<PatientIssueDTO> page = patientIssueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/patient-issue");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /patient-issue/:id : get the "id" PatientIssue.
     *
     * @param id the id of the PatientIssueDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the PatientIssueDTO, or with status 404 (Not Found)
     */
    @GetMapping("/patient-issue/{id}")
    @Timed
    public ResponseEntity<PatientIssueDTO> getPatientIssue(@PathVariable Long id) {
        log.debug("REST request to get PatientIssue : {}", id);
        PatientIssueDTO PatientIssueDTO = patientIssueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(PatientIssueDTO));
    }

    /**
     * DELETE  /patient-issue/:id : delete the "id" PatientIssue.
     *
     * @param id the id of the PatientIssueDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/patient-issue/{id}")
    @Timed
    public ResponseEntity<Void> deletePatientIssue(@PathVariable Long id) {
        log.debug("REST request to delete PatientIssue : {}", id);
        patientIssueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/patient-issue?query=:query : search for the PatientIssue corresponding
     * to the query.
     *
     * @param query the query of the PatientIssue search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/patient-issue")
    @Timed
    public ResponseEntity<List<PatientIssueDTO>> searchPatientIssue(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PatientIssue for query {}", query);
        Page<PatientIssueDTO> page = patientIssueService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/patient-issue");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
