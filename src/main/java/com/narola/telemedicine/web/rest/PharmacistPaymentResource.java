package com.narola.telemedicine.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.narola.telemedicine.domain.PharmacistPayment;
import com.narola.telemedicine.service.PharmacistPaymentService;
import com.narola.telemedicine.web.rest.errors.BadRequestAlertException;
import com.narola.telemedicine.web.rest.util.HeaderUtil;
import com.narola.telemedicine.web.rest.util.PaginationUtil;
import com.narola.telemedicine.service.dto.PharmacistPaymentDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PharmacistPayment.
 */
@RestController
@RequestMapping("/api")
public class PharmacistPaymentResource {

    private final Logger log = LoggerFactory.getLogger(PharmacistPaymentResource.class);

    private static final String ENTITY_NAME = "pharmacistPayment";

    private final PharmacistPaymentService pharmacistPaymentService;

    public PharmacistPaymentResource(PharmacistPaymentService pharmacistPaymentService) {
        this.pharmacistPaymentService = pharmacistPaymentService;
    }

    /**
     * POST  /pharmacist-payments : Create a new pharmacistPayment.
     *
     * @param pharmacistPaymentDTO the pharmacistPaymentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pharmacistPaymentDTO, or with status 400 (Bad Request) if the pharmacistPayment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pharmacist-payments")
    @Timed
    public ResponseEntity<PharmacistPaymentDTO> createPharmacistPayment(@Valid @RequestBody PharmacistPaymentDTO pharmacistPaymentDTO) throws URISyntaxException {
        log.debug("REST request to save PharmacistPayment : {}", pharmacistPaymentDTO);
        if (pharmacistPaymentDTO.getId() != null) {
            throw new BadRequestAlertException("A new pharmacistPayment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PharmacistPaymentDTO result = pharmacistPaymentService.save(pharmacistPaymentDTO);
        return ResponseEntity.created(new URI("/api/pharmacist-payments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pharmacist-payments : Updates an existing pharmacistPayment.
     *
     * @param pharmacistPaymentDTO the pharmacistPaymentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pharmacistPaymentDTO,
     * or with status 400 (Bad Request) if the pharmacistPaymentDTO is not valid,
     * or with status 500 (Internal Server Error) if the pharmacistPaymentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pharmacist-payments")
    @Timed
    public ResponseEntity<PharmacistPaymentDTO> updatePharmacistPayment(@Valid @RequestBody PharmacistPaymentDTO pharmacistPaymentDTO) throws URISyntaxException {
        log.debug("REST request to update PharmacistPayment : {}", pharmacistPaymentDTO);
        if (pharmacistPaymentDTO.getId() == null) {
            return createPharmacistPayment(pharmacistPaymentDTO);
        }
        PharmacistPaymentDTO result = pharmacistPaymentService.save(pharmacistPaymentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pharmacistPaymentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pharmacist-payments : get all the pharmacistPayments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pharmacistPayments in body
     */
    @GetMapping("/pharmacist-payments")
    @Timed
    public ResponseEntity<List<PharmacistPaymentDTO>> getAllPharmacistPayments(Pageable pageable) {
        log.debug("REST request to get a page of PharmacistPayments");
        Page<PharmacistPaymentDTO> page = pharmacistPaymentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pharmacist-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    // pharmacist_payment by pharmacist_id
    @GetMapping("/pharmacist-payments/pharmacist/{id}")
    @Timed
    public ResponseEntity<List<PharmacistPaymentDTO>> getAllPharmacistPaymentsbyPharmacistId(@PathVariable Long id) {
        log.debug("REST request to get a page of PharmacistPayments");
        List<PharmacistPaymentDTO> page = pharmacistPaymentService.findByPharmacistId(id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("pharmacist-payments", "/api/pharmacist-payments/pharmacist");
        return new ResponseEntity<>(page, headers, HttpStatus.OK);
    }

    // pharmacist_payment by patinet_id
    @GetMapping("/pharmacist-payments/patient/{id}")
    @Timed
    public ResponseEntity<List<PharmacistPaymentDTO>> getAllPharmacistPaymentsbyPatientId(@PathVariable Long id) {
        log.debug("REST request to get a page of PharmacistPayments");
        List<PharmacistPaymentDTO> page = pharmacistPaymentService.findByPatientId(id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("pharmacist-payments", "/api/pharmacist-payments/patient");
        return new ResponseEntity<>(page, headers, HttpStatus.OK);
    }
    /**
     * GET  /pharmacist-payments/:id : get the "id" pharmacistPayment.
     *
     * @param id the id of the pharmacistPaymentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pharmacistPaymentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pharmacist-payments/{id}")
    @Timed
    public ResponseEntity<PharmacistPaymentDTO> getPharmacistPayment(@PathVariable Long id) {
        log.debug("REST request to get PharmacistPayment : {}", id);
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pharmacistPaymentDTO));
    }

    /**
     * DELETE  /pharmacist-payments/:id : delete the "id" pharmacistPayment.
     *
     * @param id the id of the pharmacistPaymentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pharmacist-payments/{id}")
    @Timed
    public ResponseEntity<Void> deletePharmacistPayment(@PathVariable Long id) {
        log.debug("REST request to delete PharmacistPayment : {}", id);
        pharmacistPaymentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/pharmacist-payments?query=:query : search for the pharmacistPayment corresponding
     * to the query.
     *
     * @param query the query of the pharmacistPayment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/pharmacist-payments")
    @Timed
    public ResponseEntity<List<PharmacistPaymentDTO>> searchPharmacistPayments(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of PharmacistPayments for query {}", query);
        Page<PharmacistPaymentDTO> page = pharmacistPaymentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/pharmacist-payments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
