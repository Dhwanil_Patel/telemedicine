package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.Degree;
import com.narola.telemedicine.repository.DegreeRepository;
import com.narola.telemedicine.service.DegreeService;
import com.narola.telemedicine.repository.search.DegreeSearchRepository;
import com.narola.telemedicine.service.dto.DegreeDTO;
import com.narola.telemedicine.service.mapper.DegreeMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DegreeResource REST controller.
 *
 * @see DegreeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class DegreeResourceIntTest {

    private static final String DEFAULT_DEGREENAME = "AAAAAAAAAA";
    private static final String UPDATED_DEGREENAME = "BBBBBBBBBB";

    @Autowired
    private DegreeRepository degreeRepository;

    @Autowired
    private DegreeMapper degreeMapper;

    @Autowired
    private DegreeService degreeService;

    @Autowired
    private DegreeSearchRepository degreeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDegreeMockMvc;

    private Degree degree;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DegreeResource degreeResource = new DegreeResource(degreeService);
        this.restDegreeMockMvc = MockMvcBuilders.standaloneSetup(degreeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Degree createEntity(EntityManager em) {
        Degree degree = new Degree()
            .degreename(DEFAULT_DEGREENAME);
        return degree;
    }

    @Before
    public void initTest() {
        degreeSearchRepository.deleteAll();
        degree = createEntity(em);
    }

    @Test
    @Transactional
    public void createDegree() throws Exception {
        int databaseSizeBeforeCreate = degreeRepository.findAll().size();

        // Create the Degree
        DegreeDTO degreeDTO = degreeMapper.toDto(degree);
        restDegreeMockMvc.perform(post("/api/degrees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(degreeDTO)))
            .andExpect(status().isCreated());

        // Validate the Degree in the database
        List<Degree> degreeList = degreeRepository.findAll();
        assertThat(degreeList).hasSize(databaseSizeBeforeCreate + 1);
        Degree testDegree = degreeList.get(degreeList.size() - 1);
        assertThat(testDegree.getDegreename()).isEqualTo(DEFAULT_DEGREENAME);

        // Validate the Degree in Elasticsearch
        Degree degreeEs = degreeSearchRepository.findOne(testDegree.getId());
        assertThat(degreeEs).isEqualToIgnoringGivenFields(testDegree);
    }

    @Test
    @Transactional
    public void createDegreeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = degreeRepository.findAll().size();

        // Create the Degree with an existing ID
        degree.setId(1L);
        DegreeDTO degreeDTO = degreeMapper.toDto(degree);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDegreeMockMvc.perform(post("/api/degrees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(degreeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Degree in the database
        List<Degree> degreeList = degreeRepository.findAll();
        assertThat(degreeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDegreenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = degreeRepository.findAll().size();
        // set the field null
        degree.setDegreename(null);

        // Create the Degree, which fails.
        DegreeDTO degreeDTO = degreeMapper.toDto(degree);

        restDegreeMockMvc.perform(post("/api/degrees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(degreeDTO)))
            .andExpect(status().isBadRequest());

        List<Degree> degreeList = degreeRepository.findAll();
        assertThat(degreeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDegrees() throws Exception {
        // Initialize the database
        degreeRepository.saveAndFlush(degree);

        // Get all the degreeList
        restDegreeMockMvc.perform(get("/api/degrees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(degree.getId().intValue())))
            .andExpect(jsonPath("$.[*].degreename").value(hasItem(DEFAULT_DEGREENAME.toString())));
    }

    @Test
    @Transactional
    public void getDegree() throws Exception {
        // Initialize the database
        degreeRepository.saveAndFlush(degree);

        // Get the degree
        restDegreeMockMvc.perform(get("/api/degrees/{id}", degree.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(degree.getId().intValue()))
            .andExpect(jsonPath("$.degreename").value(DEFAULT_DEGREENAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDegree() throws Exception {
        // Get the degree
        restDegreeMockMvc.perform(get("/api/degrees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDegree() throws Exception {
        // Initialize the database
        degreeRepository.saveAndFlush(degree);
        degreeSearchRepository.save(degree);
        int databaseSizeBeforeUpdate = degreeRepository.findAll().size();

        // Update the degree
        Degree updatedDegree = degreeRepository.findOne(degree.getId());
        // Disconnect from session so that the updates on updatedDegree are not directly saved in db
        em.detach(updatedDegree);
        updatedDegree
            .degreename(UPDATED_DEGREENAME);
        DegreeDTO degreeDTO = degreeMapper.toDto(updatedDegree);

        restDegreeMockMvc.perform(put("/api/degrees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(degreeDTO)))
            .andExpect(status().isOk());

        // Validate the Degree in the database
        List<Degree> degreeList = degreeRepository.findAll();
        assertThat(degreeList).hasSize(databaseSizeBeforeUpdate);
        Degree testDegree = degreeList.get(degreeList.size() - 1);
        assertThat(testDegree.getDegreename()).isEqualTo(UPDATED_DEGREENAME);

        // Validate the Degree in Elasticsearch
        Degree degreeEs = degreeSearchRepository.findOne(testDegree.getId());
        assertThat(degreeEs).isEqualToIgnoringGivenFields(testDegree);
    }

    @Test
    @Transactional
    public void updateNonExistingDegree() throws Exception {
        int databaseSizeBeforeUpdate = degreeRepository.findAll().size();

        // Create the Degree
        DegreeDTO degreeDTO = degreeMapper.toDto(degree);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDegreeMockMvc.perform(put("/api/degrees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(degreeDTO)))
            .andExpect(status().isCreated());

        // Validate the Degree in the database
        List<Degree> degreeList = degreeRepository.findAll();
        assertThat(degreeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDegree() throws Exception {
        // Initialize the database
        degreeRepository.saveAndFlush(degree);
        degreeSearchRepository.save(degree);
        int databaseSizeBeforeDelete = degreeRepository.findAll().size();

        // Get the degree
        restDegreeMockMvc.perform(delete("/api/degrees/{id}", degree.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean degreeExistsInEs = degreeSearchRepository.exists(degree.getId());
        assertThat(degreeExistsInEs).isFalse();

        // Validate the database is empty
        List<Degree> degreeList = degreeRepository.findAll();
        assertThat(degreeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDegree() throws Exception {
        // Initialize the database
        degreeRepository.saveAndFlush(degree);
        degreeSearchRepository.save(degree);

        // Search the degree
        restDegreeMockMvc.perform(get("/api/_search/degrees?query=id:" + degree.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(degree.getId().intValue())))
            .andExpect(jsonPath("$.[*].degreename").value(hasItem(DEFAULT_DEGREENAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Degree.class);
        Degree degree1 = new Degree();
        degree1.setId(1L);
        Degree degree2 = new Degree();
        degree2.setId(degree1.getId());
        assertThat(degree1).isEqualTo(degree2);
        degree2.setId(2L);
        assertThat(degree1).isNotEqualTo(degree2);
        degree1.setId(null);
        assertThat(degree1).isNotEqualTo(degree2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DegreeDTO.class);
        DegreeDTO degreeDTO1 = new DegreeDTO();
        degreeDTO1.setId(1L);
        DegreeDTO degreeDTO2 = new DegreeDTO();
        assertThat(degreeDTO1).isNotEqualTo(degreeDTO2);
        degreeDTO2.setId(degreeDTO1.getId());
        assertThat(degreeDTO1).isEqualTo(degreeDTO2);
        degreeDTO2.setId(2L);
        assertThat(degreeDTO1).isNotEqualTo(degreeDTO2);
        degreeDTO1.setId(null);
        assertThat(degreeDTO1).isNotEqualTo(degreeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(degreeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(degreeMapper.fromId(null)).isNull();
    }
}
