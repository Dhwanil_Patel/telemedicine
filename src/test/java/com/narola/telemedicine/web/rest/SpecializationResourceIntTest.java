package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.Specialization;
import com.narola.telemedicine.repository.SpecializationRepository;
import com.narola.telemedicine.service.SpecializationService;
import com.narola.telemedicine.repository.search.SpecializationSearchRepository;
import com.narola.telemedicine.service.dto.SpecializationDTO;
import com.narola.telemedicine.service.mapper.SpecializationMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SpecializationResource REST controller.
 *
 * @see SpecializationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class SpecializationResourceIntTest {

    private static final String DEFAULT_EXPERTISE = "AAAAAAAAAA";
    private static final String UPDATED_EXPERTISE = "BBBBBBBBBB";

    @Autowired
    private SpecializationRepository specializationRepository;

    @Autowired
    private SpecializationMapper specializationMapper;

    @Autowired
    private SpecializationService specializationService;

    @Autowired
    private SpecializationSearchRepository specializationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSpecializationMockMvc;

    private Specialization specialization;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpecializationResource specializationResource = new SpecializationResource(specializationService,specializationRepository);
        this.restSpecializationMockMvc = MockMvcBuilders.standaloneSetup(specializationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Specialization createEntity(EntityManager em) {
        Specialization specialization = new Specialization()
            .expertise(DEFAULT_EXPERTISE);
        return specialization;
    }

    @Before
    public void initTest() {
        specializationSearchRepository.deleteAll();
        specialization = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpecialization() throws Exception {
        int databaseSizeBeforeCreate = specializationRepository.findAll().size();

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);
        restSpecializationMockMvc.perform(post("/api/specializations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specializationDTO)))
            .andExpect(status().isCreated());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeCreate + 1);
        Specialization testSpecialization = specializationList.get(specializationList.size() - 1);
        assertThat(testSpecialization.getExpertise()).isEqualTo(DEFAULT_EXPERTISE);

        // Validate the Specialization in Elasticsearch
        Specialization specializationEs = specializationSearchRepository.findOne(testSpecialization.getId());
        assertThat(specializationEs).isEqualToIgnoringGivenFields(testSpecialization);
    }

    @Test
    @Transactional
    public void createSpecializationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = specializationRepository.findAll().size();

        // Create the Specialization with an existing ID
        specialization.setId(1L);
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpecializationMockMvc.perform(post("/api/specializations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specializationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkExpertiseIsRequired() throws Exception {
        int databaseSizeBeforeTest = specializationRepository.findAll().size();
        // set the field null
        specialization.setExpertise(null);

        // Create the Specialization, which fails.
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        restSpecializationMockMvc.perform(post("/api/specializations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specializationDTO)))
            .andExpect(status().isBadRequest());

        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpecializations() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList
        restSpecializationMockMvc.perform(get("/api/specializations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specialization.getId().intValue())))
            .andExpect(jsonPath("$.[*].expertise").value(hasItem(DEFAULT_EXPERTISE.toString())));
    }

    @Test
    @Transactional
    public void getSpecialization() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get the specialization
        restSpecializationMockMvc.perform(get("/api/specializations/{id}", specialization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(specialization.getId().intValue()))
            .andExpect(jsonPath("$.expertise").value(DEFAULT_EXPERTISE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSpecialization() throws Exception {
        // Get the specialization
        restSpecializationMockMvc.perform(get("/api/specializations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpecialization() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);
        specializationSearchRepository.save(specialization);
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();

        // Update the specialization
        Specialization updatedSpecialization = specializationRepository.findOne(specialization.getId());
        // Disconnect from session so that the updates on updatedSpecialization are not directly saved in db
        em.detach(updatedSpecialization);
        updatedSpecialization
            .expertise(UPDATED_EXPERTISE);
        SpecializationDTO specializationDTO = specializationMapper.toDto(updatedSpecialization);

        restSpecializationMockMvc.perform(put("/api/specializations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specializationDTO)))
            .andExpect(status().isOk());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
        Specialization testSpecialization = specializationList.get(specializationList.size() - 1);
        assertThat(testSpecialization.getExpertise()).isEqualTo(UPDATED_EXPERTISE);

        // Validate the Specialization in Elasticsearch
        Specialization specializationEs = specializationSearchRepository.findOne(testSpecialization.getId());
        assertThat(specializationEs).isEqualToIgnoringGivenFields(testSpecialization);
    }

    @Test
    @Transactional
    public void updateNonExistingSpecialization() throws Exception {
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSpecializationMockMvc.perform(put("/api/specializations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(specializationDTO)))
            .andExpect(status().isCreated());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSpecialization() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);
        specializationSearchRepository.save(specialization);
        int databaseSizeBeforeDelete = specializationRepository.findAll().size();

        // Get the specialization
        restSpecializationMockMvc.perform(delete("/api/specializations/{id}", specialization.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean specializationExistsInEs = specializationSearchRepository.exists(specialization.getId());
        assertThat(specializationExistsInEs).isFalse();

        // Validate the database is empty
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSpecialization() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);
        specializationSearchRepository.save(specialization);

        // Search the specialization
        restSpecializationMockMvc.perform(get("/api/_search/specializations?query=id:" + specialization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specialization.getId().intValue())))
            .andExpect(jsonPath("$.[*].expertise").value(hasItem(DEFAULT_EXPERTISE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Specialization.class);
        Specialization specialization1 = new Specialization();
        specialization1.setId(1L);
        Specialization specialization2 = new Specialization();
        specialization2.setId(specialization1.getId());
        assertThat(specialization1).isEqualTo(specialization2);
        specialization2.setId(2L);
        assertThat(specialization1).isNotEqualTo(specialization2);
        specialization1.setId(null);
        assertThat(specialization1).isNotEqualTo(specialization2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpecializationDTO.class);
        SpecializationDTO specializationDTO1 = new SpecializationDTO();
        specializationDTO1.setId(1L);
        SpecializationDTO specializationDTO2 = new SpecializationDTO();
        assertThat(specializationDTO1).isNotEqualTo(specializationDTO2);
        specializationDTO2.setId(specializationDTO1.getId());
        assertThat(specializationDTO1).isEqualTo(specializationDTO2);
        specializationDTO2.setId(2L);
        assertThat(specializationDTO1).isNotEqualTo(specializationDTO2);
        specializationDTO1.setId(null);
        assertThat(specializationDTO1).isNotEqualTo(specializationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(specializationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(specializationMapper.fromId(null)).isNull();
    }
}
