package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.OrderMedicine;
import com.narola.telemedicine.repository.OrderMedicineRepository;
import com.narola.telemedicine.service.OrderMedicineService;
import com.narola.telemedicine.repository.search.OrderMedicineSearchRepository;
import com.narola.telemedicine.service.dto.OrderMedicineDTO;
import com.narola.telemedicine.service.mapper.OrderMedicineMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OrderMedicineResource REST controller.
 *
 * @see OrderMedicineResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class OrderMedicineResourceIntTest {

    private static final String DEFAULT_MEDICINEDETAIL = "AAAAAAAAAA";
    private static final String UPDATED_MEDICINEDETAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_TOTALPRICE = 1;
    private static final Integer UPDATED_TOTALPRICE = 2;

    @Autowired
    private OrderMedicineRepository orderMedicineRepository;

    @Autowired
    private OrderMedicineMapper orderMedicineMapper;

    @Autowired
    private OrderMedicineService orderMedicineService;

    @Autowired
    private OrderMedicineSearchRepository orderMedicineSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOrderMedicineMockMvc;

    private OrderMedicine orderMedicine;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrderMedicineResource orderMedicineResource = new OrderMedicineResource(orderMedicineService);
        this.restOrderMedicineMockMvc = MockMvcBuilders.standaloneSetup(orderMedicineResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderMedicine createEntity(EntityManager em) {
        OrderMedicine orderMedicine = new OrderMedicine()
            .medicinedetail(DEFAULT_MEDICINEDETAIL)
            .totalprice(DEFAULT_TOTALPRICE);
        return orderMedicine;
    }

    @Before
    public void initTest() {
        orderMedicineSearchRepository.deleteAll();
        orderMedicine = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderMedicine() throws Exception {
        int databaseSizeBeforeCreate = orderMedicineRepository.findAll().size();

        // Create the OrderMedicine
        OrderMedicineDTO orderMedicineDTO = orderMedicineMapper.toDto(orderMedicine);
        restOrderMedicineMockMvc.perform(post("/api/order-medicines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderMedicineDTO)))
            .andExpect(status().isCreated());

        // Validate the OrderMedicine in the database
        List<OrderMedicine> orderMedicineList = orderMedicineRepository.findAll();
        assertThat(orderMedicineList).hasSize(databaseSizeBeforeCreate + 1);
        OrderMedicine testOrderMedicine = orderMedicineList.get(orderMedicineList.size() - 1);
        assertThat(testOrderMedicine.getMedicinedetail()).isEqualTo(DEFAULT_MEDICINEDETAIL);
        assertThat(testOrderMedicine.getTotalprice()).isEqualTo(DEFAULT_TOTALPRICE);

        // Validate the OrderMedicine in Elasticsearch
        OrderMedicine orderMedicineEs = orderMedicineSearchRepository.findOne(testOrderMedicine.getId());
        assertThat(orderMedicineEs).isEqualToIgnoringGivenFields(testOrderMedicine);
    }

    @Test
    @Transactional
    public void createOrderMedicineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderMedicineRepository.findAll().size();

        // Create the OrderMedicine with an existing ID
        orderMedicine.setId(1L);
        OrderMedicineDTO orderMedicineDTO = orderMedicineMapper.toDto(orderMedicine);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderMedicineMockMvc.perform(post("/api/order-medicines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderMedicineDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrderMedicine in the database
        List<OrderMedicine> orderMedicineList = orderMedicineRepository.findAll();
        assertThat(orderMedicineList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMedicinedetailIsRequired() throws Exception {
        int databaseSizeBeforeTest = orderMedicineRepository.findAll().size();
        // set the field null
        orderMedicine.setMedicinedetail(null);

        // Create the OrderMedicine, which fails.
        OrderMedicineDTO orderMedicineDTO = orderMedicineMapper.toDto(orderMedicine);

        restOrderMedicineMockMvc.perform(post("/api/order-medicines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderMedicineDTO)))
            .andExpect(status().isBadRequest());

        List<OrderMedicine> orderMedicineList = orderMedicineRepository.findAll();
        assertThat(orderMedicineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTotalpriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = orderMedicineRepository.findAll().size();
        // set the field null
        orderMedicine.setTotalprice(null);

        // Create the OrderMedicine, which fails.
        OrderMedicineDTO orderMedicineDTO = orderMedicineMapper.toDto(orderMedicine);

        restOrderMedicineMockMvc.perform(post("/api/order-medicines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderMedicineDTO)))
            .andExpect(status().isBadRequest());

        List<OrderMedicine> orderMedicineList = orderMedicineRepository.findAll();
        assertThat(orderMedicineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOrderMedicines() throws Exception {
        // Initialize the database
        orderMedicineRepository.saveAndFlush(orderMedicine);

        // Get all the orderMedicineList
        restOrderMedicineMockMvc.perform(get("/api/order-medicines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderMedicine.getId().intValue())))
            .andExpect(jsonPath("$.[*].medicinedetail").value(hasItem(DEFAULT_MEDICINEDETAIL.toString())))
            .andExpect(jsonPath("$.[*].totalprice").value(hasItem(DEFAULT_TOTALPRICE)));
    }

    @Test
    @Transactional
    public void getOrderMedicine() throws Exception {
        // Initialize the database
        orderMedicineRepository.saveAndFlush(orderMedicine);

        // Get the orderMedicine
        restOrderMedicineMockMvc.perform(get("/api/order-medicines/{id}", orderMedicine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(orderMedicine.getId().intValue()))
            .andExpect(jsonPath("$.medicinedetail").value(DEFAULT_MEDICINEDETAIL.toString()))
            .andExpect(jsonPath("$.totalprice").value(DEFAULT_TOTALPRICE));
    }

    @Test
    @Transactional
    public void getNonExistingOrderMedicine() throws Exception {
        // Get the orderMedicine
        restOrderMedicineMockMvc.perform(get("/api/order-medicines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderMedicine() throws Exception {
        // Initialize the database
        orderMedicineRepository.saveAndFlush(orderMedicine);
        orderMedicineSearchRepository.save(orderMedicine);
        int databaseSizeBeforeUpdate = orderMedicineRepository.findAll().size();

        // Update the orderMedicine
        OrderMedicine updatedOrderMedicine = orderMedicineRepository.findOne(orderMedicine.getId());
        // Disconnect from session so that the updates on updatedOrderMedicine are not directly saved in db
        em.detach(updatedOrderMedicine);
        updatedOrderMedicine
            .medicinedetail(UPDATED_MEDICINEDETAIL)
            .totalprice(UPDATED_TOTALPRICE);
        OrderMedicineDTO orderMedicineDTO = orderMedicineMapper.toDto(updatedOrderMedicine);

        restOrderMedicineMockMvc.perform(put("/api/order-medicines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderMedicineDTO)))
            .andExpect(status().isOk());

        // Validate the OrderMedicine in the database
        List<OrderMedicine> orderMedicineList = orderMedicineRepository.findAll();
        assertThat(orderMedicineList).hasSize(databaseSizeBeforeUpdate);
        OrderMedicine testOrderMedicine = orderMedicineList.get(orderMedicineList.size() - 1);
        assertThat(testOrderMedicine.getMedicinedetail()).isEqualTo(UPDATED_MEDICINEDETAIL);
        assertThat(testOrderMedicine.getTotalprice()).isEqualTo(UPDATED_TOTALPRICE);

        // Validate the OrderMedicine in Elasticsearch
        OrderMedicine orderMedicineEs = orderMedicineSearchRepository.findOne(testOrderMedicine.getId());
        assertThat(orderMedicineEs).isEqualToIgnoringGivenFields(testOrderMedicine);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderMedicine() throws Exception {
        int databaseSizeBeforeUpdate = orderMedicineRepository.findAll().size();

        // Create the OrderMedicine
        OrderMedicineDTO orderMedicineDTO = orderMedicineMapper.toDto(orderMedicine);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOrderMedicineMockMvc.perform(put("/api/order-medicines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderMedicineDTO)))
            .andExpect(status().isCreated());

        // Validate the OrderMedicine in the database
        List<OrderMedicine> orderMedicineList = orderMedicineRepository.findAll();
        assertThat(orderMedicineList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteOrderMedicine() throws Exception {
        // Initialize the database
        orderMedicineRepository.saveAndFlush(orderMedicine);
        orderMedicineSearchRepository.save(orderMedicine);
        int databaseSizeBeforeDelete = orderMedicineRepository.findAll().size();

        // Get the orderMedicine
        restOrderMedicineMockMvc.perform(delete("/api/order-medicines/{id}", orderMedicine.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean orderMedicineExistsInEs = orderMedicineSearchRepository.exists(orderMedicine.getId());
        assertThat(orderMedicineExistsInEs).isFalse();

        // Validate the database is empty
        List<OrderMedicine> orderMedicineList = orderMedicineRepository.findAll();
        assertThat(orderMedicineList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchOrderMedicine() throws Exception {
        // Initialize the database
        orderMedicineRepository.saveAndFlush(orderMedicine);
        orderMedicineSearchRepository.save(orderMedicine);

        // Search the orderMedicine
        restOrderMedicineMockMvc.perform(get("/api/_search/order-medicines?query=id:" + orderMedicine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderMedicine.getId().intValue())))
            .andExpect(jsonPath("$.[*].medicinedetail").value(hasItem(DEFAULT_MEDICINEDETAIL.toString())))
            .andExpect(jsonPath("$.[*].totalprice").value(hasItem(DEFAULT_TOTALPRICE)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderMedicine.class);
        OrderMedicine orderMedicine1 = new OrderMedicine();
        orderMedicine1.setId(1L);
        OrderMedicine orderMedicine2 = new OrderMedicine();
        orderMedicine2.setId(orderMedicine1.getId());
        assertThat(orderMedicine1).isEqualTo(orderMedicine2);
        orderMedicine2.setId(2L);
        assertThat(orderMedicine1).isNotEqualTo(orderMedicine2);
        orderMedicine1.setId(null);
        assertThat(orderMedicine1).isNotEqualTo(orderMedicine2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderMedicineDTO.class);
        OrderMedicineDTO orderMedicineDTO1 = new OrderMedicineDTO();
        orderMedicineDTO1.setId(1L);
        OrderMedicineDTO orderMedicineDTO2 = new OrderMedicineDTO();
        assertThat(orderMedicineDTO1).isNotEqualTo(orderMedicineDTO2);
        orderMedicineDTO2.setId(orderMedicineDTO1.getId());
        assertThat(orderMedicineDTO1).isEqualTo(orderMedicineDTO2);
        orderMedicineDTO2.setId(2L);
        assertThat(orderMedicineDTO1).isNotEqualTo(orderMedicineDTO2);
        orderMedicineDTO1.setId(null);
        assertThat(orderMedicineDTO1).isNotEqualTo(orderMedicineDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(orderMedicineMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(orderMedicineMapper.fromId(null)).isNull();
    }
}
