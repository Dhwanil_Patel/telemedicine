package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.PatientIssue;
import com.narola.telemedicine.repository.PatientIssueRepository;
import com.narola.telemedicine.service.PatientIssueService;
import com.narola.telemedicine.repository.search.PatientIssueSearchRepository;
import com.narola.telemedicine.service.dto.PatientIssueDTO;
import com.narola.telemedicine.service.mapper.PatientIssueMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PatientIssueResource REST controller.
 *
 * @see PatientIssueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class PatientIssueResourceIntTest {

    private static final String DEFAULT_ISSUE = "AAAAAAAAAA";
    private static final String UPDATED_ISSUE = "BBBBBBBBBB";

    @Autowired
    private PatientIssueRepository PatientIssueRepository;

    @Autowired
    private PatientIssueMapper PatientIssueMapper;

    @Autowired
    private PatientIssueService PatientIssueService;

    @Autowired
    private PatientIssueSearchRepository PatientIssueSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPatientIssueMockMvc;

    private PatientIssue PatientIssue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PatientIssueResource PatientIssueResource = new PatientIssueResource(PatientIssueService);
        this.restPatientIssueMockMvc = MockMvcBuilders.standaloneSetup(PatientIssueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PatientIssue createEntity(EntityManager em) {
        PatientIssue PatientIssue = new PatientIssue()
            .issue(DEFAULT_ISSUE);
        return PatientIssue;
    }

    @Before
    public void initTest() {
        PatientIssueSearchRepository.deleteAll();
        PatientIssue = createEntity(em);
    }

    @Test
    @Transactional
    public void createPatientIssue() throws Exception {
        int databaseSizeBeforeCreate = PatientIssueRepository.findAll().size();

        // Create the PatientIssue
        PatientIssueDTO PatientIssueDTO = PatientIssueMapper.toDto(PatientIssue);
        restPatientIssueMockMvc.perform(post("/api/patient-issue")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(PatientIssueDTO)))
            .andExpect(status().isCreated());

        // Validate the PatientIssue in the database
        List<PatientIssue> PatientIssueList = PatientIssueRepository.findAll();
        assertThat(PatientIssueList).hasSize(databaseSizeBeforeCreate + 1);
        PatientIssue testPatientIssue = PatientIssueList.get(PatientIssueList.size() - 1);
        assertThat(testPatientIssue.getIssue()).isEqualTo(DEFAULT_ISSUE);

        // Validate the PatientIssue in Elasticsearch
        PatientIssue PatientIssueEs = PatientIssueSearchRepository.findOne(testPatientIssue.getId());
        assertThat(PatientIssueEs).isEqualToIgnoringGivenFields(testPatientIssue);
    }

    @Test
    @Transactional
    public void createPatientIssueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = PatientIssueRepository.findAll().size();

        // Create the PatientIssue with an existing ID
        PatientIssue.setId(1L);
        PatientIssueDTO PatientIssueDTO = PatientIssueMapper.toDto(PatientIssue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPatientIssueMockMvc.perform(post("/api/patient-issue")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(PatientIssueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PatientIssue in the database
        List<PatientIssue> PatientIssueList = PatientIssueRepository.findAll();
        assertThat(PatientIssueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIssueIsRequired() throws Exception {
        int databaseSizeBeforeTest = PatientIssueRepository.findAll().size();
        // set the field null
        PatientIssue.setIssue(null);

        // Create the PatientIssue, which fails.
        PatientIssueDTO PatientIssueDTO = PatientIssueMapper.toDto(PatientIssue);

        restPatientIssueMockMvc.perform(post("/api/patient-issue")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(PatientIssueDTO)))
            .andExpect(status().isBadRequest());

        List<PatientIssue> PatientIssueList = PatientIssueRepository.findAll();
        assertThat(PatientIssueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPatientIssue() throws Exception {
        // Initialize the database
        PatientIssueRepository.saveAndFlush(PatientIssue);

        // Get all the PatientIssueList
        restPatientIssueMockMvc.perform(get("/api/patient-issue?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(PatientIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].issue").value(hasItem(DEFAULT_ISSUE.toString())));
    }

    @Test
    @Transactional
    public void getPatientIssue() throws Exception {
        // Initialize the database
        PatientIssueRepository.saveAndFlush(PatientIssue);

        // Get the PatientIssue
        restPatientIssueMockMvc.perform(get("/api/patient-issue/{id}", PatientIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(PatientIssue.getId().intValue()))
            .andExpect(jsonPath("$.issue").value(DEFAULT_ISSUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPatientIssue() throws Exception {
        // Get the PatientIssue
        restPatientIssueMockMvc.perform(get("/api/patient-issue/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePatientIssue() throws Exception {
        // Initialize the database
        PatientIssueRepository.saveAndFlush(PatientIssue);
        PatientIssueSearchRepository.save(PatientIssue);
        int databaseSizeBeforeUpdate = PatientIssueRepository.findAll().size();

        // Update the PatientIssue
        PatientIssue updatedPatientIssue = PatientIssueRepository.findOne(PatientIssue.getId());
        // Disconnect from session so that the updates on updatedPatientIssue are not directly saved in db
        em.detach(updatedPatientIssue);
        updatedPatientIssue
            .issue(UPDATED_ISSUE);
        PatientIssueDTO PatientIssueDTO = PatientIssueMapper.toDto(updatedPatientIssue);

        restPatientIssueMockMvc.perform(put("/api/patient-issue")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(PatientIssueDTO)))
            .andExpect(status().isOk());

        // Validate the PatientIssue in the database
        List<PatientIssue> PatientIssueList = PatientIssueRepository.findAll();
        assertThat(PatientIssueList).hasSize(databaseSizeBeforeUpdate);
        PatientIssue testPatientIssue = PatientIssueList.get(PatientIssueList.size() - 1);
        assertThat(testPatientIssue.getIssue()).isEqualTo(UPDATED_ISSUE);

        // Validate the PatientIssue in Elasticsearch
        PatientIssue PatientIssueEs = PatientIssueSearchRepository.findOne(testPatientIssue.getId());
        assertThat(PatientIssueEs).isEqualToIgnoringGivenFields(testPatientIssue);
    }

    @Test
    @Transactional
    public void updateNonExistingPatientIssue() throws Exception {
        int databaseSizeBeforeUpdate = PatientIssueRepository.findAll().size();

        // Create the PatientIssue
        PatientIssueDTO PatientIssueDTO = PatientIssueMapper.toDto(PatientIssue);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPatientIssueMockMvc.perform(put("/api/patient-issue")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(PatientIssueDTO)))
            .andExpect(status().isCreated());

        // Validate the PatientIssue in the database
        List<PatientIssue> PatientIssueList = PatientIssueRepository.findAll();
        assertThat(PatientIssueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePatientIssue() throws Exception {
        // Initialize the database
        PatientIssueRepository.saveAndFlush(PatientIssue);
        PatientIssueSearchRepository.save(PatientIssue);
        int databaseSizeBeforeDelete = PatientIssueRepository.findAll().size();

        // Get the PatientIssue
        restPatientIssueMockMvc.perform(delete("/api/patient-issue/{id}", PatientIssue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean PatientIssueExistsInEs = PatientIssueSearchRepository.exists(PatientIssue.getId());
        assertThat(PatientIssueExistsInEs).isFalse();

        // Validate the database is empty
        List<PatientIssue> PatientIssueList = PatientIssueRepository.findAll();
        assertThat(PatientIssueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPatientIssue() throws Exception {
        // Initialize the database
        PatientIssueRepository.saveAndFlush(PatientIssue);
        PatientIssueSearchRepository.save(PatientIssue);

        // Search the PatientIssue
        restPatientIssueMockMvc.perform(get("/api/_search/patient-issue?query=id:" + PatientIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(PatientIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].issue").value(hasItem(DEFAULT_ISSUE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PatientIssue.class);
        PatientIssue PatientIssue1 = new PatientIssue();
        PatientIssue1.setId(1L);
        PatientIssue PatientIssue2 = new PatientIssue();
        PatientIssue2.setId(PatientIssue1.getId());
        assertThat(PatientIssue1).isEqualTo(PatientIssue2);
        PatientIssue2.setId(2L);
        assertThat(PatientIssue1).isNotEqualTo(PatientIssue2);
        PatientIssue1.setId(null);
        assertThat(PatientIssue1).isNotEqualTo(PatientIssue2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PatientIssueDTO.class);
        PatientIssueDTO PatientIssueDTO1 = new PatientIssueDTO();
        PatientIssueDTO1.setId(1L);
        PatientIssueDTO PatientIssueDTO2 = new PatientIssueDTO();
        assertThat(PatientIssueDTO1).isNotEqualTo(PatientIssueDTO2);
        PatientIssueDTO2.setId(PatientIssueDTO1.getId());
        assertThat(PatientIssueDTO1).isEqualTo(PatientIssueDTO2);
        PatientIssueDTO2.setId(2L);
        assertThat(PatientIssueDTO1).isNotEqualTo(PatientIssueDTO2);
        PatientIssueDTO1.setId(null);
        assertThat(PatientIssueDTO1).isNotEqualTo(PatientIssueDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(PatientIssueMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(PatientIssueMapper.fromId(null)).isNull();
    }
}
