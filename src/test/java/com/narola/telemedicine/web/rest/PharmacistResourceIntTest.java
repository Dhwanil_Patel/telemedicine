package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.Pharmacist;
import com.narola.telemedicine.repository.PharmacistRepository;
import com.narola.telemedicine.service.PharmacistService;
import com.narola.telemedicine.repository.search.PharmacistSearchRepository;
import com.narola.telemedicine.service.dto.PharmacistDTO;
import com.narola.telemedicine.service.mapper.PharmacistMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.narola.telemedicine.domain.enumeration.Process_Status;
/**
 * Test class for the PharmacistResource REST controller.
 *
 * @see PharmacistResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class PharmacistResourceIntTest {

    private static final String DEFAULT_SHOPNAME = "AAAAAAAAAA";
    private static final String UPDATED_SHOPNAME = "BBBBBBBBBB";

    private static final Process_Status DEFAULT_STATUS = Process_Status.ACTIVE;
    private static final Process_Status UPDATED_STATUS = Process_Status.DEACTIVE;

    private static final String DEFAULT_SHOPADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_SHOPADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONENO = "AAAAAAAAAA";
    private static final String UPDATED_PHONENO = "BBBBBBBBBB";

    private static final String DEFAULT_LICENCENO = "AAAAAAAAAA";
    private static final String UPDATED_LICENCENO = "BBBBBBBBBB";

    private static final byte[] DEFAULT_DOCUMENTS = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_DOCUMENTS = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_DOCUMENTS_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_DOCUMENTS_CONTENT_TYPE = "image/png";

    @Autowired
    private PharmacistRepository pharmacistRepository;

    @Autowired
    private PharmacistMapper pharmacistMapper;

    @Autowired
    private PharmacistService pharmacistService;

    @Autowired
    private PharmacistSearchRepository pharmacistSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPharmacistMockMvc;

    private Pharmacist pharmacist;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PharmacistResource pharmacistResource = new PharmacistResource(pharmacistService);
        this.restPharmacistMockMvc = MockMvcBuilders.standaloneSetup(pharmacistResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pharmacist createEntity(EntityManager em) {
        Pharmacist pharmacist = new Pharmacist()
            .shopname(DEFAULT_SHOPNAME)
            .status(DEFAULT_STATUS)
            .shopaddress(DEFAULT_SHOPADDRESS)
            .phoneno(DEFAULT_PHONENO)
            .licenceno(DEFAULT_LICENCENO)
            .documents(DEFAULT_DOCUMENTS)
            .documentsContentType(DEFAULT_DOCUMENTS_CONTENT_TYPE);
        return pharmacist;
    }

    @Before
    public void initTest() {
        pharmacistSearchRepository.deleteAll();
        pharmacist = createEntity(em);
    }

    @Test
    @Transactional
    public void createPharmacist() throws Exception {
        int databaseSizeBeforeCreate = pharmacistRepository.findAll().size();

        // Create the Pharmacist
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);
        restPharmacistMockMvc.perform(post("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isCreated());

        // Validate the Pharmacist in the database
        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeCreate + 1);
        Pharmacist testPharmacist = pharmacistList.get(pharmacistList.size() - 1);
        assertThat(testPharmacist.getShopname()).isEqualTo(DEFAULT_SHOPNAME);
        assertThat(testPharmacist.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPharmacist.getShopaddress()).isEqualTo(DEFAULT_SHOPADDRESS);
        assertThat(testPharmacist.getPhoneno()).isEqualTo(DEFAULT_PHONENO);
        assertThat(testPharmacist.getLicenceno()).isEqualTo(DEFAULT_LICENCENO);
        assertThat(testPharmacist.getDocuments()).isEqualTo(DEFAULT_DOCUMENTS);
        assertThat(testPharmacist.getDocumentsContentType()).isEqualTo(DEFAULT_DOCUMENTS_CONTENT_TYPE);

        // Validate the Pharmacist in Elasticsearch
        Pharmacist pharmacistEs = pharmacistSearchRepository.findOne(testPharmacist.getId());
        assertThat(pharmacistEs).isEqualToIgnoringGivenFields(testPharmacist);
    }

    @Test
    @Transactional
    public void createPharmacistWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pharmacistRepository.findAll().size();

        // Create the Pharmacist with an existing ID
        pharmacist.setId(1L);
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPharmacistMockMvc.perform(post("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pharmacist in the database
        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkShopnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistRepository.findAll().size();
        // set the field null
        pharmacist.setShopname(null);

        // Create the Pharmacist, which fails.
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);

        restPharmacistMockMvc.perform(post("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isBadRequest());

        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkShopaddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistRepository.findAll().size();
        // set the field null
        pharmacist.setShopaddress(null);

        // Create the Pharmacist, which fails.
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);

        restPharmacistMockMvc.perform(post("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isBadRequest());

        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhonenoIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistRepository.findAll().size();
        // set the field null
        pharmacist.setPhoneno(null);

        // Create the Pharmacist, which fails.
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);

        restPharmacistMockMvc.perform(post("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isBadRequest());

        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLicencenoIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistRepository.findAll().size();
        // set the field null
        pharmacist.setLicenceno(null);

        // Create the Pharmacist, which fails.
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);

        restPharmacistMockMvc.perform(post("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isBadRequest());

        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDocumentsIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistRepository.findAll().size();
        // set the field null
        pharmacist.setDocuments(null);

        // Create the Pharmacist, which fails.
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);

        restPharmacistMockMvc.perform(post("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isBadRequest());

        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPharmacists() throws Exception {
        // Initialize the database
        pharmacistRepository.saveAndFlush(pharmacist);

        // Get all the pharmacistList
        restPharmacistMockMvc.perform(get("/api/pharmacists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pharmacist.getId().intValue())))
            .andExpect(jsonPath("$.[*].shopname").value(hasItem(DEFAULT_SHOPNAME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].shopaddress").value(hasItem(DEFAULT_SHOPADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phoneno").value(hasItem(DEFAULT_PHONENO.toString())))
            .andExpect(jsonPath("$.[*].licenceno").value(hasItem(DEFAULT_LICENCENO.toString())))
            .andExpect(jsonPath("$.[*].documentsContentType").value(hasItem(DEFAULT_DOCUMENTS_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].documents").value(hasItem(Base64Utils.encodeToString(DEFAULT_DOCUMENTS))));
    }

    @Test
    @Transactional
    public void getPharmacist() throws Exception {
        // Initialize the database
        pharmacistRepository.saveAndFlush(pharmacist);

        // Get the pharmacist
        restPharmacistMockMvc.perform(get("/api/pharmacists/{id}", pharmacist.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pharmacist.getId().intValue()))
            .andExpect(jsonPath("$.shopname").value(DEFAULT_SHOPNAME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.shopaddress").value(DEFAULT_SHOPADDRESS.toString()))
            .andExpect(jsonPath("$.phoneno").value(DEFAULT_PHONENO.toString()))
            .andExpect(jsonPath("$.licenceno").value(DEFAULT_LICENCENO.toString()))
            .andExpect(jsonPath("$.documentsContentType").value(DEFAULT_DOCUMENTS_CONTENT_TYPE))
            .andExpect(jsonPath("$.documents").value(Base64Utils.encodeToString(DEFAULT_DOCUMENTS)));
    }

    @Test
    @Transactional
    public void getNonExistingPharmacist() throws Exception {
        // Get the pharmacist
        restPharmacistMockMvc.perform(get("/api/pharmacists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePharmacist() throws Exception {
        // Initialize the database
        pharmacistRepository.saveAndFlush(pharmacist);
        pharmacistSearchRepository.save(pharmacist);
        int databaseSizeBeforeUpdate = pharmacistRepository.findAll().size();

        // Update the pharmacist
        Pharmacist updatedPharmacist = pharmacistRepository.findOne(pharmacist.getId());
        // Disconnect from session so that the updates on updatedPharmacist are not directly saved in db
        em.detach(updatedPharmacist);
        updatedPharmacist
            .shopname(UPDATED_SHOPNAME)
            .status(UPDATED_STATUS)
            .shopaddress(UPDATED_SHOPADDRESS)
            .phoneno(UPDATED_PHONENO)
            .licenceno(UPDATED_LICENCENO)
            .documents(UPDATED_DOCUMENTS)
            .documentsContentType(UPDATED_DOCUMENTS_CONTENT_TYPE);
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(updatedPharmacist);

        restPharmacistMockMvc.perform(put("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isOk());

        // Validate the Pharmacist in the database
        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeUpdate);
        Pharmacist testPharmacist = pharmacistList.get(pharmacistList.size() - 1);
        assertThat(testPharmacist.getShopname()).isEqualTo(UPDATED_SHOPNAME);
        assertThat(testPharmacist.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPharmacist.getShopaddress()).isEqualTo(UPDATED_SHOPADDRESS);
        assertThat(testPharmacist.getPhoneno()).isEqualTo(UPDATED_PHONENO);
        assertThat(testPharmacist.getLicenceno()).isEqualTo(UPDATED_LICENCENO);
        assertThat(testPharmacist.getDocuments()).isEqualTo(UPDATED_DOCUMENTS);
        assertThat(testPharmacist.getDocumentsContentType()).isEqualTo(UPDATED_DOCUMENTS_CONTENT_TYPE);

        // Validate the Pharmacist in Elasticsearch
        Pharmacist pharmacistEs = pharmacistSearchRepository.findOne(testPharmacist.getId());
        assertThat(pharmacistEs).isEqualToIgnoringGivenFields(testPharmacist);
    }

    @Test
    @Transactional
    public void updateNonExistingPharmacist() throws Exception {
        int databaseSizeBeforeUpdate = pharmacistRepository.findAll().size();

        // Create the Pharmacist
        PharmacistDTO pharmacistDTO = pharmacistMapper.toDto(pharmacist);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPharmacistMockMvc.perform(put("/api/pharmacists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistDTO)))
            .andExpect(status().isCreated());

        // Validate the Pharmacist in the database
        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePharmacist() throws Exception {
        // Initialize the database
        pharmacistRepository.saveAndFlush(pharmacist);
        pharmacistSearchRepository.save(pharmacist);
        int databaseSizeBeforeDelete = pharmacistRepository.findAll().size();

        // Get the pharmacist
        restPharmacistMockMvc.perform(delete("/api/pharmacists/{id}", pharmacist.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean pharmacistExistsInEs = pharmacistSearchRepository.exists(pharmacist.getId());
        assertThat(pharmacistExistsInEs).isFalse();

        // Validate the database is empty
        List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
        assertThat(pharmacistList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPharmacist() throws Exception {
        // Initialize the database
        pharmacistRepository.saveAndFlush(pharmacist);
        pharmacistSearchRepository.save(pharmacist);

        // Search the pharmacist
        restPharmacistMockMvc.perform(get("/api/_search/pharmacists?query=id:" + pharmacist.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pharmacist.getId().intValue())))
            .andExpect(jsonPath("$.[*].shopname").value(hasItem(DEFAULT_SHOPNAME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].shopaddress").value(hasItem(DEFAULT_SHOPADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phoneno").value(hasItem(DEFAULT_PHONENO.toString())))
            .andExpect(jsonPath("$.[*].licenceno").value(hasItem(DEFAULT_LICENCENO.toString())))
            .andExpect(jsonPath("$.[*].documentsContentType").value(hasItem(DEFAULT_DOCUMENTS_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].documents").value(hasItem(Base64Utils.encodeToString(DEFAULT_DOCUMENTS))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pharmacist.class);
        Pharmacist pharmacist1 = new Pharmacist();
        pharmacist1.setId(1L);
        Pharmacist pharmacist2 = new Pharmacist();
        pharmacist2.setId(pharmacist1.getId());
        assertThat(pharmacist1).isEqualTo(pharmacist2);
        pharmacist2.setId(2L);
        assertThat(pharmacist1).isNotEqualTo(pharmacist2);
        pharmacist1.setId(null);
        assertThat(pharmacist1).isNotEqualTo(pharmacist2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PharmacistDTO.class);
        PharmacistDTO pharmacistDTO1 = new PharmacistDTO();
        pharmacistDTO1.setId(1L);
        PharmacistDTO pharmacistDTO2 = new PharmacistDTO();
        assertThat(pharmacistDTO1).isNotEqualTo(pharmacistDTO2);
        pharmacistDTO2.setId(pharmacistDTO1.getId());
        assertThat(pharmacistDTO1).isEqualTo(pharmacistDTO2);
        pharmacistDTO2.setId(2L);
        assertThat(pharmacistDTO1).isNotEqualTo(pharmacistDTO2);
        pharmacistDTO1.setId(null);
        assertThat(pharmacistDTO1).isNotEqualTo(pharmacistDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pharmacistMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pharmacistMapper.fromId(null)).isNull();
    }
}
