package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.DoctorSchedule;
import com.narola.telemedicine.repository.DoctorScheduleRepository;
import com.narola.telemedicine.service.DoctorScheduleService;
import com.narola.telemedicine.repository.search.DoctorScheduleSearchRepository;
import com.narola.telemedicine.service.dto.DoctorScheduleDTO;
import com.narola.telemedicine.service.mapper.DoctorScheduleMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.narola.telemedicine.domain.enumeration.Days;
import com.narola.telemedicine.domain.enumeration.Schedule_Status;
/**
 * Test class for the DoctorScheduleResource REST controller.
 *
 * @see DoctorScheduleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class DoctorScheduleResourceIntTest {

    private static final Days DEFAULT_WORKINGDAY = Days.SUNDAY;
    private static final Days UPDATED_WORKINGDAY = Days.MONDAY;

    private static final String DEFAULT_FROMTIME = "AAAAAAAAAA";
    private static final String UPDATED_FROMTIME = "BBBBBBBBBB";

    private static final String DEFAULT_TOTIME = "AAAAAAAAAA";
    private static final String UPDATED_TOTIME = "BBBBBBBBBB";

    private static final Integer DEFAULT_NOOFPATIENT = 1;
    private static final Integer UPDATED_NOOFPATIENT = 2;

    private static final Schedule_Status DEFAULT_STATUS = Schedule_Status.ENABLE;
    private static final Schedule_Status UPDATED_STATUS = Schedule_Status.DISABLE;

    @Autowired
    private DoctorScheduleRepository doctorScheduleRepository;

    @Autowired
    private DoctorScheduleMapper doctorScheduleMapper;

    @Autowired
    private DoctorScheduleService doctorScheduleService;

    @Autowired
    private DoctorScheduleSearchRepository doctorScheduleSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDoctorScheduleMockMvc;

    private DoctorSchedule doctorSchedule;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DoctorScheduleResource doctorScheduleResource = new DoctorScheduleResource(doctorScheduleService);
        this.restDoctorScheduleMockMvc = MockMvcBuilders.standaloneSetup(doctorScheduleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DoctorSchedule createEntity(EntityManager em) {
        DoctorSchedule doctorSchedule = new DoctorSchedule()
            .workingday(DEFAULT_WORKINGDAY)
            .fromtime(DEFAULT_FROMTIME)
            .totime(DEFAULT_TOTIME)
            .noofpatient(DEFAULT_NOOFPATIENT)
            .status(DEFAULT_STATUS);
        return doctorSchedule;
    }

    @Before
    public void initTest() {
        doctorScheduleSearchRepository.deleteAll();
        doctorSchedule = createEntity(em);
    }

    @Test
    @Transactional
    public void createDoctorSchedule() throws Exception {
        int databaseSizeBeforeCreate = doctorScheduleRepository.findAll().size();

        // Create the DoctorSchedule
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);
        restDoctorScheduleMockMvc.perform(post("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isCreated());

        // Validate the DoctorSchedule in the database
        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeCreate + 1);
        DoctorSchedule testDoctorSchedule = doctorScheduleList.get(doctorScheduleList.size() - 1);
        assertThat(testDoctorSchedule.getWorkingday()).isEqualTo(DEFAULT_WORKINGDAY);
        assertThat(testDoctorSchedule.getFromtime()).isEqualTo(DEFAULT_FROMTIME);
        assertThat(testDoctorSchedule.getTotime()).isEqualTo(DEFAULT_TOTIME);
        assertThat(testDoctorSchedule.getNoofpatient()).isEqualTo(DEFAULT_NOOFPATIENT);
        assertThat(testDoctorSchedule.getStatus()).isEqualTo(DEFAULT_STATUS);

        // Validate the DoctorSchedule in Elasticsearch
        DoctorSchedule doctorScheduleEs = doctorScheduleSearchRepository.findOne(testDoctorSchedule.getId());
        assertThat(doctorScheduleEs).isEqualToIgnoringGivenFields(testDoctorSchedule);
    }

    @Test
    @Transactional
    public void createDoctorScheduleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = doctorScheduleRepository.findAll().size();

        // Create the DoctorSchedule with an existing ID
        doctorSchedule.setId(1L);
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDoctorScheduleMockMvc.perform(post("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DoctorSchedule in the database
        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkWorkingdayIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorScheduleRepository.findAll().size();
        // set the field null
        doctorSchedule.setWorkingday(null);

        // Create the DoctorSchedule, which fails.
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);

        restDoctorScheduleMockMvc.perform(post("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isBadRequest());

        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFromtimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorScheduleRepository.findAll().size();
        // set the field null
        doctorSchedule.setFromtime(null);

        // Create the DoctorSchedule, which fails.
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);

        restDoctorScheduleMockMvc.perform(post("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isBadRequest());

        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTotimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorScheduleRepository.findAll().size();
        // set the field null
        doctorSchedule.setTotime(null);

        // Create the DoctorSchedule, which fails.
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);

        restDoctorScheduleMockMvc.perform(post("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isBadRequest());

        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNoofpatientIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorScheduleRepository.findAll().size();
        // set the field null
        doctorSchedule.setNoofpatient(null);

        // Create the DoctorSchedule, which fails.
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);

        restDoctorScheduleMockMvc.perform(post("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isBadRequest());

        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorScheduleRepository.findAll().size();
        // set the field null
        doctorSchedule.setStatus(null);

        // Create the DoctorSchedule, which fails.
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);

        restDoctorScheduleMockMvc.perform(post("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isBadRequest());

        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDoctorSchedules() throws Exception {
        // Initialize the database
        doctorScheduleRepository.saveAndFlush(doctorSchedule);

        // Get all the doctorScheduleList
        restDoctorScheduleMockMvc.perform(get("/api/doctor-schedules?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doctorSchedule.getId().intValue())))
            .andExpect(jsonPath("$.[*].workingday").value(hasItem(DEFAULT_WORKINGDAY.toString())))
            .andExpect(jsonPath("$.[*].fromtime").value(hasItem(DEFAULT_FROMTIME.toString())))
            .andExpect(jsonPath("$.[*].totime").value(hasItem(DEFAULT_TOTIME.toString())))
            .andExpect(jsonPath("$.[*].noofpatient").value(hasItem(DEFAULT_NOOFPATIENT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getDoctorSchedule() throws Exception {
        // Initialize the database
        doctorScheduleRepository.saveAndFlush(doctorSchedule);

        // Get the doctorSchedule
        restDoctorScheduleMockMvc.perform(get("/api/doctor-schedules/{id}", doctorSchedule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(doctorSchedule.getId().intValue()))
            .andExpect(jsonPath("$.workingday").value(DEFAULT_WORKINGDAY.toString()))
            .andExpect(jsonPath("$.fromtime").value(DEFAULT_FROMTIME.toString()))
            .andExpect(jsonPath("$.totime").value(DEFAULT_TOTIME.toString()))
            .andExpect(jsonPath("$.noofpatient").value(DEFAULT_NOOFPATIENT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDoctorSchedule() throws Exception {
        // Get the doctorSchedule
        restDoctorScheduleMockMvc.perform(get("/api/doctor-schedules/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDoctorSchedule() throws Exception {
        // Initialize the database
        doctorScheduleRepository.saveAndFlush(doctorSchedule);
        doctorScheduleSearchRepository.save(doctorSchedule);
        int databaseSizeBeforeUpdate = doctorScheduleRepository.findAll().size();

        // Update the doctorSchedule
        DoctorSchedule updatedDoctorSchedule = doctorScheduleRepository.findOne(doctorSchedule.getId());
        // Disconnect from session so that the updates on updatedDoctorSchedule are not directly saved in db
        em.detach(updatedDoctorSchedule);
        updatedDoctorSchedule
            .workingday(UPDATED_WORKINGDAY)
            .fromtime(UPDATED_FROMTIME)
            .totime(UPDATED_TOTIME)
            .noofpatient(UPDATED_NOOFPATIENT)
            .status(UPDATED_STATUS);
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(updatedDoctorSchedule);

        restDoctorScheduleMockMvc.perform(put("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isOk());

        // Validate the DoctorSchedule in the database
        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeUpdate);
        DoctorSchedule testDoctorSchedule = doctorScheduleList.get(doctorScheduleList.size() - 1);
        assertThat(testDoctorSchedule.getWorkingday()).isEqualTo(UPDATED_WORKINGDAY);
        assertThat(testDoctorSchedule.getFromtime()).isEqualTo(UPDATED_FROMTIME);
        assertThat(testDoctorSchedule.getTotime()).isEqualTo(UPDATED_TOTIME);
        assertThat(testDoctorSchedule.getNoofpatient()).isEqualTo(UPDATED_NOOFPATIENT);
        assertThat(testDoctorSchedule.getStatus()).isEqualTo(UPDATED_STATUS);

        // Validate the DoctorSchedule in Elasticsearch
        DoctorSchedule doctorScheduleEs = doctorScheduleSearchRepository.findOne(testDoctorSchedule.getId());
        assertThat(doctorScheduleEs).isEqualToIgnoringGivenFields(testDoctorSchedule);
    }

    @Test
    @Transactional
    public void updateNonExistingDoctorSchedule() throws Exception {
        int databaseSizeBeforeUpdate = doctorScheduleRepository.findAll().size();

        // Create the DoctorSchedule
        DoctorScheduleDTO doctorScheduleDTO = doctorScheduleMapper.toDto(doctorSchedule);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDoctorScheduleMockMvc.perform(put("/api/doctor-schedules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorScheduleDTO)))
            .andExpect(status().isCreated());

        // Validate the DoctorSchedule in the database
        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDoctorSchedule() throws Exception {
        // Initialize the database
        doctorScheduleRepository.saveAndFlush(doctorSchedule);
        doctorScheduleSearchRepository.save(doctorSchedule);
        int databaseSizeBeforeDelete = doctorScheduleRepository.findAll().size();

        // Get the doctorSchedule
        restDoctorScheduleMockMvc.perform(delete("/api/doctor-schedules/{id}", doctorSchedule.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean doctorScheduleExistsInEs = doctorScheduleSearchRepository.exists(doctorSchedule.getId());
        assertThat(doctorScheduleExistsInEs).isFalse();

        // Validate the database is empty
        List<DoctorSchedule> doctorScheduleList = doctorScheduleRepository.findAll();
        assertThat(doctorScheduleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDoctorSchedule() throws Exception {
        // Initialize the database
        doctorScheduleRepository.saveAndFlush(doctorSchedule);
        doctorScheduleSearchRepository.save(doctorSchedule);

        // Search the doctorSchedule
        restDoctorScheduleMockMvc.perform(get("/api/_search/doctor-schedules?query=id:" + doctorSchedule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doctorSchedule.getId().intValue())))
            .andExpect(jsonPath("$.[*].workingday").value(hasItem(DEFAULT_WORKINGDAY.toString())))
            .andExpect(jsonPath("$.[*].fromtime").value(hasItem(DEFAULT_FROMTIME.toString())))
            .andExpect(jsonPath("$.[*].totime").value(hasItem(DEFAULT_TOTIME.toString())))
            .andExpect(jsonPath("$.[*].noofpatient").value(hasItem(DEFAULT_NOOFPATIENT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DoctorSchedule.class);
        DoctorSchedule doctorSchedule1 = new DoctorSchedule();
        doctorSchedule1.setId(1L);
        DoctorSchedule doctorSchedule2 = new DoctorSchedule();
        doctorSchedule2.setId(doctorSchedule1.getId());
        assertThat(doctorSchedule1).isEqualTo(doctorSchedule2);
        doctorSchedule2.setId(2L);
        assertThat(doctorSchedule1).isNotEqualTo(doctorSchedule2);
        doctorSchedule1.setId(null);
        assertThat(doctorSchedule1).isNotEqualTo(doctorSchedule2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DoctorScheduleDTO.class);
        DoctorScheduleDTO doctorScheduleDTO1 = new DoctorScheduleDTO();
        doctorScheduleDTO1.setId(1L);
        DoctorScheduleDTO doctorScheduleDTO2 = new DoctorScheduleDTO();
        assertThat(doctorScheduleDTO1).isNotEqualTo(doctorScheduleDTO2);
        doctorScheduleDTO2.setId(doctorScheduleDTO1.getId());
        assertThat(doctorScheduleDTO1).isEqualTo(doctorScheduleDTO2);
        doctorScheduleDTO2.setId(2L);
        assertThat(doctorScheduleDTO1).isNotEqualTo(doctorScheduleDTO2);
        doctorScheduleDTO1.setId(null);
        assertThat(doctorScheduleDTO1).isNotEqualTo(doctorScheduleDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(doctorScheduleMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(doctorScheduleMapper.fromId(null)).isNull();
    }
}
