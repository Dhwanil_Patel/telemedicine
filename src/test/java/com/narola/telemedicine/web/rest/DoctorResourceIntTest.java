package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.Doctor;
import com.narola.telemedicine.repository.DoctorRepository;
import com.narola.telemedicine.service.DoctorService;
import com.narola.telemedicine.repository.search.DoctorSearchRepository;
import com.narola.telemedicine.service.dto.DoctorDTO;
import com.narola.telemedicine.service.mapper.DoctorMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.narola.telemedicine.domain.enumeration.Process_Status;
import com.narola.telemedicine.domain.enumeration.Gender;
/**
 * Test class for the DoctorResource REST controller.
 *
 * @see DoctorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class DoctorResourceIntTest {

    private static final String DEFAULT_FIRSTNAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRSTNAME = "BBBBBBBBBB";

    private static final String DEFAULT_LASTNAME = "AAAAAAAAAA";
    private static final String UPDATED_LASTNAME = "BBBBBBBBBB";

    private static final Process_Status DEFAULT_STATUS = Process_Status.ACTIVE;
    private static final Process_Status UPDATED_STATUS = Process_Status.DEACTIVE;

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final LocalDate DEFAULT_DATEOFBIRTH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATEOFBIRTH = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILENO = "AAAAAAAAAA";
    private static final String UPDATED_MOBILENO = "BBBBBBBBBB";

    private static final String DEFAULT_BLOODGROUP = "AAAAAAAAAA";
    private static final String UPDATED_BLOODGROUP = "BBBBBBBBBB";

    private static final Float DEFAULT_EXPRIENCE = 1F;
    private static final Float UPDATED_EXPRIENCE = 2F;

    private static final String DEFAULT_LICENCENO = "AAAAAAAAAA";
    private static final String UPDATED_LICENCENO = "BBBBBBBBBB";

    private static final byte[] DEFAULT_DOCUMENTS = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_DOCUMENTS = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_DOCUMENTS_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_DOCUMENTS_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_GOOGLEPLUSID = "AAAAAAAAAA";
    private static final String UPDATED_GOOGLEPLUSID = "BBBBBBBBBB";

    private static final String DEFAULT_FACEBOOKID = "AAAAAAAAAA";
    private static final String UPDATED_FACEBOOKID = "BBBBBBBBBB";

    private static final String DEFAULT_TWITTERID = "AAAAAAAAAA";
    private static final String UPDATED_TWITTERID = "BBBBBBBBBB";

    private static final Integer DEFAULT_CONSULTATIONCHARGE = 1;
    private static final Integer UPDATED_CONSULTATIONCHARGE = 2;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private DoctorSearchRepository doctorSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDoctorMockMvc;

    private Doctor doctor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DoctorResource doctorResource = new DoctorResource(doctorService); //, degreeService, specializationService
        this.restDoctorMockMvc = MockMvcBuilders.standaloneSetup(doctorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Doctor createEntity(EntityManager em) {
        Doctor doctor = new Doctor()
            .firstname(DEFAULT_FIRSTNAME)
            .lastname(DEFAULT_LASTNAME)
            .status(DEFAULT_STATUS)
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .gender(DEFAULT_GENDER)
            .dateofbirth(DEFAULT_DATEOFBIRTH)
            .address(DEFAULT_ADDRESS)
            .mobileno(DEFAULT_MOBILENO)
            .bloodgroup(DEFAULT_BLOODGROUP)
            .exprience(DEFAULT_EXPRIENCE)
            .licenceno(DEFAULT_LICENCENO)
            .documents(DEFAULT_DOCUMENTS)
            .documentsContentType(DEFAULT_DOCUMENTS_CONTENT_TYPE)
            .googleplusid(DEFAULT_GOOGLEPLUSID)
            .facebookid(DEFAULT_FACEBOOKID)
            .twitterid(DEFAULT_TWITTERID)
            .consultationcharge(DEFAULT_CONSULTATIONCHARGE);
        return doctor;
    }

    @Before
    public void initTest() {
        doctorSearchRepository.deleteAll();
        doctor = createEntity(em);
    }

    @Test
    @Transactional
    public void createDoctor() throws Exception {
        int databaseSizeBeforeCreate = doctorRepository.findAll().size();

        // Create the Doctor
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);
        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isCreated());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeCreate + 1);
        Doctor testDoctor = doctorList.get(doctorList.size() - 1);
        assertThat(testDoctor.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testDoctor.getLastname()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(testDoctor.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDoctor.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testDoctor.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testDoctor.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testDoctor.getDateofbirth()).isEqualTo(DEFAULT_DATEOFBIRTH);
        assertThat(testDoctor.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testDoctor.getMobileno()).isEqualTo(DEFAULT_MOBILENO);
        assertThat(testDoctor.getBloodgroup()).isEqualTo(DEFAULT_BLOODGROUP);
        assertThat(testDoctor.getExprience()).isEqualTo(DEFAULT_EXPRIENCE);
        assertThat(testDoctor.getLicenceno()).isEqualTo(DEFAULT_LICENCENO);
        assertThat(testDoctor.getDocuments()).isEqualTo(DEFAULT_DOCUMENTS);
        assertThat(testDoctor.getDocumentsContentType()).isEqualTo(DEFAULT_DOCUMENTS_CONTENT_TYPE);
        assertThat(testDoctor.getGoogleplusid()).isEqualTo(DEFAULT_GOOGLEPLUSID);
        assertThat(testDoctor.getFacebookid()).isEqualTo(DEFAULT_FACEBOOKID);
        assertThat(testDoctor.getTwitterid()).isEqualTo(DEFAULT_TWITTERID);
        assertThat(testDoctor.getConsultationcharge()).isEqualTo(DEFAULT_CONSULTATIONCHARGE);

        // Validate the Doctor in Elasticsearch
        Doctor doctorEs = doctorSearchRepository.findOne(testDoctor.getId());
        assertThat(doctorEs).isEqualToIgnoringGivenFields(testDoctor);
    }

    @Test
    @Transactional
    public void createDoctorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = doctorRepository.findAll().size();

        // Create the Doctor with an existing ID
        doctor.setId(1L);
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFirstnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setFirstname(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setLastname(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setGender(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setAddress(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMobilenoIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setMobileno(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkExprienceIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setExprience(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLicencenoIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setLicenceno(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDocumentsIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setDocuments(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConsultationchargeIsRequired() throws Exception {
        int databaseSizeBeforeTest = doctorRepository.findAll().size();
        // set the field null
        doctor.setConsultationcharge(null);

        // Create the Doctor, which fails.
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isBadRequest());

        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDoctors() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);

        // Get all the doctorList
        restDoctorMockMvc.perform(get("/api/doctors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doctor.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME.toString())))
            .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].dateofbirth").value(hasItem(DEFAULT_DATEOFBIRTH.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].mobileno").value(hasItem(DEFAULT_MOBILENO.toString())))
            .andExpect(jsonPath("$.[*].bloodgroup").value(hasItem(DEFAULT_BLOODGROUP.toString())))
            .andExpect(jsonPath("$.[*].exprience").value(hasItem(DEFAULT_EXPRIENCE.doubleValue())))
            .andExpect(jsonPath("$.[*].licenceno").value(hasItem(DEFAULT_LICENCENO.toString())))
            .andExpect(jsonPath("$.[*].documentsContentType").value(hasItem(DEFAULT_DOCUMENTS_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].documents").value(hasItem(Base64Utils.encodeToString(DEFAULT_DOCUMENTS))))
            .andExpect(jsonPath("$.[*].googleplusid").value(hasItem(DEFAULT_GOOGLEPLUSID.toString())))
            .andExpect(jsonPath("$.[*].facebookid").value(hasItem(DEFAULT_FACEBOOKID.toString())))
            .andExpect(jsonPath("$.[*].twitterid").value(hasItem(DEFAULT_TWITTERID.toString())))
            .andExpect(jsonPath("$.[*].consultationcharge").value(hasItem(DEFAULT_CONSULTATIONCHARGE)));
    }

    @Test
    @Transactional
    public void getDoctor() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);

        // Get the doctor
        restDoctorMockMvc.perform(get("/api/doctors/{id}", doctor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(doctor.getId().intValue()))
            .andExpect(jsonPath("$.firstname").value(DEFAULT_FIRSTNAME.toString()))
            .andExpect(jsonPath("$.lastname").value(DEFAULT_LASTNAME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.dateofbirth").value(DEFAULT_DATEOFBIRTH.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.mobileno").value(DEFAULT_MOBILENO.toString()))
            .andExpect(jsonPath("$.bloodgroup").value(DEFAULT_BLOODGROUP.toString()))
            .andExpect(jsonPath("$.exprience").value(DEFAULT_EXPRIENCE.doubleValue()))
            .andExpect(jsonPath("$.licenceno").value(DEFAULT_LICENCENO.toString()))
            .andExpect(jsonPath("$.documentsContentType").value(DEFAULT_DOCUMENTS_CONTENT_TYPE))
            .andExpect(jsonPath("$.documents").value(Base64Utils.encodeToString(DEFAULT_DOCUMENTS)))
            .andExpect(jsonPath("$.googleplusid").value(DEFAULT_GOOGLEPLUSID.toString()))
            .andExpect(jsonPath("$.facebookid").value(DEFAULT_FACEBOOKID.toString()))
            .andExpect(jsonPath("$.twitterid").value(DEFAULT_TWITTERID.toString()))
            .andExpect(jsonPath("$.consultationcharge").value(DEFAULT_CONSULTATIONCHARGE));
    }

    @Test
    @Transactional
    public void getNonExistingDoctor() throws Exception {
        // Get the doctor
        restDoctorMockMvc.perform(get("/api/doctors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDoctor() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);
        doctorSearchRepository.save(doctor);
        int databaseSizeBeforeUpdate = doctorRepository.findAll().size();

        // Update the doctor
        Doctor updatedDoctor = doctorRepository.findOne(doctor.getId());
        // Disconnect from session so that the updates on updatedDoctor are not directly saved in db
        em.detach(updatedDoctor);
        updatedDoctor
            .firstname(UPDATED_FIRSTNAME)
            .lastname(UPDATED_LASTNAME)
            .status(UPDATED_STATUS)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .gender(UPDATED_GENDER)
            .dateofbirth(UPDATED_DATEOFBIRTH)
            .address(UPDATED_ADDRESS)
            .mobileno(UPDATED_MOBILENO)
            .bloodgroup(UPDATED_BLOODGROUP)
            .exprience(UPDATED_EXPRIENCE)
            .licenceno(UPDATED_LICENCENO)
            .documents(UPDATED_DOCUMENTS)
            .documentsContentType(UPDATED_DOCUMENTS_CONTENT_TYPE)
            .googleplusid(UPDATED_GOOGLEPLUSID)
            .facebookid(UPDATED_FACEBOOKID)
            .twitterid(UPDATED_TWITTERID)
            .consultationcharge(UPDATED_CONSULTATIONCHARGE);
        DoctorDTO doctorDTO = doctorMapper.toDto(updatedDoctor);

        restDoctorMockMvc.perform(put("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isOk());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeUpdate);
        Doctor testDoctor = doctorList.get(doctorList.size() - 1);
        assertThat(testDoctor.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testDoctor.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testDoctor.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDoctor.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testDoctor.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testDoctor.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testDoctor.getDateofbirth()).isEqualTo(UPDATED_DATEOFBIRTH);
        assertThat(testDoctor.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testDoctor.getMobileno()).isEqualTo(UPDATED_MOBILENO);
        assertThat(testDoctor.getBloodgroup()).isEqualTo(UPDATED_BLOODGROUP);
        assertThat(testDoctor.getExprience()).isEqualTo(UPDATED_EXPRIENCE);
        assertThat(testDoctor.getLicenceno()).isEqualTo(UPDATED_LICENCENO);
        assertThat(testDoctor.getDocuments()).isEqualTo(UPDATED_DOCUMENTS);
        assertThat(testDoctor.getDocumentsContentType()).isEqualTo(UPDATED_DOCUMENTS_CONTENT_TYPE);
        assertThat(testDoctor.getGoogleplusid()).isEqualTo(UPDATED_GOOGLEPLUSID);
        assertThat(testDoctor.getFacebookid()).isEqualTo(UPDATED_FACEBOOKID);
        assertThat(testDoctor.getTwitterid()).isEqualTo(UPDATED_TWITTERID);
        assertThat(testDoctor.getConsultationcharge()).isEqualTo(UPDATED_CONSULTATIONCHARGE);

        // Validate the Doctor in Elasticsearch
        Doctor doctorEs = doctorSearchRepository.findOne(testDoctor.getId());
        assertThat(doctorEs).isEqualToIgnoringGivenFields(testDoctor);
    }

    @Test
    @Transactional
    public void updateNonExistingDoctor() throws Exception {
        int databaseSizeBeforeUpdate = doctorRepository.findAll().size();

        // Create the Doctor
        DoctorDTO doctorDTO = doctorMapper.toDto(doctor);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDoctorMockMvc.perform(put("/api/doctors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doctorDTO)))
            .andExpect(status().isCreated());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDoctor() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);
        doctorSearchRepository.save(doctor);
        int databaseSizeBeforeDelete = doctorRepository.findAll().size();

        // Get the doctor
        restDoctorMockMvc.perform(delete("/api/doctors/{id}", doctor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean doctorExistsInEs = doctorSearchRepository.exists(doctor.getId());
        assertThat(doctorExistsInEs).isFalse();

        // Validate the database is empty
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDoctor() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);
        doctorSearchRepository.save(doctor);

        // Search the doctor
        restDoctorMockMvc.perform(get("/api/_search/doctors?query=id:" + doctor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doctor.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME.toString())))
            .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].dateofbirth").value(hasItem(DEFAULT_DATEOFBIRTH.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].mobileno").value(hasItem(DEFAULT_MOBILENO.toString())))
            .andExpect(jsonPath("$.[*].bloodgroup").value(hasItem(DEFAULT_BLOODGROUP.toString())))
            .andExpect(jsonPath("$.[*].exprience").value(hasItem(DEFAULT_EXPRIENCE.doubleValue())))
            .andExpect(jsonPath("$.[*].licenceno").value(hasItem(DEFAULT_LICENCENO.toString())))
            .andExpect(jsonPath("$.[*].documentsContentType").value(hasItem(DEFAULT_DOCUMENTS_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].documents").value(hasItem(Base64Utils.encodeToString(DEFAULT_DOCUMENTS))))
            .andExpect(jsonPath("$.[*].googleplusid").value(hasItem(DEFAULT_GOOGLEPLUSID.toString())))
            .andExpect(jsonPath("$.[*].facebookid").value(hasItem(DEFAULT_FACEBOOKID.toString())))
            .andExpect(jsonPath("$.[*].twitterid").value(hasItem(DEFAULT_TWITTERID.toString())))
            .andExpect(jsonPath("$.[*].consultationcharge").value(hasItem(DEFAULT_CONSULTATIONCHARGE)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Doctor.class);
        Doctor doctor1 = new Doctor();
        doctor1.setId(1L);
        Doctor doctor2 = new Doctor();
        doctor2.setId(doctor1.getId());
        assertThat(doctor1).isEqualTo(doctor2);
        doctor2.setId(2L);
        assertThat(doctor1).isNotEqualTo(doctor2);
        doctor1.setId(null);
        assertThat(doctor1).isNotEqualTo(doctor2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DoctorDTO.class);
        DoctorDTO doctorDTO1 = new DoctorDTO();
        doctorDTO1.setId(1L);
        DoctorDTO doctorDTO2 = new DoctorDTO();
        assertThat(doctorDTO1).isNotEqualTo(doctorDTO2);
        doctorDTO2.setId(doctorDTO1.getId());
        assertThat(doctorDTO1).isEqualTo(doctorDTO2);
        doctorDTO2.setId(2L);
        assertThat(doctorDTO1).isNotEqualTo(doctorDTO2);
        doctorDTO1.setId(null);
        assertThat(doctorDTO1).isNotEqualTo(doctorDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(doctorMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(doctorMapper.fromId(null)).isNull();
    }
}
