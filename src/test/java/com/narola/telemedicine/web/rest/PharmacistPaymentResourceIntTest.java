package com.narola.telemedicine.web.rest;

import com.narola.telemedicine.TelemedicineApplicationApp;

import com.narola.telemedicine.domain.PharmacistPayment;
import com.narola.telemedicine.repository.PharmacistPaymentRepository;
import com.narola.telemedicine.service.PharmacistPaymentService;
import com.narola.telemedicine.repository.search.PharmacistPaymentSearchRepository;
import com.narola.telemedicine.service.dto.PharmacistPaymentDTO;
import com.narola.telemedicine.service.mapper.PharmacistPaymentMapper;
import com.narola.telemedicine.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.narola.telemedicine.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.narola.telemedicine.domain.enumeration.Payment_Status;
/**
 * Test class for the PharmacistPaymentResource REST controller.
 *
 * @see PharmacistPaymentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TelemedicineApplicationApp.class)
public class PharmacistPaymentResourceIntTest {

    private static final LocalDate DEFAULT_PAYMENT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PAYMENT_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_TOTALAMOUNT = 1;
    private static final Integer UPDATED_TOTALAMOUNT = 2;

    private static final String DEFAULT_PAYMENTMODE = "AAAAAAAAAA";
    private static final String UPDATED_PAYMENTMODE = "BBBBBBBBBB";

    private static final Payment_Status DEFAULT_PAYMENT_STATUS = Payment_Status.PAID;
    private static final Payment_Status UPDATED_PAYMENT_STATUS = Payment_Status.UNPAID;

    private static final String DEFAULT_TRANSCATIONID = "AAAAAAAAAA";
    private static final String UPDATED_TRANSCATIONID = "BBBBBBBBBB";

    @Autowired
    private PharmacistPaymentRepository pharmacistPaymentRepository;

    @Autowired
    private PharmacistPaymentMapper pharmacistPaymentMapper;

    @Autowired
    private PharmacistPaymentService pharmacistPaymentService;

    @Autowired
    private PharmacistPaymentSearchRepository pharmacistPaymentSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPharmacistPaymentMockMvc;

    private PharmacistPayment pharmacistPayment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PharmacistPaymentResource pharmacistPaymentResource = new PharmacistPaymentResource(pharmacistPaymentService);
        this.restPharmacistPaymentMockMvc = MockMvcBuilders.standaloneSetup(pharmacistPaymentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PharmacistPayment createEntity(EntityManager em) {
        PharmacistPayment pharmacistPayment = new PharmacistPayment()
            .paymentDate(DEFAULT_PAYMENT_DATE)
            .totalamount(DEFAULT_TOTALAMOUNT)
            .paymentmode(DEFAULT_PAYMENTMODE)
            .paymentStatus(DEFAULT_PAYMENT_STATUS)
            .transcationid(DEFAULT_TRANSCATIONID);
        return pharmacistPayment;
    }

    @Before
    public void initTest() {
        pharmacistPaymentSearchRepository.deleteAll();
        pharmacistPayment = createEntity(em);
    }

    @Test
    @Transactional
    public void createPharmacistPayment() throws Exception {
        int databaseSizeBeforeCreate = pharmacistPaymentRepository.findAll().size();

        // Create the PharmacistPayment
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);
        restPharmacistPaymentMockMvc.perform(post("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isCreated());

        // Validate the PharmacistPayment in the database
        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeCreate + 1);
        PharmacistPayment testPharmacistPayment = pharmacistPaymentList.get(pharmacistPaymentList.size() - 1);
        assertThat(testPharmacistPayment.getPaymentDate()).isEqualTo(DEFAULT_PAYMENT_DATE);
        assertThat(testPharmacistPayment.getTotalamount()).isEqualTo(DEFAULT_TOTALAMOUNT);
        assertThat(testPharmacistPayment.getPaymentmode()).isEqualTo(DEFAULT_PAYMENTMODE);
        assertThat(testPharmacistPayment.getPaymentStatus()).isEqualTo(DEFAULT_PAYMENT_STATUS);
        assertThat(testPharmacistPayment.getTranscationid()).isEqualTo(DEFAULT_TRANSCATIONID);

        // Validate the PharmacistPayment in Elasticsearch
        PharmacistPayment pharmacistPaymentEs = pharmacistPaymentSearchRepository.findOne(testPharmacistPayment.getId());
        assertThat(pharmacistPaymentEs).isEqualToIgnoringGivenFields(testPharmacistPayment);
    }

    @Test
    @Transactional
    public void createPharmacistPaymentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pharmacistPaymentRepository.findAll().size();

        // Create the PharmacistPayment with an existing ID
        pharmacistPayment.setId(1L);
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPharmacistPaymentMockMvc.perform(post("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PharmacistPayment in the database
        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPaymentDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistPaymentRepository.findAll().size();
        // set the field null
        pharmacistPayment.setPaymentDate(null);

        // Create the PharmacistPayment, which fails.
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);

        restPharmacistPaymentMockMvc.perform(post("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isBadRequest());

        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTotalamountIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistPaymentRepository.findAll().size();
        // set the field null
        pharmacistPayment.setTotalamount(null);

        // Create the PharmacistPayment, which fails.
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);

        restPharmacistPaymentMockMvc.perform(post("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isBadRequest());

        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaymentmodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistPaymentRepository.findAll().size();
        // set the field null
        pharmacistPayment.setPaymentmode(null);

        // Create the PharmacistPayment, which fails.
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);

        restPharmacistPaymentMockMvc.perform(post("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isBadRequest());

        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaymentStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistPaymentRepository.findAll().size();
        // set the field null
        pharmacistPayment.setPaymentStatus(null);

        // Create the PharmacistPayment, which fails.
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);

        restPharmacistPaymentMockMvc.perform(post("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isBadRequest());

        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTranscationidIsRequired() throws Exception {
        int databaseSizeBeforeTest = pharmacistPaymentRepository.findAll().size();
        // set the field null
        pharmacistPayment.setTranscationid(null);

        // Create the PharmacistPayment, which fails.
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);

        restPharmacistPaymentMockMvc.perform(post("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isBadRequest());

        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPharmacistPayments() throws Exception {
        // Initialize the database
        pharmacistPaymentRepository.saveAndFlush(pharmacistPayment);

        // Get all the pharmacistPaymentList
        restPharmacistPaymentMockMvc.perform(get("/api/pharmacist-payments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pharmacistPayment.getId().intValue())))
            .andExpect(jsonPath("$.[*].paymentDate").value(hasItem(DEFAULT_PAYMENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalamount").value(hasItem(DEFAULT_TOTALAMOUNT)))
            .andExpect(jsonPath("$.[*].paymentmode").value(hasItem(DEFAULT_PAYMENTMODE.toString())))
            .andExpect(jsonPath("$.[*].paymentStatus").value(hasItem(DEFAULT_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].transcationid").value(hasItem(DEFAULT_TRANSCATIONID.toString())));
    }

    @Test
    @Transactional
    public void getPharmacistPayment() throws Exception {
        // Initialize the database
        pharmacistPaymentRepository.saveAndFlush(pharmacistPayment);

        // Get the pharmacistPayment
        restPharmacistPaymentMockMvc.perform(get("/api/pharmacist-payments/{id}", pharmacistPayment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pharmacistPayment.getId().intValue()))
            .andExpect(jsonPath("$.paymentDate").value(DEFAULT_PAYMENT_DATE.toString()))
            .andExpect(jsonPath("$.totalamount").value(DEFAULT_TOTALAMOUNT))
            .andExpect(jsonPath("$.paymentmode").value(DEFAULT_PAYMENTMODE.toString()))
            .andExpect(jsonPath("$.paymentStatus").value(DEFAULT_PAYMENT_STATUS.toString()))
            .andExpect(jsonPath("$.transcationid").value(DEFAULT_TRANSCATIONID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPharmacistPayment() throws Exception {
        // Get the pharmacistPayment
        restPharmacistPaymentMockMvc.perform(get("/api/pharmacist-payments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePharmacistPayment() throws Exception {
        // Initialize the database
        pharmacistPaymentRepository.saveAndFlush(pharmacistPayment);
        pharmacistPaymentSearchRepository.save(pharmacistPayment);
        int databaseSizeBeforeUpdate = pharmacistPaymentRepository.findAll().size();

        // Update the pharmacistPayment
        PharmacistPayment updatedPharmacistPayment = pharmacistPaymentRepository.findOne(pharmacistPayment.getId());
        // Disconnect from session so that the updates on updatedPharmacistPayment are not directly saved in db
        em.detach(updatedPharmacistPayment);
        updatedPharmacistPayment
            .paymentDate(UPDATED_PAYMENT_DATE)
            .totalamount(UPDATED_TOTALAMOUNT)
            .paymentmode(UPDATED_PAYMENTMODE)
            .paymentStatus(UPDATED_PAYMENT_STATUS)
            .transcationid(UPDATED_TRANSCATIONID);
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(updatedPharmacistPayment);

        restPharmacistPaymentMockMvc.perform(put("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isOk());

        // Validate the PharmacistPayment in the database
        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeUpdate);
        PharmacistPayment testPharmacistPayment = pharmacistPaymentList.get(pharmacistPaymentList.size() - 1);
        assertThat(testPharmacistPayment.getPaymentDate()).isEqualTo(UPDATED_PAYMENT_DATE);
        assertThat(testPharmacistPayment.getTotalamount()).isEqualTo(UPDATED_TOTALAMOUNT);
        assertThat(testPharmacistPayment.getPaymentmode()).isEqualTo(UPDATED_PAYMENTMODE);
        assertThat(testPharmacistPayment.getPaymentStatus()).isEqualTo(UPDATED_PAYMENT_STATUS);
        assertThat(testPharmacistPayment.getTranscationid()).isEqualTo(UPDATED_TRANSCATIONID);

        // Validate the PharmacistPayment in Elasticsearch
        PharmacistPayment pharmacistPaymentEs = pharmacistPaymentSearchRepository.findOne(testPharmacistPayment.getId());
        assertThat(pharmacistPaymentEs).isEqualToIgnoringGivenFields(testPharmacistPayment);
    }

    @Test
    @Transactional
    public void updateNonExistingPharmacistPayment() throws Exception {
        int databaseSizeBeforeUpdate = pharmacistPaymentRepository.findAll().size();

        // Create the PharmacistPayment
        PharmacistPaymentDTO pharmacistPaymentDTO = pharmacistPaymentMapper.toDto(pharmacistPayment);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPharmacistPaymentMockMvc.perform(put("/api/pharmacist-payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pharmacistPaymentDTO)))
            .andExpect(status().isCreated());

        // Validate the PharmacistPayment in the database
        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePharmacistPayment() throws Exception {
        // Initialize the database
        pharmacistPaymentRepository.saveAndFlush(pharmacistPayment);
        pharmacistPaymentSearchRepository.save(pharmacistPayment);
        int databaseSizeBeforeDelete = pharmacistPaymentRepository.findAll().size();

        // Get the pharmacistPayment
        restPharmacistPaymentMockMvc.perform(delete("/api/pharmacist-payments/{id}", pharmacistPayment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean pharmacistPaymentExistsInEs = pharmacistPaymentSearchRepository.exists(pharmacistPayment.getId());
        assertThat(pharmacistPaymentExistsInEs).isFalse();

        // Validate the database is empty
        List<PharmacistPayment> pharmacistPaymentList = pharmacistPaymentRepository.findAll();
        assertThat(pharmacistPaymentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPharmacistPayment() throws Exception {
        // Initialize the database
        pharmacistPaymentRepository.saveAndFlush(pharmacistPayment);
        pharmacistPaymentSearchRepository.save(pharmacistPayment);

        // Search the pharmacistPayment
        restPharmacistPaymentMockMvc.perform(get("/api/_search/pharmacist-payments?query=id:" + pharmacistPayment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pharmacistPayment.getId().intValue())))
            .andExpect(jsonPath("$.[*].paymentDate").value(hasItem(DEFAULT_PAYMENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalamount").value(hasItem(DEFAULT_TOTALAMOUNT)))
            .andExpect(jsonPath("$.[*].paymentmode").value(hasItem(DEFAULT_PAYMENTMODE.toString())))
            .andExpect(jsonPath("$.[*].paymentStatus").value(hasItem(DEFAULT_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].transcationid").value(hasItem(DEFAULT_TRANSCATIONID.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PharmacistPayment.class);
        PharmacistPayment pharmacistPayment1 = new PharmacistPayment();
        pharmacistPayment1.setId(1L);
        PharmacistPayment pharmacistPayment2 = new PharmacistPayment();
        pharmacistPayment2.setId(pharmacistPayment1.getId());
        assertThat(pharmacistPayment1).isEqualTo(pharmacistPayment2);
        pharmacistPayment2.setId(2L);
        assertThat(pharmacistPayment1).isNotEqualTo(pharmacistPayment2);
        pharmacistPayment1.setId(null);
        assertThat(pharmacistPayment1).isNotEqualTo(pharmacistPayment2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PharmacistPaymentDTO.class);
        PharmacistPaymentDTO pharmacistPaymentDTO1 = new PharmacistPaymentDTO();
        pharmacistPaymentDTO1.setId(1L);
        PharmacistPaymentDTO pharmacistPaymentDTO2 = new PharmacistPaymentDTO();
        assertThat(pharmacistPaymentDTO1).isNotEqualTo(pharmacistPaymentDTO2);
        pharmacistPaymentDTO2.setId(pharmacistPaymentDTO1.getId());
        assertThat(pharmacistPaymentDTO1).isEqualTo(pharmacistPaymentDTO2);
        pharmacistPaymentDTO2.setId(2L);
        assertThat(pharmacistPaymentDTO1).isNotEqualTo(pharmacistPaymentDTO2);
        pharmacistPaymentDTO1.setId(null);
        assertThat(pharmacistPaymentDTO1).isNotEqualTo(pharmacistPaymentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pharmacistPaymentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pharmacistPaymentMapper.fromId(null)).isNull();
    }
}
