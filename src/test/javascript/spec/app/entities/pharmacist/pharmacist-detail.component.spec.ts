/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PharmacistDetailComponent } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist-detail.component';
import { PharmacistService } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist.service';
import { Pharmacist } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist.model';

describe('Component Tests', () => {

    describe('Pharmacist Management Detail Component', () => {
        let comp: PharmacistDetailComponent;
        let fixture: ComponentFixture<PharmacistDetailComponent>;
        let service: PharmacistService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PharmacistDetailComponent],
                providers: [
                    PharmacistService
                ]
            })
            .overrideTemplate(PharmacistDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PharmacistDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PharmacistService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Pharmacist(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.pharmacist).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
