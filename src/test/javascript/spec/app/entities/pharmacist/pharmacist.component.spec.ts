/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PharmacistComponent } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist.component';
import { PharmacistService } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist.service';
import { Pharmacist } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist.model';

describe('Component Tests', () => {

    describe('Pharmacist Management Component', () => {
        let comp: PharmacistComponent;
        let fixture: ComponentFixture<PharmacistComponent>;
        let service: PharmacistService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PharmacistComponent],
                providers: [
                    PharmacistService
                ]
            })
            .overrideTemplate(PharmacistComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PharmacistComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PharmacistService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Pharmacist(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.pharmacists[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
