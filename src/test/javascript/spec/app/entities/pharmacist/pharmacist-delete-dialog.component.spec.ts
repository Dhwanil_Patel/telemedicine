/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PharmacistDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist-delete-dialog.component';
import { PharmacistService } from '../../../../../../main/webapp/app/entities/pharmacist/pharmacist.service';

describe('Component Tests', () => {

    describe('Pharmacist Management Delete Component', () => {
        let comp: PharmacistDeleteDialogComponent;
        let fixture: ComponentFixture<PharmacistDeleteDialogComponent>;
        let service: PharmacistService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PharmacistDeleteDialogComponent],
                providers: [
                    PharmacistService
                ]
            })
            .overrideTemplate(PharmacistDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PharmacistDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PharmacistService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
