/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PharmacistPaymentDetailComponent } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment-detail.component';
import { PharmacistPaymentService } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.service';
import { PharmacistPayment } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.model';

describe('Component Tests', () => {

    describe('PharmacistPayment Management Detail Component', () => {
        let comp: PharmacistPaymentDetailComponent;
        let fixture: ComponentFixture<PharmacistPaymentDetailComponent>;
        let service: PharmacistPaymentService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PharmacistPaymentDetailComponent],
                providers: [
                    PharmacistPaymentService
                ]
            })
            .overrideTemplate(PharmacistPaymentDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PharmacistPaymentDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PharmacistPaymentService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new PharmacistPayment(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.pharmacistPayment).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
