/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PharmacistPaymentDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment-delete-dialog.component';
import { PharmacistPaymentService } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.service';

describe('Component Tests', () => {

    describe('PharmacistPayment Management Delete Component', () => {
        let comp: PharmacistPaymentDeleteDialogComponent;
        let fixture: ComponentFixture<PharmacistPaymentDeleteDialogComponent>;
        let service: PharmacistPaymentService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PharmacistPaymentDeleteDialogComponent],
                providers: [
                    PharmacistPaymentService
                ]
            })
            .overrideTemplate(PharmacistPaymentDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PharmacistPaymentDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PharmacistPaymentService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
