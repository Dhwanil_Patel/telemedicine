/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PharmacistPaymentComponent } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.component';
import { PharmacistPaymentService } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.service';
import { PharmacistPayment } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.model';

describe('Component Tests', () => {

    describe('PharmacistPayment Management Component', () => {
        let comp: PharmacistPaymentComponent;
        let fixture: ComponentFixture<PharmacistPaymentComponent>;
        let service: PharmacistPaymentService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PharmacistPaymentComponent],
                providers: [
                    PharmacistPaymentService
                ]
            })
            .overrideTemplate(PharmacistPaymentComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PharmacistPaymentComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PharmacistPaymentService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new PharmacistPayment(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.pharmacistPayments[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
