/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PharmacistPaymentDialogComponent } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment-dialog.component';
import { PharmacistPaymentService } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.service';
import { PharmacistPayment } from '../../../../../../main/webapp/app/entities/pharmacist-payment/pharmacist-payment.model';
import { OrderMedicineService } from '../../../../../../main/webapp/app/entities/order-medicine';

describe('Component Tests', () => {

    describe('PharmacistPayment Management Dialog Component', () => {
        let comp: PharmacistPaymentDialogComponent;
        let fixture: ComponentFixture<PharmacistPaymentDialogComponent>;
        let service: PharmacistPaymentService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PharmacistPaymentDialogComponent],
                providers: [
                    OrderMedicineService,
                    PharmacistPaymentService
                ]
            })
            .overrideTemplate(PharmacistPaymentDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PharmacistPaymentDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PharmacistPaymentService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PharmacistPayment(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.pharmacistPayment = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'pharmacistPaymentListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PharmacistPayment();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.pharmacistPayment = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'pharmacistPaymentListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
