/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { DoctorScheduleDetailComponent } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule-detail.component';
import { DoctorScheduleService } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule.service';
import { DoctorSchedule } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule.model';

describe('Component Tests', () => {

    describe('DoctorSchedule Management Detail Component', () => {
        let comp: DoctorScheduleDetailComponent;
        let fixture: ComponentFixture<DoctorScheduleDetailComponent>;
        let service: DoctorScheduleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [DoctorScheduleDetailComponent],
                providers: [
                    DoctorScheduleService
                ]
            })
            .overrideTemplate(DoctorScheduleDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DoctorScheduleDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DoctorScheduleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new DoctorSchedule(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.doctorSchedule).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
