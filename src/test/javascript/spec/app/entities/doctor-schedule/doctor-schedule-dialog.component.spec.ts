/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { DoctorScheduleDialogComponent } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule-dialog.component';
import { DoctorScheduleService } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule.service';
import { DoctorSchedule } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule.model';
import { DoctorService } from '../../../../../../main/webapp/app/entities/doctor';

describe('Component Tests', () => {

    describe('DoctorSchedule Management Dialog Component', () => {
        let comp: DoctorScheduleDialogComponent;
        let fixture: ComponentFixture<DoctorScheduleDialogComponent>;
        let service: DoctorScheduleService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [DoctorScheduleDialogComponent],
                providers: [
                    DoctorService,
                    DoctorScheduleService
                ]
            })
            .overrideTemplate(DoctorScheduleDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DoctorScheduleDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DoctorScheduleService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new DoctorSchedule(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.doctorSchedule = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'doctorScheduleListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new DoctorSchedule();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.doctorSchedule = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'doctorScheduleListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
