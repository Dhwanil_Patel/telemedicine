/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { DoctorScheduleComponent } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule.component';
import { DoctorScheduleService } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule.service';
import { DoctorSchedule } from '../../../../../../main/webapp/app/entities/doctor-schedule/doctor-schedule.model';

describe('Component Tests', () => {

    describe('DoctorSchedule Management Component', () => {
        let comp: DoctorScheduleComponent;
        let fixture: ComponentFixture<DoctorScheduleComponent>;
        let service: DoctorScheduleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [DoctorScheduleComponent],
                providers: [
                    DoctorScheduleService
                ]
            })
            .overrideTemplate(DoctorScheduleComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DoctorScheduleComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DoctorScheduleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new DoctorSchedule(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.doctorSchedules[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
