/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PatientIssueDetailComponent } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue-detail.component';
import { PatientIssueService } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue.service';
import { PatientIssue } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue.model';

describe('Component Tests', () => {

    describe('PatientIssue Management Detail Component', () => {
        let comp: PatientIssueDetailComponent;
        let fixture: ComponentFixture<PatientIssueDetailComponent>;
        let service: PatientIssueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PatientIssueDetailComponent],
                providers: [
                    PatientIssueService
                ]
            })
            .overrideTemplate(PatientIssueDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PatientIssueDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PatientIssueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new PatientIssue(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.PatientIssue).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
