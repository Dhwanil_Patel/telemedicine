/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PatientIssueComponent } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue.component';
import { PatientIssueService } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue.service';
import { PatientIssue } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue.model';

describe('Component Tests', () => {

    describe('PatientIssues Management Component', () => {
        let comp: PatientIssueComponent;
        let fixture: ComponentFixture<PatientIssueComponent>;
        let service: PatientIssueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PatientIssue],
                providers: [
                    PatientIssueService
                ]
            })
            .overrideTemplate(PatientIssue, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PatientIssueComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PatientIssueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new PatientIssue(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.patientIssue[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
