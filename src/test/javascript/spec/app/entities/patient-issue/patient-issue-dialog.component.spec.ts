/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { PatientIssueDialogComponent } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue-dialog.component';
import { PatientIssueService } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue.service';
import { PatientIssue } from '../../../../../../main/webapp/app/entities/patient-issue/patient-issue.model';

describe('Component Tests', () => {

    describe('PatientIssue Management Dialog Component', () => {
        let comp: PatientIssueDialogComponent;
        let fixture: ComponentFixture<PatientIssueDialogComponent>;
        let service: PatientIssueService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [PatientIssueDialogComponent],
                providers: [
                    PatientIssueService
                ]
            })
                .overrideTemplate(PatientIssueDialogComponent, '')
                .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PatientIssueDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PatientIssueService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PatientIssue(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.patientIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'patientIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PatientIssue();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.patientIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'patientIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
