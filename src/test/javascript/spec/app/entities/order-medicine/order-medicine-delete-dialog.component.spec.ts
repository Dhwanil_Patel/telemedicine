/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { OrderMedicineDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine-delete-dialog.component';
import { OrderMedicineService } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.service';

describe('Component Tests', () => {

    describe('OrderMedicine Management Delete Component', () => {
        let comp: OrderMedicineDeleteDialogComponent;
        let fixture: ComponentFixture<OrderMedicineDeleteDialogComponent>;
        let service: OrderMedicineService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [OrderMedicineDeleteDialogComponent],
                providers: [
                    OrderMedicineService
                ]
            })
            .overrideTemplate(OrderMedicineDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OrderMedicineDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrderMedicineService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
