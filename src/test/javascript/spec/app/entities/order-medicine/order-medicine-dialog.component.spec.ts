/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { OrderMedicineDialogComponent } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine-dialog.component';
import { OrderMedicineService } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.service';
import { OrderMedicine } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.model';
import { PrescriptionService } from '../../../../../../main/webapp/app/entities/prescription';
import { PharmacistService } from '../../../../../../main/webapp/app/entities/pharmacist';
import { PharmacistPaymentService } from '../../../../../../main/webapp/app/entities/pharmacist-payment';

describe('Component Tests', () => {

    describe('OrderMedicine Management Dialog Component', () => {
        let comp: OrderMedicineDialogComponent;
        let fixture: ComponentFixture<OrderMedicineDialogComponent>;
        let service: OrderMedicineService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [OrderMedicineDialogComponent],
                providers: [
                    PrescriptionService,
                    PharmacistService,
                    PharmacistPaymentService,
                    OrderMedicineService
                ]
            })
            .overrideTemplate(OrderMedicineDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OrderMedicineDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrderMedicineService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new OrderMedicine(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.orderMedicine = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'orderMedicineListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new OrderMedicine();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.orderMedicine = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'orderMedicineListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
