/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { OrderMedicineComponent } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.component';
import { OrderMedicineService } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.service';
import { OrderMedicine } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.model';

describe('Component Tests', () => {

    describe('OrderMedicine Management Component', () => {
        let comp: OrderMedicineComponent;
        let fixture: ComponentFixture<OrderMedicineComponent>;
        let service: OrderMedicineService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [OrderMedicineComponent],
                providers: [
                    OrderMedicineService
                ]
            })
            .overrideTemplate(OrderMedicineComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OrderMedicineComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrderMedicineService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new OrderMedicine(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.orderMedicines[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
