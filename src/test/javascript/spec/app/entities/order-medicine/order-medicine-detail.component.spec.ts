/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { OrderMedicineDetailComponent } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine-detail.component';
import { OrderMedicineService } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.service';
import { OrderMedicine } from '../../../../../../main/webapp/app/entities/order-medicine/order-medicine.model';

describe('Component Tests', () => {

    describe('OrderMedicine Management Detail Component', () => {
        let comp: OrderMedicineDetailComponent;
        let fixture: ComponentFixture<OrderMedicineDetailComponent>;
        let service: OrderMedicineService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [OrderMedicineDetailComponent],
                providers: [
                    OrderMedicineService
                ]
            })
            .overrideTemplate(OrderMedicineDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OrderMedicineDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OrderMedicineService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new OrderMedicine(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.orderMedicine).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
