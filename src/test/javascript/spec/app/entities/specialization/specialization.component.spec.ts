/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { SpecializationComponent } from '../../../../../../main/webapp/app/entities/specialization/specialization.component';
import { SpecializationService } from '../../../../../../main/webapp/app/entities/specialization/specialization.service';
import { Specialization } from '../../../../../../main/webapp/app/entities/specialization/specialization.model';

describe('Component Tests', () => {

    describe('Specialization Management Component', () => {
        let comp: SpecializationComponent;
        let fixture: ComponentFixture<SpecializationComponent>;
        let service: SpecializationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [SpecializationComponent],
                providers: [
                    SpecializationService
                ]
            })
            .overrideTemplate(SpecializationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SpecializationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SpecializationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Specialization(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.specializations[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
