/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TelemedicineApplicationTestModule } from '../../../test.module';
import { SpecializationDetailComponent } from '../../../../../../main/webapp/app/entities/specialization/specialization-detail.component';
import { SpecializationService } from '../../../../../../main/webapp/app/entities/specialization/specialization.service';
import { Specialization } from '../../../../../../main/webapp/app/entities/specialization/specialization.model';

describe('Component Tests', () => {

    describe('Specialization Management Detail Component', () => {
        let comp: SpecializationDetailComponent;
        let fixture: ComponentFixture<SpecializationDetailComponent>;
        let service: SpecializationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TelemedicineApplicationTestModule],
                declarations: [SpecializationDetailComponent],
                providers: [
                    SpecializationService
                ]
            })
            .overrideTemplate(SpecializationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SpecializationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SpecializationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Specialization(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.specialization).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
